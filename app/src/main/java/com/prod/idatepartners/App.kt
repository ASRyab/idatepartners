package com.prod.idatepartners

import android.app.Application
import com.facebook.stetho.Stetho
import com.flurry.android.FlurryAgent
import com.prod.idatepartners.di.components.AppComponent
import com.prod.idatepartners.di.components.DaggerAppComponent
import com.prod.idatepartners.di.modules.ContextModule

class App : Application() {
    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .contextModule(ContextModule(this))
            .build()

        val appComponent: AppComponent = App.component
        appComponent.inject(this)

        if (!BuildConfig.DEBUG) {
            initFlurry()
        } else {
            initStetho()
        }
    }

    private fun initFlurry() {
        FlurryAgent.Builder()
            .build(this, getString(R.string.flurry_key))
    }

    private fun initStetho() {
        Stetho.initializeWithDefaults(this)
    }

}