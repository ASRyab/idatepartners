package com.prod.idatepartners.api

import android.content.Context
import com.prod.idatepartners.R

class Config(private val context: Context) {
    fun getBaseUrl(): String {
        return context.getString(R.string.server_url_live)
    }
}