package com.prod.idatepartners.api

import com.prod.idatepartners.api.models.AuthData
import com.prod.idatepartners.api.requests.AuthApi
import com.prod.idatepartners.api.utils.defaults.DeviceUtils
import io.reactivex.Single

class LoginRequest(private val requestManager: RequestManager) {


    fun login(login: String, pass: String): Single<AuthData> {
        val map = HashMap<String, String>()
        map["ManagerLoginForm[email]"] = login
        map["ManagerLoginForm[password]"] = pass
        map["deviceIdHex"] = DeviceUtils.getDeviceIdHex(requestManager.context)
        return requestManager.retrofit.create(AuthApi::class.java).userLogin(map)
            .compose { upstream -> requestManager.loginTransformer(upstream) }
    }
}