package com.prod.idatepartners.api

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import com.prod.idatepartners.api.models.AuthData
import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.GenerateAuthData
import com.prod.idatepartners.api.requests.AuthApi
import com.prod.idatepartners.api.rpc.ISocketManager
import com.prod.idatepartners.api.utils.ErrorResponse
import com.prod.idatepartners.api.utils.Headers
import com.prod.idatepartners.api.utils.HeadersInterceptor
import com.prod.idatepartners.utils.Debug
import com.prod.idatepartners.utils.PreferenceManager
import com.prod.idatepartners.utils.RxUtils
import com.prod.idatepartners.utils.UiHandler
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.Nullable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit


class RequestManager(
    internal val context: Context,
    internal val retrofit: Retrofit,
    private val preferenceManager: PreferenceManager,
    private val headersInterceptor: HeadersInterceptor,
    private val socketManager: ISocketManager,
    private val uiHandler: UiHandler
) {

    private var sessionEmitter: Observer<ServerSession>
    private var sessionObservable: Observable<ServerSession>
    private var pingDisposable: Disposable? = null
    private var validSessionObservable: Single<ServerSession>
    private var pingTime: Long = 0

    companion object {
        val TAG = RequestManager::class.java.name
        val EXPIRED_ACCESS_TOKEN_TIME = TimeUnit.MINUTES.toMillis(15)
        val PING_DELAY = TimeUnit.MINUTES.toMillis(3)
        val PING_PERIOD = PING_DELAY
    }

    init {
        val emitter = BehaviorSubject.create<ServerSession>()
        sessionEmitter = emitter
        sessionObservable = emitter.scan { oldSession, newSession ->
            this.updateSession(oldSession, newSession)
        }.doOnNext { this.setCurrentSession(it) }
            .replay(1).autoConnect()
        validSessionObservable = sessionObservable
            .filter { isSessionValid(it) }
            .firstOrError()
            .observeOn(Schedulers.io())

        initSession()
        Observable
            .combineLatest(uiHandler.haveUiSubject,
                sessionObservable,
                BiFunction<Boolean, ServerSession, Boolean> { haveUi, session ->
                    this.checkSessionOnBackToOnline(haveUi, session)
                }
            )
            .filter { it }
            .flatMap {
                sessionObservable.firstOrError().flatMap { session ->
                    session.refreshToken?.let { refreshToken(it) }
                }.toObservable()
            }
            .subscribe(RxUtils.emptyDataConsumer, RxUtils.getEmptyErrorConsumer(TAG, "init"))
    }

    private fun checkSessionOnBackToOnline(haveUi: Boolean, session: ServerSession): Boolean {
        Debug.logI(TAG, "changeConnectedState haveUI=$haveUi; session=$session")
        if (haveUi && isSessionValid(session)) {
            initPing()
            requestGenerateAuthData()
        }
        return haveUi && !isSessionValid(session)
    }

    private fun isSessionValid(session: ServerSession): Boolean {
        return (
                ServerSession.EMPTY_SESSION != session && !TextUtils.isEmpty(session.accessToken)
                        &&
                        (System.currentTimeMillis() < session.lastRequest + EXPIRED_ACCESS_TOKEN_TIME
                                || pingTime.plus(PING_PERIOD) > System.currentTimeMillis())
                )
    }

    private fun setCurrentSession(session: ServerSession) {
        if (ServerSession.EMPTY_SESSION != session) {
            headersInterceptor.addHeader(
                Headers.AUTHORIZATION_HEADER,
                Headers.AUTHORIZATION_PREFIX + session.accessToken
            )
            preferenceManager.serverSession = session
        }
    }

    fun <T : Any> requestWithTokenValidation(
        upstream: Single<BaseModelResponse<T>>
    ): SingleSource<T> {
        return validSessionObservable
            .map { Any() }
            .doOnError { socketManager.destroy() }
            .onErrorResumeNext {
                sessionObservable.firstOrError()
                    .flatMap { session ->
                        session.refreshToken?.let {
                            refreshToken(it)
                        }
                    }
            }
            .flatMap { upstream }
            .compose(defaultTransformer())
    }

    fun refreshToken(refreshToken: String): Single<AuthData> {
        headersInterceptor.removeHeader(Headers.AUTHORIZATION_HEADER)
        val map = HashMap<String, String>()
        map["key"] = refreshToken
        Debug.logD(TAG, "refreshToken=$refreshToken")

        return retrofit.create(AuthApi::class.java).autologin(map)
            .compose { upstream -> tokenTransformer(upstream) }
            .doOnSuccess { initPing() }
            .doOnSuccess { requestGenerateAuthData() }
            .observeOn(AndroidSchedulers.mainThread())
    }


    private fun updateSession(oldSession: ServerSession, newSession: ServerSession): ServerSession {
        if (ServerSession.EMPTY_SESSION == oldSession || ServerSession.EMPTY_SESSION == newSession) {
            return newSession
        }
        if (TextUtils.isEmpty(oldSession.refreshToken)) {
            oldSession.refreshToken = newSession.refreshToken
        }
        oldSession.accessToken = newSession.accessToken
        oldSession.lastRequest = newSession.lastRequest
        return oldSession
    }

    private fun initSession() {
        val session = preferenceManager.serverSession
        sessionEmitter.onNext(session)
    }

    private fun tokenTransformer(upstream: Single<BaseModelResponse<AuthData>>): SingleSource<AuthData> {
        return upstream
            .compose<AuthData>(defaultTransformer())
            .doOnSuccess { this.onUserAuthorized(it) }
    }

    fun loginTransformer(upstream: Single<BaseModelResponse<AuthData>>): SingleSource<AuthData> {
        return upstream
            .compose { loginStream -> tokenTransformer(loginStream) }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { requestGenerateAuthData() }
            .doOnSuccess { initPing() }
    }


    private fun initPing() {
        pingDisposable?.dispose()
        pingDisposable =
            Observable.interval(PING_DELAY, PING_PERIOD, TimeUnit.MILLISECONDS, Schedulers.io())
                .takeUntil(uiHandler.haveUiSubject.filter { !it })
                .subscribe {
                    retrofit.create(AuthApi::class.java).pingManager()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            Consumer { pingTime = System.currentTimeMillis() },
                            RxUtils.getEmptyErrorConsumer(TAG, "pingManager")
                        )
                }
    }

    @SuppressLint("CheckResult")
    private fun requestGenerateAuthData() {
        retrofit.create(AuthApi::class.java).generateAuthData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(Consumer {
                initSocket(it)

            }, RxUtils.getEmptyErrorConsumer(TAG, "requestGenerateAuthData"))
    }

    private fun initSocket(it: GenerateAuthData) {
        socketManager.connectSocket(it)
    }


    @SuppressLint("CheckResult")
    protected fun <T : Any> defaultTransformer(): SingleTransformer<BaseModelResponse<T>, T> {
        return SingleTransformer { upstream ->
            upstream
                .flatMap { t -> checkErrors(t) }
                .subscribeOn(Schedulers.io())
        }
    }

    protected fun onUserAuthorized(@Nullable authData: AuthData) {
        val sessionStartTime = System.currentTimeMillis()
        val session = ServerSession(authData.access_token, authData.refresh_token, sessionStartTime)
        sessionEmitter.onNext(session)
    }

    @Throws(Exception::class)
    protected fun <T : Any> checkErrors(response: BaseModelResponse<T>): SingleSource<T> {
        if (response.isError) {
            return Single.error(
                if (response.meta != null)
                    ErrorResponse.serverError(response.meta)
                else ErrorResponse.parseError(Exception())
            )
        }
        return Single.just(response.data)
    }

    @Synchronized
    fun clearSession() {
        headersInterceptor.removeHeader(Headers.AUTHORIZATION_HEADER)
        preferenceManager.clearAll()
        pingDisposable?.dispose()
        initSession()
    }

    fun currentSessionObservable(): Maybe<ServerSession> {
        return sessionObservable.firstElement()
    }
}