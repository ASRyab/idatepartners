package com.prod.idatepartners.api

import com.google.gson.annotations.Expose

data class ServerSession(
    @Expose
    var accessToken: String? = null,
    @Expose
    var refreshToken: String? = null,

    @Expose
    var lastRequest: Long = 0
) {

    companion object {
        val EMPTY_SESSION = ServerSession()
    }
}