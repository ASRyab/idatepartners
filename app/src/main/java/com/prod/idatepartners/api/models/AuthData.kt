package com.prod.idatepartners.api.models

data class AuthData(
    val autologin_key: String? = null,
    val token_type: String? = null,
    val refresh_token: String? = null,
    var access_token: String? = null)

