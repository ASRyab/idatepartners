package com.prod.idatepartners.api.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class BaseModelResponse<T>(
    val status: Status? = null,
    val meta: Meta? = null,
    val data: T? = null
) : Serializable {


    val isError: Boolean
        get() = Status.ERROR == status

    enum class Status {
        @SerializedName("success")
        SUCCESS,
        @SerializedName("error")
        ERROR
    }

    override fun toString(): String {
        return "BaseModelResponse{" +
                "status=" + status +
                ", meta=" + meta +
                ", data=" + data +
                '}'.toString()
    }
}