package com.prod.idatepartners.api.models;

import com.google.gson.annotations.SerializedName;

data class GenerateAuthData(
    @SerializedName("host")
    val host: String?,
    @SerializedName("params")
    val params: Params
)

data class Params(
    @SerializedName("query")
    val query: String?,
    @SerializedName("interactionType")
    val interactionType: String?
)
