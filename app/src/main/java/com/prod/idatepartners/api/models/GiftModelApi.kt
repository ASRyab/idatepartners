package com.prod.idatepartners.api.models;

import com.google.gson.annotations.SerializedName;

data class GiftModelApi(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("categoryId")
    val categoryId: Int,
    @SerializedName("imgUrl")
    val imgUrl: String
)
