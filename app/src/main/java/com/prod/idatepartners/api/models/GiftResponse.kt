package com.prod.idatepartners.api.models;

import com.google.gson.annotations.SerializedName;

data class GiftResponse(

    @SerializedName("gifts")
    val gifts: HashMap<String, List<GiftModelApi>>
)


