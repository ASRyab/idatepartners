package com.prod.idatepartners.api.models

import com.google.gson.annotations.SerializedName
import com.prod.idatepartners.api.models.profiles.UserApi

data class LetterApiResponse(
    @SerializedName("upgradeUrl")
    val upgradeUrl: String?,
    @SerializedName("limit")
    val limit: Int?,
    @SerializedName("offset")
    val offset: Int?,
    @SerializedName("fromTime")
    val fromTime: String,
    @SerializedName("toTime")
    val toTime: String,
    @SerializedName("messages")
    val letters: Letters,
    @SerializedName("users")
    val users: HashMap<String, UserApi>
)

data class Letters(
    @SerializedName("list")
    val list: List<SingleLetterApi>
) {
    companion object {
        val EMPTY_LETTERS = Letters(ArrayList())
    }
}
