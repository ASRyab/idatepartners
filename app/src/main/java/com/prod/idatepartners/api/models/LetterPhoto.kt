package com.prod.idatepartners.api.models

import com.google.gson.annotations.SerializedName

data class LetterPhoto(
    @SerializedName("key")
    val key: String?,
    @SerializedName("isTitle")
    val isTitle: Boolean?,
    @SerializedName("isBlured")
    val isBlured: Boolean?,
    @SerializedName("url")
    val url: String?,
    @SerializedName("getUnbluredUrl")
    val getUnbluredUrl: String
)