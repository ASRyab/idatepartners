package com.prod.idatepartners.api.models

import android.text.Html
import com.google.gson.annotations.Expose
import java.util.*


class Meta(
    @Expose val code: Int, @Expose val redirect: String, @Expose val description: HashMap<String, Array<String>>
) {

    companion object {
        private val possibleErrorKeys = arrayOf("message", "password", "errors", "email", "username", "manager", "general")
    }

    val firstMessage: String?
        get() {
            var firstKey: String? = null
            if (!description.isEmpty()) {
                for (key in possibleErrorKeys) {
                    if (description.containsKey(key)) {
                        firstKey = key
                        break
                    }
                }
                if (firstKey == null) {
                    firstKey = description.keys.iterator().next()
                }
                return Html.fromHtml(description[firstKey]!![0]).toString()
            }
            return null
        }


    fun getFirstMessageByKey(key: String): String? {
        if (!description.isEmpty()) {
            if (description.containsKey(key) && description[key]!!.isNotEmpty()) {
                return Html.fromHtml(description[key]!![0]).toString()
            }
        }
        return null
    }

    override fun toString(): String {
        return "Meta{" +
                "code=" + code +
                ", redirect='" + redirect + '\''.toString() +
                ", description=" + description +
                '}'.toString()
    }
}