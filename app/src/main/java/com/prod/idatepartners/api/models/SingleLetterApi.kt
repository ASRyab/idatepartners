package com.prod.idatepartners.api.models

import com.google.gson.annotations.SerializedName

data class SingleLetterApi(
    @SerializedName("id")
    val id: String,
    @SerializedName("senderId")
    val senderId: String,
    @SerializedName("recipientId")
    val recipientId: String,
    @SerializedName("time")
    val time: String?,
    @SerializedName("dt")
    val dt: String?,
    @SerializedName("isRead")
    val isRead: Boolean?,
    @SerializedName("subject")
    val subject: String?,
    @SerializedName("type")
    val type: String?,
    @SerializedName("replyToId")
    val replyToId: String?,
    @SerializedName("images")
    val images: List<LetterPhoto>?,
    @SerializedName("timestamp")
    val timestamp: Long?,
    @SerializedName("shortText")
    val shortText: String?,
    @SerializedName("direction")
    val direction: String,
    @SerializedName("senderName")
    val senderName: String?,
    @SerializedName("hasRepliedSubject")
    val hasRepliedSubject: Boolean?,
    @SerializedName("text")
    val text: String?
)