package com.prod.idatepartners.api.models.chat;

import com.google.gson.annotations.SerializedName;

data class ChatHistoryResponse(
    @SerializedName("list")
    val list: List<MessageApi>,
    @SerializedName("prev_exist")
    val prevExist: Boolean?,
    @SerializedName("isUserAdmin")
    val isUserAdmin: Boolean?,
    @SerializedName("upgrade_type")
    val upgradeType: String?,
    @SerializedName("upgrade_read")
    val upgradeRead: String?,
    @SerializedName("upgrade_communication")
    val upgradeCommunication: String?,
    @SerializedName("upgradeChataholicPack")
    val upgradeChataholicPack: String?,
    @SerializedName("upgrade_communication_show")
    val upgradeCommunicationShow: Boolean?,
    @SerializedName("upgrade_delivery")
    val upgradeDelivery: String?,
    @SerializedName("userBlocked")
    val userBlocked: Boolean?,
    @SerializedName("userReported")
    val userReported: Boolean?,
    @SerializedName("myUserBlocked")
    val myUserBlocked: Boolean?,
    @SerializedName("myUserReported")
    val myUserReported: Boolean?,
    @SerializedName("messageCount")
    val messageCount: Int?,
    @SerializedName("hiddenLegendItems")
    val hiddenLegendItems: HashMap<String,Boolean>,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("myGender")
    val myGender: String?,
    @SerializedName("smileBlank")
    val smileBlank: String?,
    @SerializedName("isUserHasFreeCommunication")
    val isUserHasFreeCommunication: Boolean?,
    @SerializedName("isUserHasCredits")
    val isUserHasCredits: Boolean?,
    @SerializedName("hasWebCam")
    val hasWebCam: String?,
    @SerializedName("isPhotosendAvailableWithUser")
    val isPhotosendAvailableWithUser: Boolean?,
    @SerializedName("freeMessagesCount")
    val freeMessagesCount: Int?,
    @SerializedName("upgradeReadTimer")
    val upgradeReadTimer: Boolean?
)




