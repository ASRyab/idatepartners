package com.prod.idatepartners.api.models.chat

import com.google.gson.annotations.SerializedName

data class MessageApi(
    @SerializedName("id")
    val id: String,
    @SerializedName("extraId")
    val extraId: String?,
    @SerializedName("senderId")
    val senderId: String,
    @SerializedName("recipientId")
    val recipientId: String,
    @SerializedName("time")
    val time: String?,
    @SerializedName("dt")
    val dt: String?,
    @SerializedName("viewedAt")
    val viewedAt: String?,
    @SerializedName("isRead")
    val isRead: Boolean = false,
    @SerializedName("subject")
    val subject: String?,
    @SerializedName("text")
    val text: String?,
    @SerializedName("type")
    val type: String,
    @SerializedName("timestamp")
    val timestamp: Long?,
    @SerializedName("resources")
    val resources: PrivatChatImage?
)

data class PrivatChatImage(
    @SerializedName("privateChatImage")
    val privateChatImage: List<ChatPhoto>
)

data class ChatPhoto(
    @SerializedName("imageId")
    val id: String,
    @SerializedName("imgUrl")
    val imgUrl: String,
    @SerializedName("isNew")
    val isNew: Boolean
)