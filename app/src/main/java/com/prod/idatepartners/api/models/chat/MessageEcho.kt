package com.prod.idatepartners.api.models.chat

data class MessageEcho(
    val id: String,
    val time: String?,
    val todayConversation: String?,
    val timestamp: Long,
    val upgrade_type: String?,
    val upgrade_communication: Boolean?,
    val isBaned: Boolean?,
    val fromUserPhoto: String?,
    val fullMembership: String?,
    val credits: Long?
)

