package com.prod.idatepartners.api.models.profiles

import com.google.gson.annotations.SerializedName

class DrivenContent(
    @SerializedName("contentId") val contentId: String,
    @SerializedName("contentType") val contentType: String,
    @SerializedName("profileId") val profileId: String,
    @SerializedName("approveStatus") val approveStatus: String,
    @SerializedName("createdAt") val createdAt: String,
    @SerializedName("url") val url: String,
    @SerializedName("previewUrl") val previewUrl: String
)