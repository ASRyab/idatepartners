package com.prod.idatepartners.api.models.profiles

import com.google.gson.annotations.SerializedName

data class DrivenContentResponse(
    @SerializedName("contents")
    val contents: HashMap<String, HashMap<String, DrivenContent>>
)