package com.prod.idatepartners.api.models.profiles;

import com.google.gson.annotations.SerializedName;

data class DrivenUser(
    @SerializedName("primaryPhoto")
    val primaryPhoto: Photo,
    @SerializedName("id")
    val id: String,
    @SerializedName("login")
    val login: String?,
    @SerializedName("firstName")
    val firstName: String?,
    @SerializedName("lastName")
    val lastName: String?,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("age")
    val age: Int?,
    @SerializedName("country")
    val country: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("registeredAt")
    val registeredAt: String?
)

