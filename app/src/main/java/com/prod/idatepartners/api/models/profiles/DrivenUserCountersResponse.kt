package com.prod.idatepartners.api.models.profiles

import com.google.gson.annotations.SerializedName

data class DrivenUserCountersResponse (
    @SerializedName("counters")
    val counters: HashMap<String, Int>)