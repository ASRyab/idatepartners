package com.prod.idatepartners.api.models.profiles

import com.google.gson.annotations.SerializedName

data class DrivenUserResponse(
    @SerializedName("profiles")
    val profiles: HashMap<String, DrivenUser>)