package com.prod.idatepartners.api.models.profiles

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Photo(
    @SerializedName("id")
    @ColumnInfo(name = "photo_id")
    val id: String?,
    @SerializedName("photoUrl", alternate = ["url"])
    @ColumnInfo(name = "photoUrl")
    val photoUrl: String?,
    @SerializedName("previewUrl")
    @ColumnInfo(name = "previewPhotoUrl")
    val previewPhotoUrl: String?
)

data class Attributes(
    @SerializedName("privatePhoto")
    val privatePhoto: String?,
    @SerializedName("level")
    val level: String?,
    @SerializedName("rate")
    val rate: String?,
    @SerializedName("width")
    val width: String?,
    @SerializedName("height")
    val height: String?,
    @SerializedName("dominantColor")
    val dominantColor: String?
)
