package com.prod.idatepartners.api.models.profiles;

import com.google.gson.annotations.SerializedName;

data class ProfileContactsResponse(
    @SerializedName("profileContacts")
    val profileContacts: ProfileContacts
)

data class ProfileContacts(
    @SerializedName("users")
    val users: HashMap<String, UserApi>,
    @SerializedName("counters")
    val counters: Counters,
    @SerializedName("profileId")
    val profileId: String?
)

data class UserApi(
    @SerializedName("primaryPhoto")
    val primaryPhoto: Photo,
    @SerializedName("build")
    val build: Int?,
    @SerializedName("height")
    val height: Int?,
    @SerializedName("weight")
    val weight: Int?,
    @SerializedName("education")
    val education: Int?,
    @SerializedName("hobbiesAndInterests")
    val hobbiesAndInterests: String?,
    @SerializedName("id")
    val id: String,
    @SerializedName("login")
    val login: String?,
    @SerializedName("firstName")
    val firstName: String?,
    @SerializedName("lastName")
    val lastName: String?,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("age")
    val age: Int?,
    @SerializedName("country")
    val country: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("registeredAt")
    val registeredAt: String?,
    @SerializedName("isFriend")
    val isFriend: Boolean?,
    @SerializedName("isOnline")
    val isOnline: Boolean?,
    @SerializedName("photos")
    val photos: ArrayList<Photo>?,
    @SerializedName("lastMessageTime")
    val lastMessageTime: Long?
)

data class Counters(
    @SerializedName("mail")
    val mail: HashMap<String, Int>?,
    @SerializedName("letter")
    val letter: HashMap<String, Int>?
) {
    companion object {
        val EMPTY_COUNTERS = Counters(HashMap(), HashMap())
    }
}



