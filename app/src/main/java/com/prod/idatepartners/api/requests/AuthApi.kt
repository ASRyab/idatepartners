package com.prod.idatepartners.api.requests

import com.prod.idatepartners.api.models.AuthData
import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.GenerateAuthData
import io.reactivex.Single
import retrofit2.http.*

interface AuthApi {

    @FormUrlEncoded
    @POST("/b2b/login/process")
    fun userLogin(@FieldMap authorizationPartMap: Map<String, String>): Single<BaseModelResponse<AuthData>>

    @GET("/interaction/generateAuthData")
    fun generateAuthData(): Single<GenerateAuthData>

    @GET("/b2b/autologin")
    fun autologin(@QueryMap authorizationPartMap: Map<String, String>): Single<BaseModelResponse<AuthData>>

    @GET("/b2b/manager/statePing")
    fun pingManager(): Single<Any>

}