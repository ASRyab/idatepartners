package com.prod.idatepartners.api.requests

import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.chat.ChatHistoryResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface ChatApi {
    @GET("/b2b/mail/history/id/{jsonData}")
    fun getChatHistory(
        @Path(
            value = "jsonData",
            encoded = true
        ) jsonData: String, @QueryMap map: Map<String, String>
    ): Single<BaseModelResponse<ChatHistoryResponse>>
}