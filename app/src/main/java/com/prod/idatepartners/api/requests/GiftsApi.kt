package com.prod.idatepartners.api.requests

import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.GiftResponse
import io.reactivex.Single
import retrofit2.http.GET

interface GiftsApi {
    @GET("/b2b/realGifts/getGiftDictionary")
    fun getGiftDictionary(): Single<BaseModelResponse<GiftResponse>>

}