package com.prod.idatepartners.api.requests

import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.LetterApiResponse
import com.prod.idatepartners.api.models.SingleLetterApi
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap

interface LetterApi {
    @GET("/b2b/letter/getConversation")
    fun getLetterHistory(@QueryMap map: Map<String, String>): Single<BaseModelResponse<LetterApiResponse>>

    @GET("/b2b/letter")
    fun getLetterById(@QueryMap map: Map<String, String>): Single<BaseModelResponse<SingleLetterApi>>

    @POST("/b2b/letter/new")
    fun sendLetter(@QueryMap map: HashMap<String, String?>): Single<BaseModelResponse<Any>>
}