package com.prod.idatepartners.api.requests

import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.profiles.DrivenContentResponse
import com.prod.idatepartners.api.models.profiles.DrivenUserCountersResponse
import com.prod.idatepartners.api.models.profiles.DrivenUserResponse
import com.prod.idatepartners.api.models.profiles.ProfileContactsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface UsersApi {

    @GET("/b2b/manager/profiles")
    fun getDrivenUsers(): Single<BaseModelResponse<DrivenUserResponse>>

    @GET("/b2b/manager/counters")
    fun getDrivenCounters(): Single<BaseModelResponse<DrivenUserCountersResponse>>

    @GET("/b2b/manager/recentContacts/profileId/{jsonData}")
    fun getUsersByDriven(
        @Path(
            value = "jsonData",
            encoded = true
        ) jsonData: String
    ): Single<BaseModelResponse<ProfileContactsResponse>>

    @GET("/b2b/content/getList")
    fun getDrivenContent(): Single<BaseModelResponse<DrivenContentResponse>>
}