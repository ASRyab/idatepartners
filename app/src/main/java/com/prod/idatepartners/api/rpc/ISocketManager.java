package com.prod.idatepartners.api.rpc;

import com.prod.idatepartners.api.models.GenerateAuthData;
import com.prod.idatepartners.api.rpc.rpc_actions.ChatReadMarker;
import com.prod.idatepartners.api.rpc.rpc_actions.MessageSocket;

import org.jetbrains.annotations.NotNull;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface ISocketManager {
    void connectSocket(GenerateAuthData generateAuthData);

    <T extends RPCResponse> Single<T> executeRPCAction(final RPCAction<T> rpcAction);

    void destroy();

    boolean isConnected();

    @NotNull
    Observable<MessageSocket> getChatMessageListener();

    Observable<ChatReadMarker> getChatReadAllListener();

    @NotNull
    Observable<MessageSocket> getLettersListener();

}

