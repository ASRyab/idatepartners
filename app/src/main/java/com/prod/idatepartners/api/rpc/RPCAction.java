package com.prod.idatepartners.api.rpc;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

import io.reactivex.SingleEmitter;

public abstract class RPCAction<R extends RPCResponse> {
    @Expose
    private int id;
    @Expose
    private final String method;
    @Expose
    private final JsonElement params;

    private SingleEmitter<R> emitter;


    public RPCAction(int id, String method, JsonElement params) {
        this(method, params);
        this.id = id;
    }

    public RPCAction(String method, JsonElement params) {
        this.method = method;
        this.params = params;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public JsonElement getParams() {
        return params;
    }

    public abstract R getResponse();

    public abstract void setResponse(R response);

    public void setEmitter(SingleEmitter<R> emitter) {
        this.emitter = emitter;
    }

    public SingleEmitter<R> getEmitter() {
        return emitter;
    }

    public boolean isSuccess() {
        R response = getResponse();
        if (response != null && response.getResult() != null) {
            if (response.getResult() instanceof RPCResult) {
                return ((RPCResult) response.getResult()).getStatus() == RPCResult.Status.SUCCESS;
            } else {
                return true;
            }
        }
        return false;
    }
}