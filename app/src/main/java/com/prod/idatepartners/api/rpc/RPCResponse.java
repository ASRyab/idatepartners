package com.prod.idatepartners.api.rpc;

import com.google.gson.annotations.Expose;

public class RPCResponse<T> {
    @Expose
    private int id;

    @Expose
    private T result;

    @Expose
    private String error;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public int getId() {
        return id;
    }

    public String getError() {
        return error;
    }
}