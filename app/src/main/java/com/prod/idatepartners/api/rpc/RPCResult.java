package com.prod.idatepartners.api.rpc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RPCResult<T> {
    @Expose
    private Status status;

    @Expose
    private T data;

    @Expose
    private List<String> notFound;

    public static enum Status {
        @SerializedName("success")
        SUCCESS,
        @SerializedName("error")
        ERROR;
    }

    public Status getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<String> getNotFound() {
        return notFound;
    }
}
