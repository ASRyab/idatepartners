package com.prod.idatepartners.api.rpc;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prod.idatepartners.api.models.GenerateAuthData;
import com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio.Acknowledge;
import com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio.ConnectCallback;
import com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio.EventCallback;
import com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio.SocketIOClient;
import com.prod.idatepartners.api.rpc.rpc_actions.ChatReadMarker;
import com.prod.idatepartners.api.rpc.rpc_actions.MessageSocket;
import com.prod.idatepartners.api.utils.ErrorResponse;
import com.prod.idatepartners.utils.Debug;
import com.prod.idatepartners.utils.RxUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import okhttp3.OkHttpClient;

public class SocketManager implements ISocketManager {
    private final static String TAG = SocketManager.class.getSimpleName();

    final private static int RAND_MAX = 32767;
    final private static String RPC = "rpc";
    final private static String AUTH = "auth";
    final private static String MSG = "msg";
    final private static String ROOMS = "rooms";

    private final String serverUrl;
    private final int socketPort;
    private final OkHttpClient okHttpClient;
    private SocketIOClient socketIOClient;
    private AtomicInteger messageId = new AtomicInteger(0);
    private Gson gson;
    private Hashtable<Integer, RPCAction> rpcActions = new Hashtable<Integer, RPCAction>();
    private PublishSubject<MessageSocket> chatMessagePublisher = PublishSubject.create();
    private PublishSubject<MessageSocket> letterMessagePublisher = PublishSubject.create();
    private PublishSubject<ChatReadMarker> readAllChatPublisher = PublishSubject.create();

    @SuppressLint("CheckResult")
    private SocketManager(Builder builder) {
        this.serverUrl = builder.serverUrl;
        this.socketPort = builder.socketPort;
        this.okHttpClient = builder.okHttpClient;
        this.gson = builder.gson;
        Observable<Boolean> haveUIObservable = builder.haveUIObservable;

        haveUIObservable.filter(it -> !it).subscribe(__ -> destroy(),
                RxUtils.INSTANCE.getEmptyErrorConsumer(TAG, "init"));
    }

    @Override
    public synchronized void connectSocket(GenerateAuthData generateAuthData) {
        destroy();
        SocketIOClient.setReconnect(true);
        StringBuilder query = new StringBuilder();
        Random rand = new Random();
        query.append("/?t=").append(rand.nextInt(RAND_MAX));
        query.append("&").append(generateAuthData.getParams().getQuery().replace("&platform=androidApp", "&platform=webSite"));
        query.append("&interactionType=").append(generateAuthData.getParams().getInteractionType());
        String url = serverUrl + ":" + socketPort + query.toString();
        SocketIOClient.connect(url, okHttpClient, new ConnectCallback() {
            @Override
            public void onConnectCompleted(Exception ex, SocketIOClient client) {
                if (ex == null) {
                    Debug.logD(TAG, "socket connected");
                } else {
                    Debug.logD(TAG, "socket error");
                    ex.printStackTrace();
                    return;
                }
                if (isConnected()) {
                    destroy();
                }
                socketIOClient = client;
                socketIOClient.addListener(RPC, rpcEventCallback);
                socketIOClient.addListener(MSG, msgEventCallback);
                socketIOClient.addListener(ROOMS, roomsEventCallback);
                socketIOClient.addListener(AUTH, authEventCallback);
            }
        }, new Handler());
    }

    @Override
    public <T extends RPCResponse> Single<T> executeRPCAction(final RPCAction<T> rpcAction) {
//        todo need implement same with REST
//        if (!isConnected() && generateAuthData != null) {
//            connectSocket(generateAuthData);
//        }
        return Single.create((SingleOnSubscribe<T>) emitter -> {
            if (isConnected()) {
                try {
                    rpcAction.setId(messageId.incrementAndGet());
                    String args = gson.toJson(rpcAction);
                    JSONObject argsObj = new JSONObject(args);
                    JSONArray array = new JSONArray();
                    array.put(argsObj);
                    rpcAction.setEmitter(emitter);
                    rpcActions.put(rpcAction.getId(), rpcAction);
                    socketIOClient.emit(RPC, array);
                } catch (JSONException exception) {
                    emitter.onError(exception);
                    exception.printStackTrace();
                }
            } else {
                if (!"b2bMarkAsRead".equals(rpcAction.getMethod())) {
                    emitter.onError(ErrorResponse.networkError((new IOException())));
                }
            }
        })
                .subscribeOn(Schedulers.io());
    }

    @Override
    public void destroy() {
        if (isConnected()) {
            SocketIOClient.setReconnect(false);
            socketIOClient.removeListener(RPC, rpcEventCallback);
            socketIOClient.removeListener(MSG, msgEventCallback);
            socketIOClient.removeListener(ROOMS, roomsEventCallback);
            socketIOClient.addListener(AUTH, authEventCallback);
            socketIOClient.disconnect();
            socketIOClient = null;
        }
    }

    @Override
    public boolean isConnected() {
        Debug.logD(TAG, "isConnected : socketIOClient : " + socketIOClient);
        return socketIOClient != null && socketIOClient.isConnected();
    }

    @NotNull
    @Override
    public Observable<MessageSocket> getChatMessageListener() {
        return chatMessagePublisher;
    }
    @Override
    public Observable<ChatReadMarker> getChatReadAllListener() {
        return readAllChatPublisher;
    }

    @NotNull
    @Override
    public Observable<MessageSocket> getLettersListener() {
        return letterMessagePublisher;
    }

    public interface SocketManagerListener {
        void onConnect();
    }

    private ArrayList<SocketManagerListener> subscribeListeners = new ArrayList<>();

    public void subscribeOn(SocketManagerListener socketManagerListener) {
        if (isConnected()) {
            socketManagerListener.onConnect();
        } else {
            subscribeListeners.add(socketManagerListener);
        }
    }

    private EventCallback rpcEventCallback = new EventCallback() {
        @Override
        public void onEvent(String event, JSONArray argument, Acknowledge acknowledge) {
            try {
                JSONObject arg = argument.getJSONObject(0);
                if (arg.has("id")) {
                    int id = arg.getInt("id");
                    if (rpcActions.containsKey(id)) {
                        RPCResponse response;
                        RPCAction action = rpcActions.get(id);
                        SingleEmitter emitter = action.getEmitter();
                        response = parseResponseData(arg.toString(), action);
                        action.setResponse(response);
                        if (action.isSuccess()) {
                            emitter.onSuccess(response);
                        } else {
                            emitter.onError(ErrorResponse.rpcError(response.getError()));
                        }
                        rpcActions.remove(id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private EventCallback authEventCallback = (event, argument, acknowledge) -> {
        for (SocketManagerListener subscriber : subscribeListeners) {
            subscriber.onConnect();
        }
        subscribeListeners.clear();
    };

    private EventCallback roomsEventCallback = (event, argument, acknowledge) -> {
        try {
            String type = argument.getString(0);
            String responseBody = argument.getString(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    };

    protected RPCResponse parseResponseData(String responseBody, RPCAction action) {
        Type dataType = null;
        Class<?> clazz = action.getClass();
        while (!Object.class.equals(clazz) && clazz != null) {
            if (clazz == null) break;
            if (!(clazz.getGenericSuperclass() instanceof ParameterizedType))//to avoid attempts to parse not parametrized response
                return new RPCResponse();
            Class superClass = clazz.getSuperclass();
            if ((superClass.equals(RPCAction.class)
                    && ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments().length > 0)) {
                dataType = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
                break;
            }
            clazz = clazz.getSuperclass();
        }

        RPCResponse rpcResponse = null;
        try {
            rpcResponse = gson.fromJson(responseBody, dataType);
        } catch (Exception e) {
            action.getEmitter().onError(ErrorResponse.parseError(e));
            e.printStackTrace();
        }
        if (rpcResponse == null) {
            rpcResponse = new RPCResponse();
        }
        return rpcResponse;
    }

    private EventCallback msgEventCallback = (event, argument, acknowledge) -> {
        int incomeMsgsCount = argument.length();
        if (incomeMsgsCount == 0) {
            return;
        }
        for (int i = 0; i < incomeMsgsCount; i++) {
            MessageSocket messageEcho = null;
            ChatReadMarker chatReadMarker = null;
            try {
                String json = argument.getString(i);
                JsonObject result = new JsonParser().parse(json).getAsJsonObject();
                if (result.get("type").getAsString().equals("mail")) {
                    messageEcho = gson.fromJson(json, MessageSocket.class);
                } else if (result.get("type").getAsString().equals("readmessages")) {
                    chatReadMarker = gson.fromJson(json, ChatReadMarker.class);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (messageEcho != null && messageEcho.getMsgType() != null) {
                if (messageEcho.getMsgType().equals("letter")) {
                    letterMessagePublisher.onNext(messageEcho);
                } else {
                    chatMessagePublisher.onNext(messageEcho);
                }
            } else if (chatReadMarker != null) {
                readAllChatPublisher.onNext(chatReadMarker);
            }
        }
    };


    public final static class Builder {
        private String serverUrl;
        private int socketPort;
        private OkHttpClient okHttpClient;
        boolean isSocketPortSet;
        private Gson gson;
        private Observable<Boolean> haveUIObservable;

        public Builder setSocketPort(int socketPort) {
            this.socketPort = socketPort;
            isSocketPortSet = true;
            return this;
        }

        public Builder setOkHttpClient(OkHttpClient okHttpClient) {
            this.okHttpClient = okHttpClient;
            return this;
        }

        public Builder setHaveUIObservable(Observable<Boolean> haveUIObservable) {
            this.haveUIObservable = haveUIObservable;
            return this;
        }

        public Builder setServerUrl(String serverUrl) {
            this.serverUrl = serverUrl;
            return this;
        }

        public Builder setGson(Gson gson) {
            this.gson = gson;
            return this;
        }

        public SocketManager create() {
            if (okHttpClient == null
                    || TextUtils.isEmpty(serverUrl)
                    || !isSocketPortSet) {
                throw new IllegalArgumentException("All fields in Builder are mandatory");
            }
            return new SocketManager(this);
        }
    }
}