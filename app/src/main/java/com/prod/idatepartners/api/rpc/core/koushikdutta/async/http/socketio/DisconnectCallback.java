package com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio;

public interface DisconnectCallback {
    void onDisconnect(Exception e);
}
