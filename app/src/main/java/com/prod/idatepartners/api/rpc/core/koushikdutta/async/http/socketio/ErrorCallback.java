package com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio;

public interface ErrorCallback {
    void onError(String error);
}
