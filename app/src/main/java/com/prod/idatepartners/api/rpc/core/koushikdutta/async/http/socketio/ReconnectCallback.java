package com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio;

public interface ReconnectCallback {
    public void onReconnect();
}