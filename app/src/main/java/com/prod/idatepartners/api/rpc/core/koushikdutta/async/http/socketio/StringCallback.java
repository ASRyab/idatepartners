package com.prod.idatepartners.api.rpc.core.koushikdutta.async.http.socketio;

public interface StringCallback {
    public void onString(String string, Acknowledge acknowledge);
}