package com.prod.idatepartners.api.rpc.core.koushikdutta.http;

import android.net.Uri;
import android.os.AsyncTask;

import com.prod.idatepartners.api.rpc.core.android_websockets.WebSocketClient;
import com.prod.idatepartners.utils.Debug;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AsyncHttpClient {
    private static final String TAG = "android-websockets:AsyncHttpClient";

    private final OkHttpClient okHttpClient;

    public AsyncHttpClient(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    public static class SocketIORequest {

        private String mUri;
        private String mEndpoint;

        public SocketIORequest(String uri) {
            this(uri, null);
        }

        public SocketIORequest(String uri, String endpoint) {

            mUri = Uri.parse(uri).buildUpon().encodedPath("/socket.io/1/").build().toString();
            mEndpoint = endpoint;
        }

        public String getUri() {

            return mUri;
        }

        public String getEndpoint() {

            return mEndpoint;
        }
    }

    public static interface StringCallback {
        public void onCompleted(final Exception e, String result);
    }

    public static interface WebSocketConnectCallback {
        public void onCompleted(Exception ex, WebSocketClient webSocket);
    }

    public void executeString(final SocketIORequest socketIORequest, final StringCallback stringCallback) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                Request request = new Request.Builder()
                        .url(socketIORequest.getUri())
                        .header("User-Agent", "android-websockets-2.0")
                        .build();

                Debug.logD(TAG, ".executeString socketIORequest.getUri(): " + socketIORequest.getUri());
                try {
                    Response response = okHttpClient.newCall(request).execute();
                    Debug.logD(TAG, ".executeString HttpResponse.getStatusLine(): " + response.message());
                    String responseString = response.body().string();

                    if (stringCallback != null) {
                        stringCallback.onCompleted(null, responseString);
                    }

                } catch (IOException e) {
                    Debug.logE(TAG, ".executeString exception: " + e.getMessage());

                    if (stringCallback != null) {
                        stringCallback.onCompleted(e, null);
                    }
                }
                return null;
            }
        }.execute();
    }

}
