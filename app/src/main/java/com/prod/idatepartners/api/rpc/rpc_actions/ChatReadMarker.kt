package com.prod.idatepartners.api.rpc.rpc_actions

import com.google.gson.annotations.SerializedName

data class ChatReadMarker(
    @SerializedName("toId")
    val drivenId: String,
    @SerializedName("fromUserId")
    val userId: String
)
