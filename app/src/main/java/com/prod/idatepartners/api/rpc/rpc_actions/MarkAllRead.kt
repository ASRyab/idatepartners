package com.prod.idatepartners.api.rpc.rpc_actions

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.prod.idatepartners.api.rpc.RPCAction
import com.prod.idatepartners.api.rpc.RPCResponse

class MarkAllRead(drivenId: String, userId: String) :
    RPCAction<RPCResponse<Any>>("b2bMarkAsRead", mapMarkAllRead(drivenId, userId)) {
    @JvmField
    var response: RPCResponse<Any>? = null

    override fun getResponse(): RPCResponse<Any>? = response

    override fun setResponse(response: RPCResponse<Any>?) {
        this.response = response
    }

}

fun mapMarkAllRead(drivenId: String, userId: String): JsonElement? {
    val params = JsonObject()
    params.add("fromId", JsonPrimitive(drivenId))
    params.add("toId", JsonPrimitive(userId))
    params.add("pageType", JsonPrimitive("privateChat"))
    return params
}