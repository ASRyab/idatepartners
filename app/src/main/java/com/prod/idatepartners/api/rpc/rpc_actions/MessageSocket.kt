package com.prod.idatepartners.api.rpc.rpc_actions

import com.google.gson.annotations.SerializedName
import com.prod.idatepartners.api.models.chat.PrivatChatImage

data class MessageSocket(
    @SerializedName("date")
    val date: String = "",
    @SerializedName("subject")
    val subject: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("activityId")
    val activityId: String = "",
    @SerializedName("text")
    val text: String = "",
    @SerializedName("messageId")
    val messageId: String = "",
    @SerializedName("firstName")
    val firstName: String = "",
    @SerializedName("toLogin")
    val toLogin: String = "",
    @SerializedName("toId")
    val toId: String = "",
    @SerializedName("msgType")
    val msgType: String = "",
    @SerializedName("hasImage")
    val hasImage: Boolean = false,
    @SerializedName("login")
    val login: String = "",
    @SerializedName("timestamp")
    val timestamp: Long = 0,
    @SerializedName("fromId")
    val fromId: String = "",
    @SerializedName("time")
    val time: String = "",
    @SerializedName("resources")
    val resources: PrivatChatImage?
)