package com.prod.idatepartners.api.rpc.rpc_actions

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.prod.idatepartners.api.models.chat.MessageEcho
import com.prod.idatepartners.api.rpc.RPCAction
import com.prod.idatepartners.api.rpc.RPCResponse
import com.prod.idatepartners.screens.choosePhoto.ContentView

data class SendSocketMessage(
    val fromId: String,
    val toId: String,
    val message: String,
    val subject: String,
    val messageType: String,
    val contentView: ContentView?
) :
    RPCAction<RPCResponse<MessageEcho>>(
        "b2bMail",
        mapSendSocketMessage(fromId, toId, message, subject, messageType, contentView)
    ) {
    @JvmField
    var response: RPCResponse<MessageEcho>? = null

    override fun getResponse(): RPCResponse<MessageEcho>? {
        return response
    }

    override fun setResponse(response: RPCResponse<MessageEcho>?) {
        this.response = response
    }
}

fun mapSendSocketMessage(
    fromId: String,
    toId: String,
    message: String,
    subject: String,
    messageType: String,
    contentView: ContentView?
): JsonElement? {
    val params = JsonObject()
    params.add("toId", JsonPrimitive(toId))
    params.add("fromId", JsonPrimitive(fromId))
    params.add("subject", JsonPrimitive(subject))
    params.add("message", JsonPrimitive(message))
    params.add("messageType", JsonPrimitive(messageType))
    if (contentView != null) {
        val image = JsonObject()
        image.add("id", JsonPrimitive(contentView.contentId))
        image.add("imageId", JsonPrimitive(contentView.contentId))
        image.add("imgUrl", JsonPrimitive(contentView.url))

        val privateChatImage = JsonObject()
        privateChatImage.add("privateChatImage", image)
        params.add("resources", privateChatImage)
    }
    return params
}
