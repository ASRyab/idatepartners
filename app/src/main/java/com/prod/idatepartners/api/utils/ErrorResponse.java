package com.prod.idatepartners.api.utils;

import com.prod.idatepartners.api.models.Meta;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Response;

public class ErrorResponse extends Exception {

    private ErrorResponse(Meta meta, Kind kind) {
        this.meta = meta;
        this.kind = kind;
    }

    public static String getServerMessage(Object throwable) {
        if (throwable instanceof ErrorResponse) {
            return ((ErrorResponse) throwable).getServerMessage();
        } else if (throwable instanceof Throwable) {
            return ((Throwable) throwable).getMessage();
        }
        return "";
    }


    public static ErrorResponse httpError(String url, Response response) {
        String message = response.code() + " " + response.message();
        return new ErrorResponse(message, url, response, ErrorResponse.Kind.HTTP, null);
    }

    public static ErrorResponse networkError(IOException exception) {
        return new ErrorResponse(exception.getMessage(), null, null, ErrorResponse.Kind.NETWORK, exception);
    }

    public static ErrorResponse unexpectedError(Throwable exception) {
        return new ErrorResponse(exception.getMessage(), null, null, ErrorResponse.Kind.UNEXPECTED, exception);
    }

    public static ErrorResponse parseError(Exception exception) {
        return new ErrorResponse(exception.getMessage(), null, null, Kind.SERVER_BULLSHIT, exception);
    }

    public static ErrorResponse serverError(Meta meta) {
        return new ErrorResponse(meta, Kind.SERVER_ERROR);
    }

    public static ErrorResponse rpcError(String message) {
        return new ErrorResponse(message, null, null, Kind.HTTP, null);
    }

    /**
     * Identifies the event kind which triggered a {@link ErrorResponse}.
     */
    public enum Kind {
        /**
         * An {@link IOException} occurred while communicating to the server.
         */
        NETWORK,
        /**
         * A non-200 HTTP status code was received from the server.
         */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED,
        SERVER_BULLSHIT,
        SERVER_ERROR
    }

    private String url;
    private Response response;
    private ErrorResponse.Kind kind;
    private Meta meta;

    private ErrorResponse(String message, String url, Response response, ErrorResponse.Kind kind, Throwable exception) {
        super(message, exception);
        this.url = url;
        this.response = response;
        this.kind = kind;
        initMeta();
    }

    private void initMeta() {
        HashMap<String, String[]> hashMap = new HashMap<>();
        switch (kind) {
            case HTTP:
                hashMap.put("message", new String[]{"Server error"});
                break;
            case NETWORK:
                hashMap.put("message", new String[]{"NETWORK doesn't work"});
                break;
            case UNEXPECTED:
                hashMap.put("message", new String[]{"Unexpected error "});
                break;
            case SERVER_BULLSHIT:
                hashMap.put("message", new String[]{"Sorry, but server is broken"});
                break;
        }
        meta = new Meta(-1, "", hashMap);
    }

    /**
     * The request URL which produced the error.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Response object containing status code, headers, body, etc.
     */
    public Response getResponse() {
        return response;
    }

    /**
     * The event kind which triggered this error.
     */
    public ErrorResponse.Kind getKind() {
        return kind;
    }

    /**
     * The Retrofit this request was executed on
     */
//    public Retrofit getRetrofit() {
//        return retrofit;
//    }
    public Meta getMeta() {
        return meta;
    }

    public String getServerMessage() {
        if (meta.getCode() == 481 && meta.getFirstMessageByKey("manager") != null)
            return "You can enter only under Agency manager role";
        return meta.getFirstMessage();
    }
}
