package com.prod.idatepartners.api.utils;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;

import static com.prod.idatepartners.api.utils.Headers.AUTHORIZATION_HEADER;
import static com.prod.idatepartners.api.utils.Headers.AUTHORIZATION_PREFIX;
import static com.prod.idatepartners.api.utils.Headers.DEVICE_ID_HEADER;
import static com.prod.idatepartners.api.utils.Headers.HTTP_ACCEPT_LANGUAGE;
import static com.prod.idatepartners.api.utils.Headers.REFRESH_TOKEN_KEY;
import static com.prod.idatepartners.api.utils.Headers.USER_AGENT;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@StringDef({
        AUTHORIZATION_HEADER,
        AUTHORIZATION_PREFIX,
        HTTP_ACCEPT_LANGUAGE,
        REFRESH_TOKEN_KEY,
        USER_AGENT,
        DEVICE_ID_HEADER
})
public @interface Headers {
    String AUTHORIZATION_HEADER = "Authorization";
    String AUTHORIZATION_PREFIX = "Bearer ";
    String HTTP_ACCEPT_LANGUAGE = "Accept-Language";
    String REFRESH_TOKEN_KEY = "refresh_token_key";
    String USER_AGENT = "User-Agent";
    String DEVICE_ID_HEADER = "App-DeviceId";
}
