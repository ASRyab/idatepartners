package com.prod.idatepartners.api.utils

import android.text.TextUtils
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class HeadersInterceptor internal constructor(private val headers: MutableMap<String, String>) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        for ((key, value) in headers) {
            if (key == "Content-Type" && !TextUtils.isEmpty(chain.request().header("Content-Type"))) {
                continue
            }
            requestBuilder.removeHeader(key)
            requestBuilder.addHeader(key, value)
        }

        return chain.proceed(requestBuilder.build())
    }

    fun addHeader(key: String, value: String) {
        headers[key] = value
    }

    fun removeHeader(key: String) {
        headers.remove(key)
    }
}
