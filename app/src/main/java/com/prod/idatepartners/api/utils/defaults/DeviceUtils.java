package com.prod.idatepartners.api.utils.defaults;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;

import java.util.UUID;
import java.util.zip.CRC32;

public class DeviceUtils {
    private static String packageName = "";

    public static String createDeviceId(Context context) {
        String macAddress = "", tmDevice = "", tmSerial = "";
        final String androidId = android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        try {
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo wInfo;
            if (wifiManager != null && (wInfo = wifiManager.getConnectionInfo()) != null) {
                macAddress = "" + wInfo.getMacAddress();
            } else {
                macAddress = "" + androidId;
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
        }

        if (TextUtils.isEmpty(macAddress)) {
            macAddress = "" + androidId;
        }
        if (TextUtils.isEmpty(tmDevice)) {
            return "";
        }
        if (TextUtils.isEmpty(tmSerial)) {
            return "";
        }
        macAddress += getDeviceName();
        UUID deviceUuid = new UUID(macAddress.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return "FF" + deviceUuid.toString();
    }

    /**
     * Method for generating device ID on multisite
     * @param context
     * @return deviceID
     */
    public static String getDeviceIdCRC32(Context context) {
        final String androidId;
        String tmSerial = "", tmDevice = "";
        androidId = ""
                + android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);

        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
        }
        if (TextUtils.isEmpty(tmDevice)) {
            return "";
        }
        if (TextUtils.isEmpty(tmSerial)) {
            return "";
        }
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        CRC32 crc32 = new CRC32();
        crc32.update(deviceId.getBytes());

        return String.valueOf(crc32.getValue());
    }

    public static void setPackageName(String name){
        packageName = name;
    }

    public static String getDeviceIdHex(Context context) {
        final String androidId;
        final String serial;

        androidId = "" +    android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);

        serial = Build.SERIAL;

        UUID deviceUuid = new UUID(androidId.hashCode(), serial.hashCode());
        String deviceId = deviceUuid.toString().replace("-", "");

        return deviceId;
    }

    private static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String getPackageInfo(Context context, PackageInfoField packageInfoField) {
        String result = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager==null) return null;
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            switch (packageInfoField) {
                case APP_VERSION:
                    result = packageInfo.versionName;
                    break;
                case BUNDLE_ID:
                    if(packageName.isEmpty()){
                        result = packageInfo.packageName;
                    } else {
                        result = packageName;
                    }
                    break;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getDeviceWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public enum PackageInfoField {
        APP_VERSION, BUNDLE_ID
    }
}