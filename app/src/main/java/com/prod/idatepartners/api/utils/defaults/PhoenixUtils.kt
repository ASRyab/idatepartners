package com.prod.idatepartners.api.utils.defaults

import android.content.Context
import android.content.pm.PackageManager
import android.webkit.WebSettings
import java.security.NoSuchAlgorithmException
import java.util.*

object PhoenixUtils {
    fun getMDFive(key: String): String {
        try {
            // Create MD5 Hash
            val digest = java.security.MessageDigest.getInstance("MD5")
            digest.update(key.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuffer()
            for (i in messageDigest.indices) {
                hexString.append(String.format("%02x", 0xFF and messageDigest[i].toInt()))
            }
            return hexString.toString()

        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return ""
    }

     fun getUserAgent(context: Context): String {
        val builder = StringBuilder()
        try {
            val versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName
            builder.append(context.getPackageName()).append("/").append(versionName)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        builder.append(" ").append(getUserAgentString(context))
        return builder.toString()
    }

     fun getUserAgentString(context: Context): String {
        return try {
            WebSettings.getDefaultUserAgent(context)
        } catch (e: Exception) {
            e.printStackTrace()
            val userAgent = System.getProperty("http.agent")
            userAgent
                ?: "Mozilla/5.0 (Linux; Android 4.2.2; ALCATEL ONE TOUCH 7041X Build/JDQ39) " +
                "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.141 Mobile Safari/537.36"
        }
    }

     fun getAppKey(): String {
        val calendar = Calendar.getInstance()
        val now = calendar.timeInMillis
        val utc = now / 1000
        val utcString = utc.toString()
        val apiKey = StringBuilder()
        val key = StringBuilder()
        key.append(utcString)
        apiKey.append(PhoenixUtils.getMDFive(key.toString()))
        apiKey.append(java.lang.Long.toHexString(utc))
        return apiKey.toString()

    }

}
