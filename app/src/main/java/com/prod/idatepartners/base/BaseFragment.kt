package com.prod.idatepartners.base

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.prod.idatepartners.App
import com.prod.idatepartners.BR
import com.prod.idatepartners.utils.toast
import javax.inject.Inject

abstract class BaseFragment<T> : Fragment() {
    protected lateinit var binding: ViewDataBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: BaseViewModel

    @LayoutRes
    abstract fun layoutId(): Int

    abstract fun viewModelClass(): Class<T>

    open fun isActivityOwner(): Boolean = false

    private fun getBaseActivity(): BaseActivity {
        return activity as BaseActivity
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component.inject(this as BaseFragment<Any>)
        viewModel = if (isActivityOwner()) {
            ViewModelProviders.of(activity!!, viewModelFactory)
                .get(viewModelClass() as Class<BaseViewModel>)
        } else {
            ViewModelProviders.of(this, viewModelFactory)
                .get(viewModelClass() as Class<BaseViewModel>)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
        binding.lifecycleOwner = this
        binding.setVariable(BR.vm, viewModel)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val progressDialog = createProgressDialog()
        viewModel.isProgress.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressDialog.show()
            } else progressDialog.dismiss()
        })
        viewModel.passBundle(arguments)

        viewModel.errorEvent.observe(viewLifecycleOwner, Observer {
            when (it) {
                is ServerError -> toast(it.errorMessage)
                is NetWorkError -> toast(it.errorMessage)
            }
        })

    }

    private fun createProgressDialog(): ProgressDialog {
        val pd = ProgressDialog(context)
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        pd.setMessage("Wait")
        pd.isIndeterminate = true
        pd.setCancelable(false)
        return pd
    }

    override fun onSaveInstanceState(outState: Bundle) {
        viewModel.onSaveInstanceState(outState)
        super.onSaveInstanceState(outState)
    }

}