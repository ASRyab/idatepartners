package com.prod.idatepartners.base

open class BaseRepository {
    open val TAG: String = this::class.java.simpleName
}