package com.prod.idatepartners.base

open class BaseUseCase {
    open val TAG: String = this::class.java.simpleName
}