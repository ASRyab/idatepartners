package com.prod.idatepartners.base

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.prod.idatepartners.api.utils.ErrorResponse
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val isProgress = MutableLiveData<Boolean>()
    val errorEvent = MutableLiveData<ErrorEvent>()

    init {
        isProgress.value = false
    }

    fun safeDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    open fun passBundle(arguments: Bundle?) {

    }

    fun onSaveInstanceState(outState: Bundle) {
    }

    open fun checkError(throwable: Throwable) {
        if (throwable is ErrorResponse)
            when (throwable.kind) {
                ErrorResponse.Kind.NETWORK -> errorEvent.postValue(
                    NetWorkError(
                        throwable.serverMessage
                    )
                )
                ErrorResponse.Kind.SERVER_ERROR -> errorEvent.postValue(
                    ServerError(
                        throwable.serverMessage, throwable
                    )
                )
                else -> postDefaultError(throwable)
            }
        else postDefaultError(throwable)
    }

    private fun postDefaultError(throwable: Throwable) {
        throwable.message?.let { errorEvent.postValue(ServerError(it, throwable)) }
    }

    open val TAG: String = this::class.java.simpleName
}

sealed class ErrorEvent
data class ServerError(val errorMessage: String, val throwable: Throwable) : ErrorEvent()
data class NetWorkError(val errorMessage: String) : ErrorEvent()