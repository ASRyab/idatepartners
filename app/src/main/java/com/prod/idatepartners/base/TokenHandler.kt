package com.prod.idatepartners.base

import com.prod.idatepartners.api.RequestManager
import com.prod.idatepartners.api.models.BaseModelResponse
import io.reactivex.Single

class TokenHandler(private val requestManager: RequestManager) : ITokenHandler {

    override fun <T : Any> makeRequest(single: Single<BaseModelResponse<T>>): Single<T> =
        single.compose { upstream -> requestManager.requestWithTokenValidation(upstream) }
}

interface ITokenHandler {
    fun <T : Any> makeRequest(single: Single<BaseModelResponse<T>>): Single<T>
}