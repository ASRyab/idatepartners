package com.prod.idatepartners.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.prod.idatepartners.db.users.DrivenUserDao
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.db.users.UserDao
import com.prod.idatepartners.db.users.UserDb

@Database(
    entities = [DrivenUserDb::class, UserDb::class, MessageDb::class, LetterDb::class],
    version = 1
)
@TypeConverters(TypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun drivenUsers(): DrivenUserDao
    abstract fun users(): UserDao
    abstract fun chat(): MessageDao
    abstract fun letters(): LetterDao

}