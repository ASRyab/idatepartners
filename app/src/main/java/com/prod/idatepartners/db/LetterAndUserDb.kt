package com.prod.idatepartners.db

import androidx.room.Embedded
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.db.users.UserDb


data class LetterAndUserDb(
    @Embedded
    var user: UserDb?,
    @Embedded
    var drivenUser: DrivenUserDb?,
    @Embedded
    var letter: LetterDb?
)
