package com.prod.idatepartners.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable

@Dao
interface LetterDao {

    @Query(
        sqlRequest +
                "WHERE (senderId=:drivenId AND recipientId=:userId) " +
                "OR (senderId=:userId AND recipientId=:drivenId) " +
                "ORDER BY timestamp DESC"
    )
    fun getAllPager(drivenId: String, userId: String): DataSource.Factory<Int, LetterAndUserDb>

    @Query(
        sqlRequest + "WHERE letter_id=:letterId LIMIT 1"
    )
    fun getLetterByIdWithUser(
        drivenId: String,
        userId: String,
        letterId: String
    ): Observable<LetterAndUserDb>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: LetterDb)

    @Query("DELETE FROM LetterDb")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<LetterDb>)

    companion object {
        private const val sqlRequest =
            "SELECT LetterDb.*, UserDb.*, DrivenUserDb.* , " +
                    "LetterDb.id AS letter_id FROM LetterDb " +
                    "INNER JOIN UserDb ON UserDb.userId =:userId AND UserDb.drivenId=:drivenId " +
                    "INNER JOIN DrivenUserDb ON DrivenUserDb.driven_id=:drivenId "
    }
}
