package com.prod.idatepartners.db

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.prod.idatepartners.api.models.LetterPhoto

@Entity(tableName = "LetterDb")
data class LetterDb(
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,
    @ColumnInfo(name = "senderId")
    val senderId: String,
    @ColumnInfo(name = "recipientId")
    val recipientId: String,
    @ColumnInfo(name = "isRead")
    var isRead: Boolean?,
    @ColumnInfo(name = "subject")
    val subject: String?,
    @ColumnInfo(name = "type")
    val type: String?,
    @ColumnInfo(name = "replyToId")
    val replyToId: String?,
    @ColumnInfo(name = "timestamp")
    val timestamp: Long?,
    @ColumnInfo(name = "direction")
    val direction: String,
    @ColumnInfo(name = "senderName")
    val senderName: String?,
    @ColumnInfo(name = "hasRepliedSubject")
    val hasRepliedSubject: Boolean?,
    @ColumnInfo(name = "text")
    val text: String?,
    @ColumnInfo(name = "images")
    val images: List<LetterPhoto>?
) {
    fun isIncoming(): Boolean = direction == "in"
}