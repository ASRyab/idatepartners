package com.prod.idatepartners.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface MessageDao {

    @Query(sqlRequest)
    fun getAll(drivenId: String, userId: String): Single<MutableList<MessageDb>>

    @Query(sqlRequest)
    fun getAllPager(drivenId: String, userId: String): DataSource.Factory<Int, MessageDb>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: MessageDb)

    @Query("DELETE FROM MessageDb")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<MessageDb>)


    companion object {
        private const val sqlRequest =
            "SELECT * FROM MessageDb WHERE (senderId=:drivenId AND recipientId=:userId) OR (senderId=:userId AND recipientId=:drivenId) ORDER BY timestamp DESC"
    }
}