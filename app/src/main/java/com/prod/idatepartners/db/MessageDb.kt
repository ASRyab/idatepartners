package com.prod.idatepartners.db

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.prod.idatepartners.api.models.chat.ChatPhoto
import com.prod.idatepartners.utils.ConvertingUtils
import com.prod.idatepartners.utils.ConvertingUtils.Companion.DATE_FORMAT_ONLY_DATE
import com.prod.idatepartners.utils.ConvertingUtils.Companion.DATE_FORMAT_TIME

@Entity(tableName = "MessageDb")
data class MessageDb(
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,
    @ColumnInfo(name = "senderId")
    val senderId: String,
    @ColumnInfo(name = "recipientId")
    val recipientId: String,
    @ColumnInfo(name = "viewedAt")
    val viewedAt: String?,
    @ColumnInfo(name = "isRead")
    var isRead: Boolean = false,
    @ColumnInfo(name = "subject")
    val subject: String?,
    @ColumnInfo(name = "text")
    var text: String?,
    @ColumnInfo(name = "msgType")
    var msgType: String,
    @ColumnInfo(name = "type")
    val type: String?,
    @ColumnInfo(name = "timestamp")
    val timestamp: Long?,
    @ColumnInfo(name = "chatImages")
    val chatImages: List<ChatPhoto>?
) {
    fun getTime() = ConvertingUtils.getStringDateByPattern(this.timestamp, DATE_FORMAT_TIME)
    fun getDate() = ConvertingUtils.getStringDateByPattern(this.timestamp, DATE_FORMAT_ONLY_DATE)
    fun getImageUrl() = chatImages?.first()?.imgUrl

}