package com.prod.idatepartners.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.prod.idatepartners.api.models.LetterPhoto
import com.prod.idatepartners.api.models.chat.ChatPhoto
import com.prod.idatepartners.api.models.profiles.DrivenContent
import com.prod.idatepartners.api.models.profiles.Photo

class TypeConverter {
    companion object {
        @TypeConverter
        @JvmStatic
        fun toLetterPhoto(photos: List<LetterPhoto>): String? {
            return Gson().toJson(photos)
        }

        @TypeConverter
        @JvmStatic
        fun fromLetterPhoto(photos: String): List<LetterPhoto>? {
            return Gson().fromJson(photos)

        }

        @TypeConverter
        @JvmStatic
        fun toChatPhoto(photos: List<ChatPhoto>?): String? {
            return Gson().toJson(photos)
        }

        @TypeConverter
        @JvmStatic
        fun fromChatPhoto(photos: String): List<ChatPhoto>? {
            return Gson().fromJson(photos)

        }

        @TypeConverter
        @JvmStatic
        fun toDrivenContent(content: List<DrivenContent>?): String? {
            return Gson().toJson(content)
        }

        @TypeConverter
        @JvmStatic
        fun fromDrivenContent(photos: String): List<DrivenContent>? {
            return Gson().fromJson(photos)

        }

        @TypeConverter
        @JvmStatic
        fun toPhoto(photos: List<Photo>): String? {
            return Gson().toJson(photos)
        }

        @TypeConverter
        @JvmStatic
        fun fromPhoto(photos: String): List<Photo>? {
            return Gson().fromJson(photos)

        }
    }
}

inline fun <reified T> Gson.fromJson(json: String): T =
    this.fromJson<T>(json, object : TypeToken<T>() {}.type)
