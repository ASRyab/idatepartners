package com.prod.idatepartners.db.users

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable

@Dao
interface DrivenUserDao {
    @Query("SELECT * FROM DrivenUserDb ORDER BY driven_id ASC")
    fun getAllObservable(): Observable<MutableList<DrivenUserDb>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: DrivenUserDb)

    @Query("DELETE FROM DrivenUserDb")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: MutableList<DrivenUserDb>)

    @Query("SELECT * FROM DrivenUserDb WHERE driven_id=:drivenUserId LIMIT 1")
    fun getById(drivenUserId: String): Observable<DrivenUserDb>

}