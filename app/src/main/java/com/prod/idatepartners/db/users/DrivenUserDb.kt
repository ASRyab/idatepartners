package com.prod.idatepartners.db.users

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.prod.idatepartners.api.models.profiles.DrivenContent
import com.prod.idatepartners.api.models.profiles.Photo

@Entity(tableName = "DrivenUserDb")
data class DrivenUserDb(
    @Embedded(prefix = "dr_")
    val primaryPhoto: Photo,
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "driven_id")
    val id: String,
    @ColumnInfo(name = "driven_login")
    val login: String?,
    @ColumnInfo(name = "driven_firstName")
    val firstName: String?,
    @ColumnInfo(name = "driven_lastName")
    val lastName: String?,
    @ColumnInfo(name = "driven_gender")
    val gender: String?,
    @ColumnInfo(name = "driven_age")
    val age: Int?,
    @ColumnInfo(name = "driven_country")
    val country: String?,
    @ColumnInfo(name = "driven_city")
    val city: String?,
    @ColumnInfo(name = "driven_registeredAt")
    val registeredAt: String?,
    @ColumnInfo(name = "driven_countUnread")
    var countUnread: Int = 0,
    @ColumnInfo(name = "driven_content")
    val content: List<DrivenContent>?,
    @ColumnInfo(name = "driven_isHidden")
    var isHidden: Boolean = false
) {
    fun getShortInfo(): String = getFullName() + ", " + this.age
    fun getFullName(): String = this.firstName + " " + this.lastName
}
