package com.prod.idatepartners.db.users

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable

@Dao
interface UserDao {

    @Query("SELECT * FROM UserDb WHERE drivenId=:id ORDER BY lastMessageTime DESC")
    fun getUsersByDriven(id: String): Observable<MutableList<UserDb>>

    @Query("SELECT * FROM UserDb WHERE userId=:id AND drivenId=:drivenUserId limit 1")
    fun getUsersById(id: String, drivenUserId: String): Observable<UserDb>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: UserDb)

    @Query("DELETE FROM UserDb")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<UserDb>): List<Long>
}