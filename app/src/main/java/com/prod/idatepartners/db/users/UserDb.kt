package com.prod.idatepartners.db.users

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.prod.idatepartners.api.models.profiles.Photo

@Entity(tableName = "UserDb")
data class UserDb(
    @Embedded
    val primaryPhoto: Photo?,
    @ColumnInfo(name = "build")
    val build: Int?,
    @ColumnInfo(name = "height")
    val height: Int?,
    @ColumnInfo(name = "weight")
    val weight: Int?,
    @ColumnInfo(name = "education")
    val education: Int?,
    @ColumnInfo(name = "hobbiesAndInterests")
    val hobbiesAndInterests: String?,
    @ColumnInfo(name = "userId")
    val userId: String,
    @ColumnInfo(name = "login")
    val login: String?,
    @ColumnInfo(name = "firstName")
    val firstName: String?,
    @ColumnInfo(name = "lastName")
    val lastName: String?,
    @ColumnInfo(name = "gender")
    val gender: String?,
    @ColumnInfo(name = "age")
    val age: Int?,
    @ColumnInfo(name = "country")
    val country: String?,
    @ColumnInfo(name = "city")
    val city: String?,
    @ColumnInfo(name = "registeredAt")
    val registeredAt: String?,
    @ColumnInfo(name = "isFriend")
    val isFriend: Boolean?,
    @ColumnInfo(name = "isOnline")
    val isOnline: Boolean?,
    @ColumnInfo(name = "drivenId")
    val drivenId: String?,
    @ColumnInfo(name = "photos")
    val photos: List<Photo>?,
    @ColumnInfo(name = "letterCount")
    var letterCount: Int = 0,
    @ColumnInfo(name = "mailCount")
    var mailCount: Int = 0,
    @ColumnInfo(name = "lastMessageTime")
    var lastMessageTime: Long? = 0
) {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "_id")
    var id: String? = drivenId + userId

    fun getShortInfo(): String = getFullName() + ", " + this.age
    fun getFullName(): String = this.firstName + " " + this.lastName
}
