package com.prod.idatepartners.di.components

import com.prod.idatepartners.App
import com.prod.idatepartners.base.BaseFragment
import com.prod.idatepartners.di.modules.*
import com.prod.idatepartners.screens.main.MainActivity
import com.prod.idatepartners.screens.main.UserViewHolder
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(
    modules = [ConfigModule::class,
        ApiModule::class,
        DatabaseModule::class,
        ContextModule::class,
        RepositoryModule::class,
        UseCaseModule::class,
        SharedPrefModule::class,
        VMBindingModule::class,
        UtilsModule::class]
)
interface AppComponent {
    fun inject(app: App)
    fun inject(app: BaseFragment<Any>)
    fun inject(app: MainActivity)
    fun inject(app: UserViewHolder)
}