package com.prod.idatepartners.di.modules

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.*
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import com.prod.idatepartners.api.Config
import com.prod.idatepartners.api.LoginRequest
import com.prod.idatepartners.api.RequestManager
import com.prod.idatepartners.api.deserializers.DateTypeDeserializer
import com.prod.idatepartners.api.models.Letters
import com.prod.idatepartners.api.models.profiles.Counters
import com.prod.idatepartners.api.requests.ChatApi
import com.prod.idatepartners.api.requests.GiftsApi
import com.prod.idatepartners.api.requests.LetterApi
import com.prod.idatepartners.api.requests.UsersApi
import com.prod.idatepartners.api.rpc.ISocketManager
import com.prod.idatepartners.api.rpc.SocketManager
import com.prod.idatepartners.api.utils.Headers
import com.prod.idatepartners.api.utils.HeadersInterceptor
import com.prod.idatepartners.api.utils.RxErrorHandlingCallAdapterFactory
import com.prod.idatepartners.api.utils.defaults.DeviceUtils.getDeviceIdHex
import com.prod.idatepartners.api.utils.defaults.PhoenixUtils.getUserAgent
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.base.TokenHandler
import com.prod.idatepartners.utils.PreferenceManager
import com.prod.idatepartners.utils.UiHandler
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ContextModule::class, SharedPrefModule::class])
class ApiModule {

    @Provides
    @Singleton
    fun retrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory,
        rxJava2CallAdapterFactory: RxJava2CallAdapterFactory,
        config: Config
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(config.getBaseUrl())
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun gson(): Gson {
        val builder = GsonBuilder()
        builder.registerTypeAdapter(Date::class.java, DateTypeDeserializer())

        builder.registerTypeAdapter(Counters::class.java, object : JsonDeserializer<Counters> {
            override fun deserialize(
                json: JsonElement?,
                typeOfT: Type?,
                context: JsonDeserializationContext?
            ): Counters {
                if (json == null) return Counters.EMPTY_COUNTERS
                return if (json.isJsonArray) Counters.EMPTY_COUNTERS else Gson().fromJson<Counters>(
                    json,
                    typeOfT
                )
            }
        })
        builder.registerTypeAdapter(Letters::class.java, object : JsonDeserializer<Letters> {
            override fun deserialize(
                json: JsonElement?,
                typeOfT: Type?,
                context: JsonDeserializationContext?
            ): Letters {
                if (json == null) return Letters.EMPTY_LETTERS
                return if (json.isJsonArray) Letters.EMPTY_LETTERS else Gson().fromJson<Letters>(
                    json,
                    typeOfT
                )
            }
        })
        builder.setLenient()
        return builder.create()
    }

    @Provides
    @Singleton
    fun gsonConverterFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

    @Provides
    @Singleton
    fun callAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun getHeadersInterceptor(context: Context): HeadersInterceptor {
        val headersMap = ConcurrentHashMap<String, String>()
        headersMap.put(Headers.USER_AGENT, getUserAgent(context))
        headersMap.put(Headers.HTTP_ACCEPT_LANGUAGE, Locale.getDefault().language + ";q=1")
        headersMap.put("Content-Type", "application/x-www-form-urlencoded")
        headersMap.put("X-Requested-With", "XMLHttpRequest")
        headersMap.put("App-Marker", "hand3856be45")
        headersMap.put(Headers.DEVICE_ID_HEADER, getDeviceIdHex(context))
        return HeadersInterceptor(headersMap)
    }

    @Provides
    @Singleton
    fun okHttpClient(
        okHttpProfilerInterceptor: OkHttpProfilerInterceptor,
        headersInterceptor: HeadersInterceptor, stethoInterceptor: StethoInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(headersInterceptor)
            .addInterceptor(okHttpProfilerInterceptor)
            .addNetworkInterceptor(stethoInterceptor)
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun okHttpProfilerInterceptor(): OkHttpProfilerInterceptor {
        return OkHttpProfilerInterceptor()
    }

    @Provides
    @Singleton
    fun okHttpStethoInterceptor(): StethoInterceptor {
        return StethoInterceptor()
    }

    @Provides
    @Singleton
    fun getRequestManager(
        context: Context
        , retrofit: Retrofit
        , preferenceManager: PreferenceManager
        , headersInterceptor: HeadersInterceptor
        , socketManager: ISocketManager
        , uiHandler: UiHandler
    ): RequestManager =
        RequestManager(
            context,
            retrofit,
            preferenceManager,
            headersInterceptor,
            socketManager,
            uiHandler
        )

    @Provides
    @Singleton
    fun getTestRequest(
        requestManager: RequestManager
    ): LoginRequest = LoginRequest(requestManager)

    @Provides
    @Singleton
    fun getSocketManager(
        config: Config,
        okHttpClient: OkHttpClient,
        uiHandler: UiHandler,
        gson: Gson
    ): ISocketManager = SocketManager.Builder()
        .setOkHttpClient(okHttpClient)
        .setHaveUIObservable(uiHandler.haveUiSubject)
        .setGson(gson)
        .setServerUrl(config.getBaseUrl())
        .setSocketPort(443)
        .create()

    @Provides
    @Singleton
    fun getUsersApi(requestManager: RequestManager): UsersApi =
        requestManager.retrofit.create(UsersApi::class.java)

    @Provides
    @Singleton
    fun getTokenHandler(requestManager: RequestManager): ITokenHandler = TokenHandler(requestManager)

    @Provides
    @Singleton
    fun getChatApi(requestManager: RequestManager): ChatApi =
        requestManager.retrofit.create(ChatApi::class.java)

    @Provides
    @Singleton
    fun getLetterApi(requestManager: RequestManager): LetterApi =
        requestManager.retrofit.create(LetterApi::class.java)

    @Provides
    @Singleton
    fun getPresentApi(requestManager: RequestManager): GiftsApi =
        requestManager.retrofit.create(GiftsApi::class.java)

}