package com.prod.idatepartners.di.modules

import android.content.Context
import com.prod.idatepartners.api.Config
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ConfigModule {
    @Provides
    @Singleton
    fun getConfig(context: Context): Config {
        return Config(context)
    }
}