package com.prod.idatepartners.di.modules

import android.content.Context
import androidx.room.Room
import com.prod.idatepartners.db.AppDatabase
import com.prod.idatepartners.db.LetterDao
import com.prod.idatepartners.db.MessageDao
import com.prod.idatepartners.db.users.DrivenUserDao
import com.prod.idatepartners.db.users.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDb(appContext: Context): AppDatabase {
        return Room.databaseBuilder(appContext, AppDatabase::class.java, "IDatePartners.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideDrivenUserDb(appDatabase: AppDatabase): DrivenUserDao {
        return appDatabase.drivenUsers()
    }

    @Provides
    @Singleton
    fun provideUserDb(appDatabase: AppDatabase): UserDao {
        return appDatabase.users()
    }

    @Provides
    @Singleton
    fun provideChatDb(appDatabase: AppDatabase): MessageDao {
        return appDatabase.chat()
    }

    @Provides
    @Singleton
    fun provideLetterDb(appDatabase: AppDatabase): LetterDao {
        return appDatabase.letters()
    }
}