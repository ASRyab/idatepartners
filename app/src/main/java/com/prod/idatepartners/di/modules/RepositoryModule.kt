package com.prod.idatepartners.di.modules

import com.prod.idatepartners.api.LoginRequest
import com.prod.idatepartners.api.requests.ChatApi
import com.prod.idatepartners.api.requests.GiftsApi
import com.prod.idatepartners.api.requests.LetterApi
import com.prod.idatepartners.api.requests.UsersApi
import com.prod.idatepartners.api.rpc.ISocketManager
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.LetterDao
import com.prod.idatepartners.db.MessageDao
import com.prod.idatepartners.db.users.DrivenUserDao
import com.prod.idatepartners.db.users.UserDao
import com.prod.idatepartners.repo.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun getLoginRepository(loginRequest: LoginRequest): LoginRepository =
        LoginRepository(loginRequest)


    @Provides
    @Singleton
    fun getDrivenUserRepository(
        usersApi: UsersApi,
        tokenHandler: ITokenHandler,
        drivenUserDao: DrivenUserDao
    ): IDrivenUserRepository = DrivenUserRepository(usersApi, tokenHandler, drivenUserDao)

    @Provides
    @Singleton
    fun getUserRepository(
        usersApi: UsersApi,
        tokenHandler: ITokenHandler,
        userDao: UserDao
    ): IUserRepository = UserRepository(usersApi, tokenHandler, userDao)

    @Provides
    @Singleton
    fun getChatRepository(
        chatApi: ChatApi,
        tokenHandler: ITokenHandler,
        messageDao: MessageDao,
        userDao: UserDao,
        socketManager: ISocketManager
    ): IChatRepository = ChatRepository(chatApi, tokenHandler, messageDao, userDao, socketManager)

    @Provides
    @Singleton
    fun getLettersRepository(
        letterApi: LetterApi,
        tokenHandler: ITokenHandler,
        listDao: LetterDao, socketManager: ISocketManager
    ): ILettersRepository = LettersRepository(letterApi, tokenHandler, listDao, socketManager)

    @Provides
    @Singleton
    fun getGiftsRepository(tokenHandler: ITokenHandler, giftsApi: GiftsApi): GiftRepository =
        GiftRepository(tokenHandler, giftsApi)
}