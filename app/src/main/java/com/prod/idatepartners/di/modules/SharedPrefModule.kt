package com.prod.idatepartners.di.modules

import android.content.Context
import com.google.gson.Gson
import com.prod.idatepartners.utils.PreferenceManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPrefModule {

    @Provides
    @Singleton
    fun getPreferenceManager(context: Context, gson: Gson): PreferenceManager = PreferenceManager(context, gson)
}