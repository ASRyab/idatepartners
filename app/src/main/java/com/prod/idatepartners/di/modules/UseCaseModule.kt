package com.prod.idatepartners.di.modules

import com.prod.idatepartners.api.RequestManager
import com.prod.idatepartners.repo.*
import com.prod.idatepartners.usecases.*
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    fun getLoginUseCase(loginRepository: LoginRepository): ILoginUseCase =
        LoginUseCase(loginRepository)

    @Provides
    fun getAutoLoginUseCase(requestManager: RequestManager): IAutoLoginUseCase =
        AutoLoginUseCase(requestManager)

    @Provides
    fun getUsersUseCase(
        drivenUserRepository: IDrivenUserRepository,
        userRepository: IUserRepository
    ): IUsersUseCase =
        UsersUseCase(drivenUserRepository, userRepository)

    @Provides
    fun getChatUseCase(chatRepository: IChatRepository): ChatUseCase =
        ChatUseCase(chatRepository)

    @Provides
    fun getLettersUseCase(
        lettersRepository: ILettersRepository,
        drivenUserRepository: IDrivenUserRepository,
        userRepository: IUserRepository
    ): ILetterUseCase =
        LetterUseCase(lettersRepository, userRepository, drivenUserRepository)

    @Provides
    fun getSocketUseCase(
        chatRepository: IChatRepository,
        userRepository: IUserRepository,
        drivenUserRepository: IDrivenUserRepository
    ): ISocketChatUseCase =
        SocketChatUseCase(chatRepository, userRepository, drivenUserRepository)

    @Provides
    fun getGiftUseCase(
        giftRepository: GiftRepository
    ): IGiftUseCase =
        GiftUseCase(giftRepository)


    @Provides
    fun getLogoutUseCase(
        requestManager: RequestManager,
        chatRepository: IChatRepository,
        drivenUserRepository: IDrivenUserRepository,
        userRepository: IUserRepository,
        lettersRepository: ILettersRepository
    ): ILogoutUseCase =
        LogoutUseCase(
            requestManager,
            chatRepository,
            drivenUserRepository,
            userRepository,
            lettersRepository
        )
}