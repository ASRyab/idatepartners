package com.prod.idatepartners.di.modules

import android.content.Context
import android.widget.TextView
import com.prod.idatepartners.utils.UiHandler
import com.prod.idatepartners.utils.UserFieldsValidator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilsModule {

    @Provides
    @Singleton
    fun provideUserFieldsValidator(context: Context) = UserFieldsValidator(context)

    @Provides
    @Singleton
    fun provideUIHandler() = UiHandler()

    @Provides
    @Singleton
    fun getTextView(context: Context) = TextView(context)

}