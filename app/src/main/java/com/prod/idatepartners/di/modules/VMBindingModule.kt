package com.prod.idatepartners.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.prod.idatepartners.base.ViewModelFactory
import com.prod.idatepartners.di.annotation.ViewModelKey
import com.prod.idatepartners.screens.choosePhoto.ChoosePhotoVM
import com.prod.idatepartners.screens.letterDetail.LetterDetailVM
import com.prod.idatepartners.screens.login.LoginVM
import com.prod.idatepartners.screens.main.LettersVM
import com.prod.idatepartners.screens.main.MainVM
import com.prod.idatepartners.screens.main.chat.ChatVM
import com.prod.idatepartners.screens.newLetter.NewLetterVM
import com.prod.idatepartners.screens.userDetail.UserDetailVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class VMBindingModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainVM::class)
    internal abstract fun bindMainVM(viewModel: MainVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginVM::class)
    internal abstract fun bindLoginVM(viewModel: LoginVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChatVM::class)
    internal abstract fun bindChatVM(viewModel: ChatVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LettersVM::class)
    internal abstract fun bindLetterVM(viewModel: LettersVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailVM::class)
    internal abstract fun userDetailVM(viewModel: UserDetailVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LetterDetailVM::class)
    internal abstract fun letterDetailVM(viewModel: LetterDetailVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewLetterVM::class)
    internal abstract fun newLetterVM(viewModel: NewLetterVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChoosePhotoVM::class)
    internal abstract fun choosePhoto(viewModel: ChoosePhotoVM): ViewModel

}