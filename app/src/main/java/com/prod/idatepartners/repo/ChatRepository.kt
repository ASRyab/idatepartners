package com.prod.idatepartners.repo

import android.text.TextUtils
import androidx.paging.DataSource
import com.prod.idatepartners.api.models.chat.ChatPhoto
import com.prod.idatepartners.api.requests.ChatApi
import com.prod.idatepartners.api.rpc.ISocketManager
import com.prod.idatepartners.api.rpc.rpc_actions.MarkAllRead
import com.prod.idatepartners.api.rpc.rpc_actions.SendSocketMessage
import com.prod.idatepartners.base.BaseRepository
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.MessageDao
import com.prod.idatepartners.db.MessageDb
import com.prod.idatepartners.db.users.UserDao
import com.prod.idatepartners.screens.choosePhoto.ContentView
import com.prod.idatepartners.utils.RxUtils
import com.prod.idatepartners.utils.toDb
import com.prod.idatepartners.utils.toMessageDb
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ChatRepository(
    private val chatApi: ChatApi,
    private val tokenHandler: ITokenHandler,
    private val dao: MessageDao,
    private val userDao: UserDao,
    private val socketManager: ISocketManager
) : BaseRepository(), IChatRepository {

    private fun getChatHistoryApi(
        modelId: String,
        userId: String,
        offset: Int
    ): Single<MutableList<MessageDb>> =
        tokenHandler.makeRequest(
            chatApi.getChatHistory(
                userId,
                mapOf(Pair("from_id", modelId), Pair("offset", offset.toString()))
            )
        ).map { it.list }
            .flatMap {
                Observable.fromIterable(it)
                    .map { it.toDb() }
                    .toList()
            }
            .doOnSuccess { dao.insertAll(it) }

    override fun getChatHistoryPaged(
        drivenUserId: String,
        userId: String
    ): DataSource.Factory<Int, MessageDb> {
        loadNext(drivenUserId, userId, 0)
        return dao.getAllPager(drivenUserId, userId)
    }

    override fun loadNext(drivenUserId: String, userId: String, offset: Int) {
        if (!TextUtils.isEmpty(drivenUserId) && !TextUtils.isEmpty(userId))
            getChatHistoryApi(drivenUserId, userId, offset).subscribe(
                RxUtils.emptyDataConsumer,
                RxUtils.getEmptyErrorConsumer(TAG, "loadNext")
            )
    }


    override fun clear() {
        dao.deleteAll()
    }

    override fun sendMessage(message: String, drivenId: String, userId: String): Single<*> =
        socketManager.executeRPCAction(
            SendSocketMessage(
                drivenId,
                userId,
                message,
                "hi",
                TEXT_MSG_TYPE,
                null
            )
        )
            .observeOn(Schedulers.io())
            .map { it.result }
            .map {
                MessageDb(
                    it.id,
                    drivenId,
                    userId,
                    null,
                    false,
                    "hi",
                    message,
                    TEXT_MSG_TYPE,
                    "mail",
                    it.timestamp,
                    null
                )
            }
            .doOnSuccess { dao.insert(it) }
            .flatMap {
                updateUser(userId, drivenId, it.timestamp)
            }

    override fun sendGift(fromId: String, toId: String, giftId: Int, categoryId: Int): Single<*> =
        socketManager.executeRPCAction(
            SendSocketMessage(
                fromId,
                toId,
                giftId.toString(),
                categoryId.toString(),
                GIFT_MSG_TYPE,
                null
            )
        )
            .observeOn(Schedulers.io())
            .map { it.result }
            .map {
                MessageDb(
                    it.id,
                    fromId,
                    toId,
                    null,
                    false,
                    categoryId.toString(),
                    giftId.toString(),
                    GIFT_MSG_TYPE,
                    "mail",
                    it.timestamp,
                    null
                )
            }
            .doOnSuccess {
                dao.insert(it)
            }
            .flatMap {
                updateUser(toId, fromId, it.timestamp)
            }

    override fun sendImage(
        fromId: String,
        toId: String,
        item: ContentView
    ): Single<*> {
        return socketManager.executeRPCAction(
            SendSocketMessage(
                fromId,
                toId,
                "#privateChatImage#" + item.contentId + "#",
                "hi",
                TEXT_MSG_TYPE,
                item
            )
        )
            .observeOn(Schedulers.io())
            .map { it.result }
            .map {
                MessageDb(
                    it.id,
                    fromId,
                    toId,
                    null,
                    false,
                    "hi",
                    "#privateChatImage#" + item.contentId + "#",
                    TEXT_MSG_TYPE,
                    "mail",
                    it.timestamp,
                    arrayListOf(ChatPhoto(item.contentId, item.url, false))
                )
            }

            .doOnSuccess {
                dao.insert(it)
            }.flatMap {
                updateUser(toId, fromId, it.timestamp)
            }
    }

    private fun updateUser(toId: String, fromId: String, timeStamp: Long?): Single<*> =
        userDao.getUsersById(toId, fromId).firstOrError()
            .doOnSuccess { userDb ->
                userDb.lastMessageTime = timeStamp
            }.doOnSuccess {
                userDao.insert(it)
            }

    override fun getChatListener(): Observable<MessageDb> =
        socketManager.chatMessageListener.map { it.toMessageDb() }
            .observeOn(Schedulers.io())
            .doOnNext { dao.insert(it) }

    override fun getChatReadAllListener(): Observable<Any> =
        socketManager.chatReadAllListener
            .observeOn(Schedulers.io())
            .flatMap { marker ->
                dao.getAll(marker.drivenId, marker.userId)
                    .toObservable()
                    .flatMapIterable { it }
                    .filter { !it.isRead && it.senderId == marker.drivenId }
                    .doOnNext { it.isRead = true }
                    .toList()
                    .doOnSuccess { dao.insertAll(it) }
                    .toObservable()
            }

    override fun setReadAll(drivenUserId: String, userId: String): Single<Any> =
        dao.getAll(drivenUserId, userId).toObservable()
            .subscribeOn(Schedulers.io())
            .flatMapIterable { it }
            .filter { !it.isRead && it.senderId == userId }
            .doOnNext { it.isRead = true }
            .toList()
            .doOnSuccess { dao.insertAll(it) }
            .flatMap {
                if (it.isNotEmpty()) socketManager.executeRPCAction(
                    MarkAllRead(
                        drivenUserId,
                        userId
                    )
                )
                    .delaySubscription(1, TimeUnit.SECONDS) else
                    Single.just(it)
            }

    companion object {
        const val GIFT_MSG_TYPE = "virtualGift"
        const val TEXT_MSG_TYPE = "privateChat"
    }
}

interface IChatRepository {
    fun getChatHistoryPaged(
        drivenUserId: String,
        userId: String
    ): DataSource.Factory<Int, MessageDb>

    fun loadNext(drivenUserId: String, userId: String, offset: Int)

    fun clear()

    fun sendMessage(message: String, drivenId: String, userId: String): Single<*>

    fun sendGift(fromId: String, toId: String, giftId: Int, categoryId: Int): Single<*>

    fun sendImage(
        fromId: String,
        toId: String,
        item: ContentView
    ): Single<*>

    fun getChatListener(): Observable<MessageDb>

    fun setReadAll(drivenUserId: String, userId: String): Single<Any>
    fun getChatReadAllListener(): Observable<Any>
}