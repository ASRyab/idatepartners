package com.prod.idatepartners.repo

import com.prod.idatepartners.api.models.profiles.DrivenContent
import com.prod.idatepartners.api.models.profiles.DrivenUser
import com.prod.idatepartners.api.requests.UsersApi
import com.prod.idatepartners.base.BaseRepository
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.users.DrivenUserDao
import com.prod.idatepartners.db.users.DrivenUserDb
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function3

class DrivenUserRepository(
    private val usersApi: UsersApi,
    private val tokenHandler: ITokenHandler,
    private val drivenUserDao: DrivenUserDao
) : BaseRepository(), IDrivenUserRepository {

    override fun getDrivenUsers(): Observable<MutableList<DrivenUserDb>> =
        Observable.merge(getDrivenUsersApi().toObservable(), getDrivenUsersDb())

    private fun getDrivenCounters(): Single<HashMap<String, Int>> =
        tokenHandler.makeRequest(usersApi.getDrivenCounters()).map { it.counters }

    private fun getDrivenContent(): Single<HashMap<String, List<DrivenContent>>> =
        tokenHandler.makeRequest(usersApi.getDrivenContent())
            .map { it.contents }
            .map {
                val resultMap = HashMap<String, List<DrivenContent>>()
                for (entrySet in it) {
                    resultMap[entrySet.key] = ArrayList(entrySet.value.values)
                }
                resultMap
            }

    private fun getDrivenUsersDb(): Observable<MutableList<DrivenUserDb>> =
        drivenUserDao.getAllObservable().flatMap {
            Observable.fromIterable(it).filter { !it.isHidden }.toList().toObservable()
        }

    private fun getDrivenUsersApi(): Completable =
        Single.zip(
            tokenHandler.makeRequest(usersApi.getDrivenUsers()).map { it.profiles },
            getDrivenCounters(),
            getDrivenContent(),
            Function3<
                    HashMap<String, DrivenUser>,
                    HashMap<String, Int>,
                    HashMap<String, List<DrivenContent>>,
                    HashMap<String, DrivenUserDb>> { t1, t2, t3 ->

                adaptToDb(
                    t1,
                    t2,
                    t3
                )
            })
            .flatMap { list ->
                drivenUserDao
                    .getAllObservable()
                    .firstOrError()
                    .toObservable()
                    .flatMapIterable { it }
                    .filter { !list.containsKey(it.id) }
                    .doOnNext {
                        it.isHidden = true
                        list[it.id] = it
                    }
                    .toList()
                    .map {
                        ArrayList<DrivenUserDb>(list.values)
                    }
            }
            .doOnSuccess { drivenUserDao.insertAll(it) }
            .ignoreElement()


    private fun adaptToDb(
        drivenMap: HashMap<String, DrivenUser>,
        countersMap: HashMap<String, Int>,
        contents: HashMap<String, List<DrivenContent>>
    ): HashMap<String, DrivenUserDb> {
        val mutableList = hashMapOf<String, DrivenUserDb>()
        for (mutableEntry in drivenMap) {
            val u = mutableEntry.value
            val counter = countersMap[mutableEntry.key]
            val media = contents[mutableEntry.key]

            mutableList[u.id] =
                DrivenUserDb(
                    u.primaryPhoto,
                    u.id,
                    u.login,
                    u.firstName,
                    u.lastName,
                    u.gender,
                    u.age,
                    u.country,
                    u.city,
                    u.registeredAt,
                    counter ?: 0,
                    media,
                    false
                )
        }

        return mutableList
    }

    override fun clear() {
        drivenUserDao.deleteAll()
    }

    override fun getDrivenUserById(drivenUserId: String): Observable<DrivenUserDb> =
        drivenUserDao.getById(drivenUserId)

    override fun update(drivenUserDb: DrivenUserDb) {
        drivenUserDao.insert(drivenUserDb)
    }
}


interface IDrivenUserRepository{

    fun getDrivenUsers(): Observable<MutableList<DrivenUserDb>>

    fun clear()

    fun getDrivenUserById(drivenUserId: String): Observable<DrivenUserDb>

    fun update(drivenUserDb: DrivenUserDb)
}