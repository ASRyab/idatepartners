package com.prod.idatepartners.repo

import com.prod.idatepartners.api.models.GiftModelApi
import com.prod.idatepartners.api.requests.GiftsApi
import com.prod.idatepartners.base.BaseRepository
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.utils.RxUtils
import io.reactivex.Observable
import io.reactivex.Single

class GiftRepository(
    tokenHandler: ITokenHandler,
    giftsApi: GiftsApi
)  : BaseRepository() {
    private val giftHotObservable: Observable<HashMap<String, List<GiftModelApi>>> = tokenHandler.makeRequest(giftsApi.getGiftDictionary())
        .map { it.gifts }.toObservable().replay(1).autoConnect()

    init {
        giftHotObservable.subscribe(RxUtils.emptyDataConsumer, RxUtils.getEmptyErrorConsumer(TAG, "init"))
    }

    fun getAllGifts(): Single<HashMap<String, List<GiftModelApi>>> {
        return giftHotObservable.firstOrError()
    }


}