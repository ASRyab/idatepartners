package com.prod.idatepartners.repo

import android.annotation.SuppressLint
import android.text.TextUtils
import androidx.paging.DataSource
import com.prod.idatepartners.api.requests.LetterApi
import com.prod.idatepartners.api.rpc.ISocketManager
import com.prod.idatepartners.base.BaseRepository
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.LetterAndUserDb
import com.prod.idatepartners.db.LetterDao
import com.prod.idatepartners.db.LetterDb
import com.prod.idatepartners.utils.RxUtils
import com.prod.idatepartners.utils.toDb
import com.prod.idatepartners.utils.toLetterDb
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LettersRepository(
    private val letterApi: LetterApi,
    private val tokenHandler: ITokenHandler,
    private val dao: LetterDao,
    private val socketManager: ISocketManager
) : BaseRepository(), ILettersRepository {
    private fun getLetterApi(
        modelId: String,
        userId: String,
        offset: Int
    ): Single<MutableList<LetterDb>> =
        tokenHandler.makeRequest(
            letterApi.getLetterHistory(
                mapOf(
                    Pair("id", modelId),
                    Pair("toId", userId),
                    Pair("offset", offset.toString())
                )
            )
        ).map { it.letters.list }
            .flatMap {
                Observable.fromIterable(it)
                    .map { letter -> letter.toDb() }
                    .toList()
            }
            .doOnSuccess { dao.insertAll(it) }


    override fun getLetterApiById(
        drivenId: String,
        userId: String,
        letterId: String
    ): Observable<LetterAndUserDb> =
        tokenHandler.makeRequest(
            letterApi.getLetterById(
                mapOf(
                    Pair("id", drivenId),
                    Pair("toId", userId),
                    Pair("messageId", letterId)
                )
            )
        ).map { letter -> letter.toDb() }
            .doOnSuccess { it.isRead = true }
            .doOnSuccess { dao.insert(it) }
            .flatMapObservable {
                dao.getLetterByIdWithUser(drivenId, userId, letterId).distinct()
            }


    @SuppressLint("CheckResult")
    override fun loadNext(drivenUserId: String, userId: String, offset: Int) {
        if (!TextUtils.isEmpty(drivenUserId) && !TextUtils.isEmpty(userId)) {
            getLetterApi(drivenUserId, userId, offset)
                .subscribe(
                    RxUtils.emptyDataConsumer,
                    RxUtils.getEmptyErrorConsumer(
                        TAG,
                        "loadNext"
                    )
                )
        }
    }

    override fun getLetterHistoryPaged(
        drivenUserId: String,
        userId: String
    ): DataSource.Factory<Int, LetterAndUserDb> {
        loadNext(drivenUserId, userId, 0)
        return dao.getAllPager(drivenUserId, userId)
    }

    override fun sendLetter(
        drivenId: String,
        userId: String,
        replyToId: String,
        subject: String,
        text: String,
        images: List<String>?
    ): Single<Any> {

        val map: HashMap<String, String?> = hashMapOf(
            "id" to drivenId,
            "recipientId" to userId,
            "replyToId" to replyToId,
            "subject" to subject,
            "text" to text
        )

        if (images != null && images.isNotEmpty()) {
            map["titleImageKey"] = images[0]
            images.forEachIndexed { index, element ->
                map["images[$index]"] = element
            }
        }

        return tokenHandler.makeRequest(
            letterApi.sendLetter(map)
        )
    }

    override fun clear() {
        dao.deleteAll()
    }

    override fun getLettersListener(): Observable<LetterDb> = socketManager
        .lettersListener
        .map { it.toLetterDb() }
        .observeOn(Schedulers.io())
        .doOnNext {
            dao.insert(it)
        }

}

interface ILettersRepository{
    fun getLetterApiById(
        drivenId: String,
        userId: String,
        letterId: String
    ): Observable<LetterAndUserDb>


    fun loadNext(drivenUserId: String, userId: String, offset: Int)

    fun getLetterHistoryPaged(
        drivenUserId: String,
        userId: String
    ): DataSource.Factory<Int, LetterAndUserDb>

    fun sendLetter(
        drivenId: String,
        userId: String,
        replyToId: String,
        subject: String,
        text: String,
        images: List<String>?
    ): Single<Any>

    fun clear()

    fun getLettersListener(): Observable<LetterDb>

}