package com.prod.idatepartners.repo

import com.prod.idatepartners.api.LoginRequest
import com.prod.idatepartners.api.models.AuthData
import com.prod.idatepartners.base.BaseRepository
import io.reactivex.Single

class LoginRepository(private var loginRequest: LoginRequest)  : BaseRepository() {

    fun loginRequest(name: String, pass: String): Single<AuthData> =
        loginRequest.login(name, pass)
}