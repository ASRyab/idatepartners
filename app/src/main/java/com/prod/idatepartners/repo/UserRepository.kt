package com.prod.idatepartners.repo

import com.prod.idatepartners.api.models.profiles.ProfileContacts
import com.prod.idatepartners.api.requests.UsersApi
import com.prod.idatepartners.base.BaseRepository
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.users.UserDao
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.utils.toDb
import io.reactivex.Completable
import io.reactivex.Observable

class UserRepository(
    private val usersApi: UsersApi,
    private val tokenHandler: ITokenHandler,
    private val userDao: UserDao
) : BaseRepository(), IUserRepository {

    override fun getUsersByDriven(id: String): Observable<MutableList<UserDb>> =
        Observable.merge(getUsersByDrivenApi(id).toObservable(), getUsersByDrivenDb(id))

    override fun getUserById(userId: String, drivenId: String): Observable<UserDb> =
        userDao.getUsersById(userId, drivenId)

    private fun getUsersByDrivenApi(id: String): Completable =
        tokenHandler.makeRequest(usersApi.getUsersByDriven(id))
            .map { it.profileContacts }
            .map { profileContacts: ProfileContacts ->
                val arrayList = mutableListOf<UserDb>()
                val counters = profileContacts.counters
                val users = profileContacts.users
                for (user in users.values) {
                    val userId = user.id
                    val letterCount = counters.letter?.get(userId)
                    val mailCount = counters.mail?.get(userId)
                    val userDbFromApi = user.toDb(id, letterCount, mailCount)
                    arrayList.add(userDbFromApi)
                }
                arrayList
            }
            .doOnSuccess { userDao.insertAll(it) }
            .ignoreElement()

    private fun getUsersByDrivenDb(id: String): Observable<MutableList<UserDb>> =
        userDao.getUsersByDriven(id)

    override fun clear() {
        userDao.deleteAll()
    }

    override fun saveUser(it: UserDb) {
        userDao.insert(it)
    }
}

interface IUserRepository {
    fun getUsersByDriven(id: String): Observable<MutableList<UserDb>>

    fun getUserById(userId: String, drivenId: String): Observable<UserDb>

    fun clear()

    fun saveUser(it: UserDb)
}