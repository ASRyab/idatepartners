package com.prod.idatepartners.screens.choosePhoto

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.R
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.screens.userDetail.UrlPhotoViewHolder


class AddPhotoAdapter(
    val list: MutableLiveData<List<Photo>>,
    private val addPhotoClickListener: View.OnClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val IMAGE = 0
    private val ADD_NEW = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            IMAGE -> {
                val binding: ViewDataBinding = DataBindingUtil.inflate(
                    inflater,
                    R.layout.item_photo,
                    parent,
                    false
                )
                return UrlPhotoViewHolder(binding)
            }
            ADD_NEW -> {
                return AddNewHolder(
                    inflater.inflate(R.layout.item_add_new, parent, false)
                )
            }
            else -> Any() as RecyclerView.ViewHolder

        }

    }


    override fun getItemCount(): Int {
        val size = list.value?.size ?: 0
        return if (size in 1..9) size + 1 else size
    }

    override fun getItemViewType(position: Int): Int {
        val size = list.value?.size ?: 0
        return if (size < 10) {
            return if (position == size && size > 0) {
                ADD_NEW
            } else IMAGE
        } else IMAGE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is UrlPhotoViewHolder) {
            list.value?.let { holder.bind(it[position]) }
        }
        holder.itemView.setOnClickListener(addPhotoClickListener)
    }

    class AddNewHolder(view: View) :
        RecyclerView.ViewHolder(view)
}

