package com.prod.idatepartners.screens.choosePhoto

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.BR
import com.prod.idatepartners.R

class ChoosePhotoAdapter(
    var list: MutableLiveData<List<ContentView>>,
    var choosePhotoVM: ChoosePhotoVM
) :
    RecyclerView.Adapter<PhotoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {

        val inflater = LayoutInflater.from(parent.context)

        val binding: ViewDataBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.item_photo_chooser,
            parent,
            false
        )
        return PhotoViewHolder(binding)
    }

    override fun getItemCount(): Int = list.value?.size ?: 0

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        list.value?.let {
            holder.bind(it[position], choosePhotoVM, position)
        }
    }
}

class PhotoViewHolder(var binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(photo: ContentView, choosePhotoVM: ChoosePhotoVM, position: Int) {
        binding.setVariable(BR.photoSelect, photo)
        binding.setVariable(BR.vm, choosePhotoVM)
        binding.setVariable(BR.position, position)
        binding.executePendingBindings()
    }
}