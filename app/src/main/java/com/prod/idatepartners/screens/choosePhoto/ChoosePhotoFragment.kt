package com.prod.idatepartners.screens.choosePhoto

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseFragment
import com.prod.idatepartners.utils.toast
import kotlinx.android.synthetic.main.fragment_choose_photo.view.*

class ChoosePhotoFragment : BaseFragment<ChoosePhotoVM>() {

    var choosePhotoVM: ChoosePhotoVM? = null
    override fun layoutId(): Int = R.layout.fragment_choose_photo
    override fun viewModelClass(): Class<ChoosePhotoVM> = ChoosePhotoVM::class.java

    override fun isActivityOwner(): Boolean = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        choosePhotoVM = viewModel as ChoosePhotoVM

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        choosePhotoVM?.choosePhotoEvent?.observe(viewLifecycleOwner, Observer {
            it?.let { event ->
                when (event) {
                    ChoosePhotoVM.ChoosePhotoStatus.Close -> activity?.onBackPressed()
                    ChoosePhotoVM.ChoosePhotoStatus.MoreThanTen -> toast(R.string.you_can_not_select_more_than_10)
                }
                choosePhotoVM?.choosePhotoEvent?.postValue(null)
            }
        })
        val recycler = binding.root.rvUserPhotos
        recycler.layoutManager = GridLayoutManager(view.context, 3)

    }

}