package com.prod.idatepartners.screens.choosePhoto

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.usecases.IUsersUseCase
import com.prod.idatepartners.utils.RxUtils
import io.reactivex.functions.Consumer
import javax.inject.Inject

class ChoosePhotoVM @Inject constructor(
    private val usersUseCase: IUsersUseCase
) : BaseViewModel() {

    val MAX_LETTER_SIZE = 10
    val listLiveData = MutableLiveData<List<ContentView>>()
    var adapter = ChoosePhotoAdapter(listLiveData, this)
    var layoutDirection = LinearLayoutManager.HORIZONTAL
    val selected = MutableLiveData<List<ContentView>>()
    val currentUser = MutableLiveData<DrivenUserDb?>()
    var isChat: MutableLiveData<Boolean> = MutableLiveData()
    var choosePhotoEvent: MutableLiveData<ChoosePhotoStatus> = MutableLiveData()
    var oldUserId: String? = ""

    override fun passBundle(arguments: Bundle?) {
        super.passBundle(arguments)
        val userId = arguments?.getString("driven_user_id")
        val isChat = arguments?.getBoolean("isChat") ?: true
        this.isChat.postValue(isChat)
        val list = listLiveData.value ?: ArrayList()
        if (userId != null && (list.isEmpty() || oldUserId != userId)) {
            safeDisposable(
                usersUseCase.getDrivenById(userId)
                    .subscribe(
                        Consumer { result ->
                            updatePhotosList(result)
                        },
                        RxUtils.getEmptyErrorConsumer(TAG, "TAG")
                    )
            )
        } else {
            for (item in list) {
                item.isSelected = false
                selected.value?.let {
                    for (selectedItem in it) {
                        if (selectedItem.contentId == item.contentId) {
                            item.isSelected = true
                            break
                        }
                    }
                }
            }
        }
        adapter.notifyDataSetChanged()

    }

    private fun updatePhotosList(result: DrivenUserDb) {
        currentUser.postValue(result)
        oldUserId = result.id
        selected.value = arrayListOf()
        listLiveData.value =
            result.content?.map {
                ContentView(
                    it.contentId,
                    it.contentType,
                    it.profileId,
                    it.approveStatus,
                    it.createdAt,
                    it.url,
                    it.previewUrl,
                    false
                )
            }
    }

    var oldPosition: Int? = null

    fun selectItem(position: Int) {
        val list = listLiveData.value

        if (isChat.value == true && oldPosition != position) {
            list?.forEach { it.isSelected = false }
            oldPosition?.let { adapter.notifyItemChanged(it) }
        } else {
            if (list?.filter { it.isSelected }?.size ?: 0 == MAX_LETTER_SIZE && list?.get(position)?.isSelected == false) {
                choosePhotoEvent.postValue(ChoosePhotoStatus.MoreThanTen)
                return
            }
        }
        list?.get(position)?.isSelected = !(list?.get(position)?.isSelected ?: false)
        listLiveData.postValue(list)

        adapter.notifyItemChanged(position)
        oldPosition = position
    }

    fun onCloseClick() {
        isChat.value?.let {
            if (it) {
                clearData()
            }
        }
        choosePhotoEvent.postValue(ChoosePhotoStatus.Close)
    }

    fun clearData() {
        selected.postValue(ArrayList())
        listLiveData.postValue(ArrayList())
        adapter.notifyDataSetChanged()
    }

    fun onOkClick() {
        selected.postValue(listLiveData.value?.filter { it.isSelected })
        choosePhotoEvent.postValue(ChoosePhotoStatus.Close)
    }


    enum class ChoosePhotoStatus {
        MoreThanTen,
        Close
    }

}