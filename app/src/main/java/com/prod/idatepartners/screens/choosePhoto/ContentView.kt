package com.prod.idatepartners.screens.choosePhoto

data class ContentView(
    val contentId: String,
    val contentType: String,
    val profileId: String,
    val approveStatus: String,
    val createdAt: String,
    val url: String,
    val previewUrl: String,
    var isSelected: Boolean
)