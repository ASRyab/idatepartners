package com.prod.idatepartners.screens.letterDetail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.prod.idatepartners.BR
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseFragment
import com.prod.idatepartners.db.LetterAndUserDb

class LetterDetailFragment : BaseFragment<LetterDetailVM>() {
    override fun layoutId(): Int = R.layout.fragment_letter_detail
    override fun viewModelClass(): Class<LetterDetailVM> = LetterDetailVM::class.java
    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val letterDetailVM = viewModel as LetterDetailVM
        letterDetailVM.replyLetterEvent.observe(this, Observer {
            openReplyLetter(it)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.clickListener, View.OnClickListener { activity?.onBackPressed() })
        navController = Navigation.findNavController(view)

    }

    private fun openReplyLetter(it: LetterAndUserDb?) {
        if (it != null) {
            val args = Bundle()
            args.putString("user_id", it.user?.userId)
            args.putString("driven_user_id", it.drivenUser?.id)
            args.putString("letter_id", it.letter?.id)
            args.putString("subject", it.letter?.subject)

            navController?.navigate(
                R.id.newLetterFragment,
                args,
                NavOptions.Builder()
                    .setPopUpTo(
                        R.id.letterDetailFragment,
                        false
                    ).build()
            )
        }
    }
}