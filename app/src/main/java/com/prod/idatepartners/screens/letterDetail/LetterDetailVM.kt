package com.prod.idatepartners.screens.letterDetail

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.db.LetterAndUserDb
import com.prod.idatepartners.screens.userDetail.PhotoAdapter
import com.prod.idatepartners.usecases.ILetterUseCase
import com.prod.idatepartners.utils.RxUtils
import com.prod.idatepartners.utils.toPhoto
import io.reactivex.functions.Consumer
import javax.inject.Inject

class LetterDetailVM @Inject constructor(var letterUseCase: ILetterUseCase) :
    BaseViewModel() {

    var letterAndUserDb: MutableLiveData<LetterAndUserDb> = MutableLiveData()
    var replyLetterEvent: MutableLiveData<LetterAndUserDb> = MutableLiveData()

    var listUrls: MutableLiveData<List<Photo>> = MutableLiveData()
    var adapter = PhotoAdapter(listUrls)
    var layoutDirection = LinearLayoutManager.HORIZONTAL

    override fun passBundle(arguments: Bundle?) {
        val userId: String = arguments?.getString("user_id") ?: ""
        val drivenUserId: String = arguments?.getString("driven_user_id") ?: ""
        val letterId: String = arguments?.getString("letter_id") ?: ""
        if (userId.isNotEmpty() && drivenUserId.isNotEmpty() && letterId.isNotEmpty()) {
            updateLetterInfo(drivenUserId, userId, letterId)
        }
    }


    private fun updateLetterInfo(drivenId: String, userId: String, letterId: String) {
        safeDisposable(
            letterUseCase.getLetterById(drivenId, userId, letterId)
                .compose(RxUtils.withLoadingObservable(Consumer { isProgress.postValue(it) }))
                .subscribe(Consumer {
                    letterAndUserDb.postValue(it)
                    it.letter?.let { letter ->
                        this.listUrls.postValue(letter.images?.map {item-> item.toPhoto() })
                    }
                }, RxUtils.getEmptyErrorConsumer(TAG, "updateLetterInfo"))
        )
    }

    fun onReplyClick() {
        replyLetterEvent.postValue(letterAndUserDb.value)
    }

}