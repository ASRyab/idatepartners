package com.prod.idatepartners.screens.login

import android.os.Bundle
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.Toast
import androidx.databinding.BindingAdapter
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseFragment


class LoginFragment : BaseFragment<LoginVM>() {

    override fun layoutId(): Int = R.layout.fragment_login

    override fun viewModelClass(): Class<LoginVM> = LoginVM::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = Navigation.findNavController(view)
        val vm = viewModel as LoginVM

        vm.loginEvent.observe(this, Observer {
            when (it.loginStatus) {
                LoginVM.LoginStatus.LoginOk -> {
                    navController.navigate(R.id.mainActivity)
                    activity?.finish()
                }

                LoginVM.LoginStatus.LoginInvalidEmail -> {
                    Toast.makeText(this.context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }

                LoginVM.LoginStatus.LoginInvalidPass -> {
                    Toast.makeText(this.context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
                else -> {
                }
            }

        })
    }

    companion object {
        @BindingAdapter("animateVerticalTop")
        @JvmStatic
        fun animateVerticalTop(view: View, isStart: Boolean) {
            var from = -1300f
            animateVertical(isStart, from, view)
        }

        @BindingAdapter("animateVerticalBottom")
        @JvmStatic
        fun animateVerticalBottom(view: View, isStart: Boolean) {

            var from = 1300f
            animateVertical(isStart, from, view)
        }

        private fun animateVertical(isStart: Boolean, from: Float, view: View) {
            if (!isStart) return
            val anim = TranslateAnimation(
                0f,
                0f,
                from,
                0f
            )
            anim.interpolator = FastOutSlowInInterpolator()
            anim.duration = 1000
            view.visibility = View.VISIBLE
            view.startAnimation(anim)
        }
    }

}
