package com.prod.idatepartners.screens.login

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.usecases.IAutoLoginUseCase
import com.prod.idatepartners.usecases.ILoginUseCase
import com.prod.idatepartners.utils.RxUtils
import com.prod.idatepartners.utils.UserFieldsValidator
import io.reactivex.functions.Consumer
import javax.inject.Inject

class LoginVM @Inject constructor(
    private val useCase: ILoginUseCase, private val userFieldsValidator: UserFieldsValidator,
    autoLoginUseCase: IAutoLoginUseCase
) : BaseViewModel() {

    var data = ObservableField<LoginData>()
    var loginEvent: MutableLiveData<LoginEvent> = MutableLiveData()
    val startAnimation: ObservableBoolean = ObservableBoolean(false)

    init {
        //TODO change fields to "" for release
//        data.set(LoginData("johnmitchell333555@gmail.com", "Megapups123321"))
//        data.set(LoginData("artmblck+4@gmail.com", "111222QQQwww"))
//        data.set(LoginData("johnmitchell333555+2@yahoo.com", "111222333Qa"))
//        data.set(LoginData("artem.mashinskiy@together.com", "112233Qwer"))
        data.set(LoginData("", ""))
        safeDisposable(
            autoLoginUseCase.tryAutologin()
                .compose(RxUtils.withLoading(Consumer { isProgress.value = it }))
                .subscribe(
                    { loginOk() }, {
                        checkError(it)
                        startAnimation.set(true)
                    }
                )
        )
    }

    fun onLoginClicked() {
        data.get()?.let { data ->
            if (validateFields(data.name, data.pass)) {
                makeLogin(data.name, data.pass)
            }
        }
    }


    private fun makeLogin(name: String, pass: String) {
        safeDisposable(
            useCase.loginRequest(name, pass)
                .compose(RxUtils.withLoading(Consumer { isProgress.value = it }))
                .subscribe(
                    { loginOk() },
                    { checkError(it) }
                ))
    }

    private fun loginOk() = postEvent(LoginEvent(LoginStatus.LoginOk, null))

    private fun postEvent(event: LoginEvent) {
        loginEvent.value = event
    }

    private fun validateFields(name: String, password: String): Boolean {

        val isValidPass = userFieldsValidator.validatePassword(
            password,
            Consumer { postEvent(LoginEvent(LoginStatus.LoginInvalidPass, it)) })

        val isValidMail = userFieldsValidator.validateEmail(
            name,
            Consumer { postEvent(LoginEvent(LoginStatus.LoginInvalidEmail, it)) })


        return isValidMail && isValidPass
    }

    data class LoginEvent(var loginStatus: LoginStatus, var errorMessage: String?)

    enum class LoginStatus {
        LoginOk,
        LoginInvalidPass,
        LoginInvalidEmail
    }

}