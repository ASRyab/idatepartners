package com.prod.idatepartners.screens.main

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.BR

open class DrivenUserViewHolder (open var binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {

    open fun bind(user: UserView, position: Int, vm: MainVM) {
        binding.setVariable(BR.user, user)
        binding.setVariable(BR.position, position)
        binding.setVariable(BR.vm, vm)
        binding.executePendingBindings()
    }
}
