package com.prod.idatepartners.screens.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.BR
import com.prod.idatepartners.R
import com.prod.idatepartners.api.models.GiftModelApi

class GiftAdapter(
    private val mainVM: MainVM,
    private val listGift: MutableLiveData<ArrayList<GiftModelApi>>
) : RecyclerView.Adapter<GiftVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiftVH {
        val inflater = LayoutInflater.from(parent.context)

        val binding: ViewDataBinding = DataBindingUtil.inflate(
            inflater, R.layout.item_gift,
            parent,
            false
        )
        return GiftVH(binding, mainVM)
    }

    override fun getItemCount(): Int = listGift.value?.size ?: 0

    override fun onBindViewHolder(holder: GiftVH, position: Int) {
        listGift.value?.let { holder.bind(it[position]) }
    }
}

class GiftVH(val binding: ViewDataBinding, private val mainVM: MainVM) : RecyclerView.ViewHolder(binding.root) {

    fun bind(giftModelApi: GiftModelApi) {
        binding.setVariable(BR.vm, giftModelApi)
        binding.setVariable(BR.mainVM, mainVM)
        binding.executePendingBindings()

    }

}