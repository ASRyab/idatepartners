package com.prod.idatepartners.screens.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.BR
import com.prod.idatepartners.R
import com.prod.idatepartners.db.LetterAndUserDb

class LettersAdapter(var letterVm: LettersVM) :
    PagedListAdapter<LetterAndUserDb, LetterViewHolder>(PersonDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LetterViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val binding: ViewDataBinding = DataBindingUtil.inflate(
            inflater, R.layout.item_letter,
            parent,
            false
        )
        return LetterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LetterViewHolder, position: Int) {
        holder.bind(getItem(position), letterVm)
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

}

class PersonDiffCallback : DiffUtil.ItemCallback<LetterAndUserDb>() {
    override fun areContentsTheSame(oldItem: LetterAndUserDb, newItem: LetterAndUserDb): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: LetterAndUserDb, newItem: LetterAndUserDb): Boolean {
        return oldItem.letter?.id == newItem.letter?.id
    }

}

class LetterViewHolder(private val binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(message: LetterAndUserDb?, letterVm: LettersVM) {
        binding.setVariable(BR.vm, message)
        binding.setVariable(BR.letterVm, letterVm)
        binding.executePendingBindings()
    }

}