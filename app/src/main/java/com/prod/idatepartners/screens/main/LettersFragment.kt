package com.prod.idatepartners.screens.main

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.LinearLayout.VERTICAL
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseFragment
import com.prod.idatepartners.db.LetterAndUserDb
import kotlinx.android.synthetic.main.fragment_letters.*


class LettersFragment : BaseFragment<LettersVM>() {

    private val KEY_RECYCLER_STATE = "recycler_state"
    private var bundleRVState: Bundle? = null

    private lateinit var navController: NavController
    lateinit var lettersVM: LettersVM

    override fun layoutId(): Int = R.layout.fragment_letters

    override fun viewModelClass(): Class<LettersVM> = LettersVM::class.java

    private val observer = object :
        RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            if (positionStart == 0) {
                rvLetters.scrollToPosition(0)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lettersVM = viewModel as LettersVM

        lettersVM.selectLetterEvent.observe(
            this,
            Observer {
                openScreenWithArgs(it, R.id.letterDetailFragment)
            })
        lettersVM.replyLetterEvent.observe(this, Observer {
            openScreenWithArgs(it, R.id.newLetterFragment)
        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemDecor = DividerItemDecoration(context, VERTICAL)
        rvLetters.addItemDecoration(itemDecor)

        val navHostFragment = parentFragment
        val parent = navHostFragment?.parentFragment

        navController = Navigation.findNavController(
            parent!!.view!!
        )

        lettersVM.adapter.registerAdapterDataObserver(observer)

        val vm = ViewModelProviders.of(activity!!, viewModelFactory)
            .get(MainVM::class.java)

        vm.selectedDrivenIdData.observe(viewLifecycleOwner, Observer { lettersVM.drivenUserId = it })
        vm.selectedUserIdData.observe(viewLifecycleOwner, Observer {
            lettersVM.lettersLiveData.removeObservers(viewLifecycleOwner)
            lettersVM.updateUserId(it)
            lettersVM.lettersLiveData.observe(viewLifecycleOwner, Observer { newList ->
                lettersVM.adapter.submitList(newList)
                tvEmptyLetters.visibility = if (newList.isEmpty()) View.VISIBLE else View.GONE
                buttonContainer.visibility = if (newList.isEmpty()) View.GONE else View.VISIBLE
            })
        })
    }

    private fun openScreenWithArgs(letterAndUserDb: LetterAndUserDb?, resId: Int) {
        val mainVm = ViewModelProviders.of(activity!!, viewModelFactory)
            .get(MainVM::class.java)

        if (letterAndUserDb != null) {
            val args = Bundle()
            args.putString("user_id", letterAndUserDb.user?.userId)
            args.putString("letter_id", letterAndUserDb.letter?.id)
            args.putString("driven_user_id", letterAndUserDb.drivenUser?.id)
            args.putString("subject", letterAndUserDb.letter?.subject)

            navController.navigate(
                resId,
                args,
                NavOptions.Builder()
                    .setPopUpTo(
                        R.id.mainFragment,
                        false
                    ).build()
            )
            mainVm.userListScrolled(true)
            mainVm.reObserveUser = true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lettersVM.adapter.unregisterAdapterDataObserver(observer)

    }

    override fun onPause() {
        super.onPause()
        bundleRVState = Bundle()
        val listState = rvLetters.layoutManager?.onSaveInstanceState()
        bundleRVState?.putParcelable(KEY_RECYCLER_STATE, listState)
    }

    override fun onResume() {
        super.onResume()
        val listState = bundleRVState?.getParcelable<Parcelable>(KEY_RECYCLER_STATE)
        rvLetters.layoutManager?.onRestoreInstanceState(listState)

    }

}
