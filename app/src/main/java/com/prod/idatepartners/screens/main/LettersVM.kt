package com.prod.idatepartners.screens.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.db.LetterAndUserDb
import com.prod.idatepartners.usecases.ILetterUseCase
import com.prod.idatepartners.utils.RxUtils
import javax.inject.Inject

class LettersVM @Inject constructor(var lettersUseCase: ILetterUseCase) : BaseViewModel() {

    var lettersLiveData: LiveData<PagedList<LetterAndUserDb>> = MutableLiveData()

    var adapter: LettersAdapter = LettersAdapter(this)
    var layoutDirection: Int = LinearLayoutManager.VERTICAL

    var drivenUserId: String = ""
    var userId: String = ""
    var offset: Int = 0

    var selectLetterEvent = MutableLiveData<LetterAndUserDb>()
    var replyLetterEvent = MutableLiveData<LetterAndUserDb>()

    fun updateUserId(it: String) {
        this.userId = it
        offset = 0
        initLetterData()
    }

    init {
        safeDisposable(
            lettersUseCase.getLettersListener().subscribe(
                RxUtils.emptyDataConsumer
                , RxUtils.getEmptyErrorConsumer(TAG, "init")
            )
        )
    }

    private fun initLetterData() {
        val factory =
            lettersUseCase.getLetterHistoryPaged(drivenUserId, this.userId)


        val builder = LivePagedListBuilder<Int, LetterAndUserDb>(
            factory,
            PagedList.Config.Builder().setPageSize(LettersVM.PAGE_SIZE).setPrefetchDistance(
                LettersVM.PREFETCH_DISTANCE
            ).build()
        )

        val boundaryCallback = object : PagedList.BoundaryCallback<LetterAndUserDb>() {
            override fun onItemAtEndLoaded(itemAtEnd: LetterAndUserDb) {
                super.onItemAtEndLoaded(itemAtEnd)
                offset += OFFSET
                if (lettersLiveData.value?.size ?: 0 == offset) {
                    lettersUseCase.loadMore(drivenUserId, userId, offset)
                }
            }
        }

        builder.setBoundaryCallback(boundaryCallback)

        lettersLiveData = builder.build()
    }

    fun onLetterClick(letterAndUserDb: LetterAndUserDb) {
        selectLetterEvent.postValue(letterAndUserDb)
    }

    fun onReplyClick() {
        replyLetterEvent.postValue(lettersLiveData.value?.firstOrNull())
    }

    companion object {
        const val PAGE_SIZE = 50
        const val PREFETCH_DISTANCE = 5
        const val OFFSET = PAGE_SIZE
    }
}