package com.prod.idatepartners.screens.main

import android.os.Bundle
import android.widget.TextView
import com.prod.idatepartners.App
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseActivity
import com.prod.idatepartners.utils.UiHandler
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var tv: TextView
    @Inject
    lateinit var uiHandler: UiHandler

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component.inject(this)
        tv.text=""
    }

    override fun onResume() {
        super.onResume()
        uiHandler.haveUiSubject.onNext(true)
    }

    override fun onPause() {
        super.onPause()
        uiHandler.haveUiSubject.onNext(false)
    }
}
