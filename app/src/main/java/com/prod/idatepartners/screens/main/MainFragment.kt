package com.prod.idatepartners.screens.main


import android.animation.ValueAnimator
import android.graphics.Rect
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseFragment
import com.prod.idatepartners.screens.choosePhoto.ChoosePhotoVM
import com.prod.idatepartners.utils.hideKeyboard
import kotlinx.android.synthetic.main.fragment_main_screen.*

class MainFragment : BaseFragment<MainVM>() {

    private lateinit var navController: NavController
    private lateinit var choosePhotoVM: ChoosePhotoVM
    private var currentIsChat: Boolean = true
    private var keyboardListenersAttached = false
    private var lastActionType = ACTION_HIDE_KEYBOARD
    private lateinit var vm: MainVM

    override fun layoutId(): Int = R.layout.fragment_main_screen

    override fun isActivityOwner(): Boolean = true

    override fun viewModelClass(): Class<MainVM> = MainVM::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = viewModel as MainVM
        choosePhotoVM = activity?.run {
            ViewModelProviders.of(requireActivity(), viewModelFactory)
                .get(ChoosePhotoVM::class.java)
        } ?: throw Exception("Invalid Activity")

        vm.tapUserIdEvent.observe(this, Observer {
            val args = Bundle()
            args.putString("user_id", it)
            args.putString("driven_user_id", vm.selectedDrivenIdData.value)
            navController.navigate(
                R.id.userDetailFragment,
                args,
                NavOptions.Builder()
                    .setPopUpTo(
                        R.id.mainFragment,
                        false
                    ).build()
            )
            vm.userListScrolled(true)
        })

        vm.choosePhotoEvent.observe(this, Observer {
            val args = Bundle()
            args.putString("driven_user_id", it)
            args.putBoolean("isChat", true)
            navController.navigate(
                R.id.choosePhotoFragment,
                args,
                NavOptions.Builder()
                    .setPopUpTo(
                        R.id.mainFragment,
                        false
                    ).build()
            )
        })

    }

    override fun onResume() {
        super.onResume()
        vm.updateDrivenUsers()
        addKeyboardListener()
    }

    private fun addKeyboardListener() {
        if (keyboardListenersAttached) {
            return
        }
        rootContainer.viewTreeObserver.addOnGlobalLayoutListener(keyboardLayoutListener)
        keyboardListenersAttached = true
    }

    override fun onPause() {
        super.onPause()
        removeKeyboardListener()
    }

    private fun removeKeyboardListener() {
        if (keyboardListenersAttached) {
            rootContainer.viewTreeObserver.removeGlobalOnLayoutListener(keyboardLayoutListener)
            keyboardListenersAttached = false
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        initRecyclers()
        initObservers(navController)
        initToolbar(navController)
        initGiftView(view)
    }

    private fun initObservers(navController: NavController) {

        choosePhotoVM.selected.observe(viewLifecycleOwner, Observer {
            if (it != null && it.isNotEmpty()) {
                vm.onImageSend(it[0])
                choosePhotoVM.clearData()
            }
        })

        vm.isChat.observe(viewLifecycleOwner, Observer {
            hideKeyboard()
            val withAnimation = currentIsChat != it
            expandAndCollapse(it, withAnimation)
            currentIsChat = it
            vm.isGiftsShow.value = false
        })

    }

    private fun initRecyclers() {
        vm.selectedUserPosition.observe(viewLifecycleOwner, Observer {
            it?.let { pos -> recyclerViewMen.scrollToPosition(pos) }
        })
        vm.listDrivenUser.observe(
            viewLifecycleOwner,
            Observer { vm.adapterDrivenUser.notifyDataSetChanged() })

        vm.listUser.observe(viewLifecycleOwner, Observer {
            vm.adapterUser.notifyDataSetChanged()
        })

        recyclerViewMen.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dx != 0)
                    vm.userListScrolled(true)
            }
        })
    }

    private fun initGiftView(view: View) {
        vm.isGiftsShow.observe(viewLifecycleOwner, Observer {
            if (it) hideKeyboard()
        })
        giftView.layoutManager =
            GridLayoutManager(view.context, 4)
    }

    private fun initToolbar(navController: NavController) {
        toolbar.overflowIcon = ContextCompat.getDrawable(context!!, R.drawable.ic_logout);
        toolbar.inflateMenu(R.menu.main_menu)
        toolbar.setOnMenuItemClickListener(object : Toolbar.OnMenuItemClickListener,
            android.widget.Toolbar.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.action_logout -> {
                        vm.logout()
                        navController.navigate(
                            R.id.loginActivity,
                            null,
                            NavOptions.Builder()
                                .setPopUpTo(
                                    R.id.loginActivity,
                                    true
                                ).build()
                        )
                        activity?.finish()

                    }
                }
                return true
            }

        })
    }


    private val keyboardLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        val activity = activity ?: return@OnGlobalLayoutListener
        val heightDiff = rootContainer.rootView.height - rootContainer.height
        val rectangle = Rect()
        activity.window.decorView.getWindowVisibleDisplayFrame(rectangle)
        val statusBarAndSoftBtnBarHeight = rectangle.top + getSoftButtonsBarHeight()
        val actionType = if (heightDiff <= statusBarAndSoftBtnBarHeight) {
            ACTION_HIDE_KEYBOARD
        } else {
            giftView.layoutParams.height = heightDiff - statusBarAndSoftBtnBarHeight
            ACTION_SHOW_KEYBOARD
        }
        if (actionType != lastActionType) {
            eventKeyBoard(actionType)
            lastActionType = actionType
        }
    }

    private fun eventKeyBoard(actionType: String) {
        if (actionType == ACTION_SHOW_KEYBOARD) vm.keyboardShowed()
    }

    private fun getSoftButtonsBarHeight(): Int {
        val activity = activity ?: return 0

        val metrics = DisplayMetrics()
        val defaultDisplay = activity.windowManager.defaultDisplay
        defaultDisplay.getMetrics(metrics)
        val usableHeight = metrics.heightPixels
        defaultDisplay.getRealMetrics(metrics)
        val realHeight = metrics.heightPixels
        return if (realHeight > usableHeight) {
            realHeight - usableHeight
        } else {
            0
        }
    }

    companion object {
        const val ACTION_HIDE_KEYBOARD = "KeyboardWillHide"
        const val ACTION_SHOW_KEYBOARD = "KeyboardWillShow"
    }

    private fun expandAndCollapse(isChat: Boolean, withAnimation: Boolean) {
        val v1: View = if (isChat) fragmentChat else fragmentLetter
        val v2: View = if (isChat) fragmentLetter else fragmentChat
        if (withAnimation) {
            val m1 = ValueAnimator.ofFloat(0.0f, 1f) //fromWeight, toWeight
            m1.duration = 400
            m1.startDelay = 0 //Optional Delay
            m1.interpolator = AccelerateDecelerateInterpolator()
            m1.addUpdateListener { animation ->

                (v1.layoutParams as LinearLayout.LayoutParams).weight =
                    animation.animatedValue as Float + if (!isChat) 0.2f else 0f
                (v2.layoutParams as LinearLayout.LayoutParams).weight =
                    1f - animation.animatedValue as Float
                v1.requestLayout()
                v2.requestLayout()
            }
            m1.start()
        } else {
            (v1.layoutParams as LinearLayout.LayoutParams).weight = 1f
            (v2.layoutParams as LinearLayout.LayoutParams).weight = 0f
        }
    }
}