package com.prod.idatepartners.screens.main

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.prod.idatepartners.api.models.GiftModelApi
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.screens.choosePhoto.ContentView
import com.prod.idatepartners.usecases.IGiftUseCase
import com.prod.idatepartners.usecases.ILogoutUseCase
import com.prod.idatepartners.usecases.ISocketChatUseCase
import com.prod.idatepartners.usecases.IUsersUseCase
import com.prod.idatepartners.utils.RxUtils
import com.prod.idatepartners.utils.toUserView
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import javax.inject.Inject


class MainVM @Inject constructor(
    private val usersUseCase: IUsersUseCase,
    private val socketChatUseCase: ISocketChatUseCase,
    private val logoutUseCase: ILogoutUseCase,
    private val giftUseCase: IGiftUseCase
) : BaseViewModel() {

    private var disposable: Disposable? = null
    private var redPointsDisposable: Disposable? = null

    val listDrivenUser: MutableLiveData<ArrayList<UserView>> = MutableLiveData()
    val listUser: MutableLiveData<ArrayList<UserView>> = MutableLiveData()
    private val listGift: MutableLiveData<ArrayList<GiftModelApi>> = MutableLiveData()

    var selectedUserPosition = MutableLiveData<Int>()
    var selectedDrivenPosition: Int = 0

    var choosePhotoEvent: MutableLiveData<String> = MutableLiveData()

    val messageText: MutableLiveData<String> = MutableLiveData()
    val layoutDirection: MutableLiveData<Int> = MutableLiveData()
    val isChat: MutableLiveData<Boolean> = MutableLiveData()
    val isGiftsShow: MutableLiveData<Boolean> = MutableLiveData()

    val hasChatRedPoint = MutableLiveData<Boolean>()
    val hasLetterRedPoint = MutableLiveData<Boolean>()

    var selectedUserIdData: MutableLiveData<String> = MutableLiveData()
    var selectedDrivenIdData: MutableLiveData<String> = MutableLiveData()

    var tapUserIdEvent: MutableLiveData<String> = MutableLiveData()
    var mainCounter: MutableLiveData<Int> = MutableLiveData()
    val isScrolledData: MutableLiveData<Boolean> = MutableLiveData()

    var adapterDrivenUser: UsersAdapter = UsersAdapter(this, listDrivenUser, true)
    var adapterUser: UsersAdapter = UsersAdapter(this, listUser, false)
    val giftAdapter: GiftAdapter = GiftAdapter(this, listGift)
    var reObserveUser = false

    init {
        selectedUserIdData.value = ""
        selectedDrivenIdData.value = ""
        layoutDirection.postValue(LinearLayoutManager.HORIZONTAL)
        messageText.value = ""
        isGiftsShow.value = false
        isChat.value = true
        selectedUserPosition.value = 0
    }

    fun onDrivenSelected(userId: String, position: Int) {
        selectedDrivenIdData.postValue(userId)
        adapterDrivenUser.notifyItemChanged(position)
        adapterDrivenUser.notifyItemChanged(selectedDrivenPosition)

        selectedUserIdData.postValue("")
        selectedDrivenPosition = position
        isChat.postValue(true)
        updateUsers(userId)
    }

    fun onUserSelected(userId: String, position: Int) {
        if (userId == selectedUserIdData.value) {
            tapUserIdEvent.postValue(userId)
        } else {
            selectedUserIdData.value = userId
            checkChatCounter()
            isChat.postValue(true)
        }
        adapterUser.notifyItemChanged(selectedUserPosition.value ?: 0)
        adapterUser.notifyItemChanged(position)
        selectedUserPosition.value = position

        updateRedPoints()
    }

    private fun updateRedPoints() {
        val selectedDriven = selectedDrivenIdData.value ?: ""
        val selectedUser = selectedUserIdData.value ?: ""

        if (selectedDriven.isNotEmpty() && selectedUser.isNotEmpty()) {
            redPointsDisposable?.dispose()
            redPointsDisposable = usersUseCase.getUser(selectedUser, selectedDriven).distinct()
                .subscribe(Consumer {
                    val isChat = isChat.value ?: false
                    hasLetterRedPoint.postValue(isChat && it.letterCount > 0)
                    hasChatRedPoint.postValue(!isChat && it.mailCount > 0)
                }, RxUtils.getEmptyErrorConsumer(TAG, "updateRedPoints"))
        }

    }

    fun onChatClicked() {
        isChat.value = true
        checkChatCounter()
        updateRedPoints()
    }

    private fun checkChatCounter() {
        val drivenUserId = selectedDrivenIdData.value
        val userId = selectedUserIdData.value
        val isChatSelected = isChat.value ?: false
        if (isChatSelected && !drivenUserId.isNullOrEmpty() && !userId.isNullOrEmpty())
            safeDisposable(
                socketChatUseCase.setReadAll(drivenUserId, userId).subscribe(
                    RxUtils.emptyDataConsumer, Consumer { checkError(it) }
                )
            )
    }

    fun onSendClicked() {
        val messageText = messageText.value
        val selectedUserId = selectedUserIdData.value
        val selectedDrivenId = selectedDrivenIdData.value
        if (!messageText.isNullOrEmpty() && !selectedDrivenId.isNullOrEmpty() && !selectedUserId.isNullOrEmpty()) {
            safeDisposable(
                socketChatUseCase.sendMessage(
                    messageText,
                    selectedDrivenId,
                    selectedUserId
                ).subscribe(
                    { this.messageText.value = "" },
                    { checkError(it) }
                )
            )
        }
    }

    fun onGiftSend(giftModelApi: GiftModelApi) {
        val selectedUserId = selectedUserIdData.value
        val selectedDrivenId = selectedDrivenIdData.value
        if (!selectedDrivenId.isNullOrEmpty() && !selectedUserId.isNullOrEmpty()) {
            safeDisposable(socketChatUseCase.sendGift(
                selectedDrivenId, selectedUserId, giftModelApi.id, giftModelApi.categoryId
            ).subscribe(
                { isGiftsShow.value = false },
                { checkError(it) }
            ))
        }
    }

    fun onImageSend(image: ContentView) {
        val selectedUserId = selectedUserIdData.value
        val selectedDrivenId = selectedDrivenIdData.value
        if (!selectedDrivenId.isNullOrEmpty() && !selectedUserId.isNullOrEmpty()) {
            safeDisposable(socketChatUseCase.sendImage(
                image, selectedDrivenId,
                selectedUserId
            ).subscribe(
                { isGiftsShow.value = false },
                { checkError(it) }
            ))
        }
    }

    fun onGiftClicked() {
        isGiftsShow.value = isGiftsShow.value != true
        showGifts()
    }

    private fun showGifts() {
        if (isGiftsShow.value == true)
            safeDisposable(
                giftUseCase.getAllGifts().subscribe(
                    { initGiftList(it) },
                    { checkError(it) }
                )
            )
    }

    private fun initGiftList(list: ArrayList<GiftModelApi>) {
        listGift.value = list
    }

    fun onChoosePhotoClicked() {
        choosePhotoEvent.postValue(selectedDrivenIdData.value)
    }

    fun onLetterClicked() {
        isChat.value = false
        updateRedPoints()
    }

    fun updateDrivenUsers() {
        safeDisposable(
            usersUseCase.getDrivenUsers()
                .flatMap {
                    Observable.fromIterable(it).map { v -> v.toUserView() }
                        .toList().toObservable()
                }.doOnNext {
                    var counter = 0
                    for (user in it) {
                        counter += user.getUnreadCount()
                    }
                    mainCounter.postValue(counter)
                }
                .subscribe(
                    { updateDrivenList(it) }, { checkError(it) }
                )
        )

    }

    private fun updateDrivenList(girlsList: List<UserView>) {
        val oldId = selectedDrivenIdData.value
        if (selectedDrivenIdData.value == "" && girlsList.isNotEmpty()) {
            selectedDrivenIdData.value = girlsList[0].id
            selectedDrivenPosition = 0
        }
        listDrivenUser.postValue(ArrayList(girlsList))
        val id = selectedDrivenIdData.value
        if (!id.isNullOrEmpty() && (oldId != id || reObserveUser)) {
            reObserveUser = false
            updateUsers(id)
        }
    }

    private fun updateUsers(id: String) {
        disposable?.dispose()
        disposable = getUsersDisposable(id)
    }

    private fun getUsersDisposable(id: String): Disposable {
        return usersUseCase.getUsers(id)
            .flatMap {
                Observable.fromIterable(it)
                    .map { v -> v.toUserView() }
                    .toList().toObservable()
            }
            .subscribe(
                { updateUserList(it) },
                { checkError(it) }

            )
    }

    private fun updateUserList(userList: List<UserView>) {
        if (selectedUserIdData.value == "" && userList.isNotEmpty()) {
            selectedUserIdData.value = userList[0].id
            selectedUserPosition.value = 0
        } else if (userList.isNotEmpty()
            && userList[selectedUserPosition.value ?: 0].id != selectedUserIdData.value ?: ""
        ) {
            selectedUserPosition.value =
                userList.indexOfFirst { us -> us.id == selectedUserIdData.value }
        }
        checkChatCounter()
        updateRedPoints()
        listUser.postValue(ArrayList(userList))
    }

    fun userListScrolled(isScrolled: Boolean) {
        this.isScrolledData.value = isScrolled
    }

    fun logout() {
        safeDisposable(
            logoutUseCase.logout().subscribe(
                RxUtils.emptyDataConsumer,
                Consumer { checkError(it) }
            )
        )
    }

    fun keyboardShowed() {
        isGiftsShow.value = false
    }
}