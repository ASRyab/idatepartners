package com.prod.idatepartners.screens.main

data class UserView(
    var id: String,
    var name: String,
    var age: Int?,
    var letterUnreadCount: Int,
    var chatUnreadCount: Int,
    var imageUrl: String?,
    var isGirl: Boolean,
    var isOnline: Boolean?
) {
    fun getUnreadCount(): Int {
        return letterUnreadCount + chatUnreadCount
    }
}