package com.prod.idatepartners.screens.main

import android.content.Context
import android.graphics.Color
import android.graphics.PixelFormat
import android.graphics.Rect
import android.view.Gravity
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.prod.idatepartners.App
import com.prod.idatepartners.R
import com.prod.idatepartners.screens.main.UsersAdapter.Companion.disposable
import com.prod.idatepartners.utils.RxUtils
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UserViewHolder(override var binding: ViewDataBinding) : DrivenUserViewHolder(binding),
    ViewTreeObserver.OnGlobalLayoutListener, Observer<Boolean> {

    private val wm: WindowManager

    var user: UserView? = null

    @Inject
    lateinit var tv: TextView

    init {
        App.component.inject(this)
        val context = binding.root.context
        wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    }


    override fun onGlobalLayout() {
        val locationsFrom = IntArray(2)
        binding.root.getLocationInWindow(locationsFrom)
        showBubble(locationsFrom)
        binding.root.viewTreeObserver?.removeOnGlobalLayoutListener(this)
    }

    private fun showBubble(locationsFrom: IntArray) {
        val params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_SUB_PANEL,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                    or WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                    or WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
            PixelFormat.TRANSLUCENT
        )
        params.gravity = Gravity.TOP or Gravity.START

        val oldText = tv.text
        val text = "${user?.name}, ${user?.age}"
        if (text == oldText) return

        hideBubble()
        tv.text = text
        tv.setBackgroundResource(R.drawable.background_red_rounded)
        tv.setTextColor(Color.WHITE)
        tv.gravity = Gravity.CENTER
        val bounds = Rect()
        tv.paint.getTextBounds(text, 0, text.length, bounds)
        val width = bounds.width()
        val pixelSize = tv.context.resources.getDimensionPixelSize(R.dimen.user_image_size)
        val pixelSizeMargin =
            tv.context.resources.getDimensionPixelSize(R.dimen.user_image_size_margin)
        val pixelSizeMargin2x =
            tv.context.resources.getDimensionPixelSize(R.dimen.margin_bg_red_rounded)
        val dX = (width - pixelSize) / 2
        params.x = locationsFrom[0] - dX - pixelSizeMargin2x
        params.y = locationsFrom[1] + pixelSize - (pixelSizeMargin * 2)

        wm.addView(tv, params)
        UsersAdapter.isAttach = true

        disposable = Observable.just(1).delay(3, TimeUnit.SECONDS)
            .subscribe(Consumer { hideBubble() }
                , RxUtils.getEmptyErrorConsumer(this.javaClass.simpleName, "hideBubble"))
    }

    override fun onChanged(t: Boolean?) {
        hideBubble()
    }

    private fun hideBubble() {
        if (UsersAdapter.isAttach && tv.isShown) {
            disposable?.dispose()
            wm.removeView(tv)
            UsersAdapter.isAttach = false
        }
    }

    override fun bind(user: UserView, position: Int, vm: MainVM) {
        super.bind(user, position, vm)
        this.user = user
        val selectedMenPosition = vm.selectedUserPosition.value
        if (selectedMenPosition == position) {
            val childAt = binding.root
            childAt.viewTreeObserver?.addOnGlobalLayoutListener(this)
        }
        vm.isScrolledData.removeObserver(this)
        vm.isScrolledData.observeForever(this)
    }
}
