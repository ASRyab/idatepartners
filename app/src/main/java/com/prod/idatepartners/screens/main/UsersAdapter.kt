package com.prod.idatepartners.screens.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.R
import io.reactivex.disposables.Disposable

class UsersAdapter(
    private val vm: MainVM,
    private val list: LiveData<ArrayList<UserView>>,
    private val isDrivenUsers: Boolean
) : RecyclerView.Adapter<DrivenUserViewHolder>() {

    companion object {
        var isAttach: Boolean = false
        var disposable: Disposable? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrivenUserViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val binding: ViewDataBinding = DataBindingUtil.inflate(
            inflater, R.layout.item_user_rounded,
            parent,
            false
        )
        return if (isDrivenUsers) DrivenUserViewHolder(binding) else UserViewHolder(binding)
    }

    override fun getItemCount(): Int = list.value?.size ?: 0

    override fun onBindViewHolder(holder: DrivenUserViewHolder, position: Int) {
        list.value?.let { holder.bind(it[position], position, vm) }
    }
}


