package com.prod.idatepartners.screens.main.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.BR
import com.prod.idatepartners.R
import com.prod.idatepartners.db.MessageDb
import com.prod.idatepartners.repo.ChatRepository.Companion.GIFT_MSG_TYPE

class ChatAdapter(private val chatVM: ChatVM) :
    PagedListAdapter<MessageDb, MessageViewHolder>(PersonDiffCallback()) {
    companion object {
        const val TYPE_MESSAGE_OWN = 0
        const val TYPE_MESSAGE_USER = 1
        const val TYPE_GIFT_OWN = 2
        const val TYPE_GIFT_USER = 3
        const val TYPE_IMAGE_OWN = 4
        const val TYPE_IMAGE_USER = 5
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val binding: ViewDataBinding = DataBindingUtil.inflate(
            inflater, getLayout(viewType),
            parent,
            false
        )
        return MessageViewHolder(binding, chatVM)
    }

    private fun getLayout(viewType: Int) =
        when (viewType) {
            TYPE_MESSAGE_OWN -> R.layout.item_message_own
            TYPE_MESSAGE_USER -> R.layout.item_message_user
            TYPE_GIFT_OWN -> R.layout.item_gift_own
            TYPE_GIFT_USER -> R.layout.item_gift_user
            TYPE_IMAGE_OWN -> R.layout.item_image_own
            TYPE_IMAGE_USER -> R.layout.item_image_user
            else -> R.layout.item_message_own
        }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {

        var shouldShowDate = false

        if (position == itemCount - 1) {
            shouldShowDate = true
        } else if (position < itemCount - 1) {
            val thisMsg = getItem(position)
            val previousMsg = getItem((position + 1))

            previousMsg?.let {
                thisMsg?.let {
                    shouldShowDate = thisMsg.getDate() != previousMsg.getDate()
                }
            }
        }
        holder.bind(getItem(position), holder.itemViewType, shouldShowDate)

    }

    override fun getItemViewType(position: Int): Int {
        val messageDb = getItem(position)
        return if (isOwn(messageDb)) {
            if (!messageDb?.chatImages.isNullOrEmpty()) return TYPE_IMAGE_OWN
            else when (messageDb?.msgType) {
                GIFT_MSG_TYPE -> TYPE_GIFT_OWN
                else -> TYPE_MESSAGE_OWN
            }
        } else {
            if (!messageDb?.chatImages.isNullOrEmpty()) return TYPE_IMAGE_USER
            else when (messageDb?.msgType) {
                GIFT_MSG_TYPE -> TYPE_GIFT_USER
                else -> TYPE_MESSAGE_USER
            }
        }
    }

    private fun isOwn(messageDb: MessageDb?): Boolean {
        return messageDb?.senderId == chatVM.drivenUserId
    }

}

class PersonDiffCallback : DiffUtil.ItemCallback<MessageDb>() {
    override fun areContentsTheSame(oldItem: MessageDb, newItem: MessageDb): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: MessageDb, newItem: MessageDb): Boolean {
        return oldItem.id == newItem.id
    }

}

class MessageViewHolder(
    private val binding: ViewDataBinding,
    private val chatVM: ChatVM
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(message: MessageDb?, itemViewType: Int, shouldShowDate: Boolean) {
        if (message == null) return

        val bindingModel: Any? = initBindingModel(message, itemViewType)

        binding.setVariable(BR.vm, bindingModel)
        binding.setVariable(BR.shouldShowDate, shouldShowDate)
        binding.executePendingBindings()
    }

    private fun initBindingModel(message: MessageDb, itemViewType: Int): Any? {
        return when (itemViewType) {
            ChatAdapter.TYPE_MESSAGE_OWN, ChatAdapter.TYPE_MESSAGE_USER,
            ChatAdapter.TYPE_IMAGE_USER, ChatAdapter.TYPE_IMAGE_OWN -> {
                message
            }
            ChatAdapter.TYPE_GIFT_OWN, ChatAdapter.TYPE_GIFT_USER ->
                Pair(
                    chatVM.giftsList.firstOrNull { (message.text == it.id.toString()) }?.imgUrl,
                    message
                )
            else -> null
        }
    }

}