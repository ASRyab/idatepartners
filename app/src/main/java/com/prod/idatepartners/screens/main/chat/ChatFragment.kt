package com.prod.idatepartners.screens.main.chat

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseFragment
import com.prod.idatepartners.screens.main.MainVM
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment : BaseFragment<ChatVM>() {
    override fun layoutId(): Int = R.layout.fragment_chat

    override fun viewModelClass(): Class<ChatVM> = ChatVM::class.java

    lateinit var chatVM: ChatVM

    private val observer = object :
        RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            if (positionStart == 0) {
                rvChat.scrollToPosition(0)
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvChat.layoutManager =
            LinearLayoutManager(view.context, RecyclerView.VERTICAL, true)
        rvChat.itemAnimator = null
        rvChat.setHasFixedSize(true)

        val vm = ViewModelProviders.of(activity!!, viewModelFactory)
            .get(MainVM::class.java)

        chatVM = viewModel as ChatVM

        vm.selectedDrivenIdData.observe(viewLifecycleOwner, Observer { chatVM.drivenUserId = it })
        vm.selectedUserIdData.observe(viewLifecycleOwner, Observer {
            chatVM.messagesLiveData.removeObservers(viewLifecycleOwner)
            chatVM.updateUserId(it)
            chatVM.messagesLiveData.observe(viewLifecycleOwner, Observer { list ->
                chatVM.adapter.submitList(list)
                tvEmptyChat.visibility = if (list.isEmpty()) View.VISIBLE else View.GONE
            })
        })
        chatVM.adapter.registerAdapterDataObserver(observer)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        chatVM.adapter.unregisterAdapterDataObserver(observer)
    }

}