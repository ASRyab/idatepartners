package com.prod.idatepartners.screens.main.chat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.PagedList.BoundaryCallback
import com.prod.idatepartners.api.models.GiftModelApi
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.db.MessageDb
import com.prod.idatepartners.usecases.ChatUseCase
import com.prod.idatepartners.usecases.IGiftUseCase
import com.prod.idatepartners.usecases.ISocketChatUseCase
import com.prod.idatepartners.utils.RxUtils
import io.reactivex.functions.Consumer
import javax.inject.Inject

class ChatVM @Inject constructor(
    private val chatUseCase: ChatUseCase,
    private val socketChatUseCase: ISocketChatUseCase,
    private val giftUseCase: IGiftUseCase
) :
    BaseViewModel() {
    var drivenUserId: String = ""
    var userId: String = ""
    var offset: Int = 0
    var messagesLiveData: LiveData<PagedList<MessageDb>> = MutableLiveData()
    var adapter: ChatAdapter = ChatAdapter(this)
    var giftsList: ArrayList<GiftModelApi> = ArrayList()

    init {
        safeDisposable(
            socketChatUseCase.getChatListener().subscribe(
                RxUtils.emptyDataConsumer
                , RxUtils.getEmptyErrorConsumer(TAG, "initChatListener")
            )
        )

        safeDisposable(
            socketChatUseCase.getChatReadAllListener().subscribe(
                RxUtils.emptyDataConsumer
                , RxUtils.getEmptyErrorConsumer(TAG, "initChatReadAllListener")
            )
        )

        safeDisposable(
            giftUseCase.getAllGifts().subscribe(
                Consumer {
                    giftsList = it
                    adapter.notifyDataSetChanged()
                },
                RxUtils.getEmptyErrorConsumer(TAG, "initGifts")
            )
        )
    }

    fun updateUserId(userId: String) {
        this.userId = userId
        offset = 0
        initChatData()
    }

    private fun initChatData() {
        val factory =
            chatUseCase.getChatHistoryPaged(drivenUserId, this.userId)

        val builder = LivePagedListBuilder<Int, MessageDb>(
            factory,
            PagedList.Config.Builder().setPageSize(PAGE_SIZE).setPrefetchDistance(PREFETCH_DISTANCE).build()
        )
        val boundaryCallback = object : BoundaryCallback<MessageDb>() {
            override fun onItemAtEndLoaded(itemAtEnd: MessageDb) {
                super.onItemAtEndLoaded(itemAtEnd)
                offset += OFFSET
                if (messagesLiveData.value?.size ?: 0 == offset) {
                    chatUseCase.loadMore(drivenUserId, userId, offset)
                }
            }
        }
        builder.setBoundaryCallback(boundaryCallback)
        messagesLiveData = builder.build()
    }

    companion object {
        const val PAGE_SIZE = 50
        const val PREFETCH_DISTANCE = 5
        const val OFFSET = PAGE_SIZE
    }
}