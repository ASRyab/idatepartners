package com.prod.idatepartners.screens.newLetter

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.prod.idatepartners.BR
import com.prod.idatepartners.R
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.base.BaseFragment
import com.prod.idatepartners.screens.choosePhoto.ChoosePhotoVM
import com.prod.idatepartners.utils.hideKeyboard
import com.prod.idatepartners.utils.toast

class NewLetterFragment : BaseFragment<NewLetterVM>() {
    private lateinit var choosePhotoVM: ChoosePhotoVM

    override fun layoutId(): Int = R.layout.fragment_new_letter
    override fun viewModelClass(): Class<NewLetterVM> = NewLetterVM::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        choosePhotoVM = activity?.run {
            ViewModelProviders.of(requireActivity()).get(ChoosePhotoVM::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = viewModel as NewLetterVM
        val navController = Navigation.findNavController(view)

        choosePhotoVM.selected.observe(requireActivity(), Observer { list ->
            (viewModel as NewLetterVM).listLiveData.value =
                list.map {
                    Photo(it.contentId, it.previewUrl, it.url)
                }
        })

        binding.setVariable(
            BR.clickListener,
            View.OnClickListener {
                activity?.onBackPressed()
            })

        vm.letterEvent.observe(this, Observer {
            when (it?.letterStatus) {
                NewLetterVM.LetterStatus.MessageSent -> {
                    hideKeyboard()
                    requireActivity().onBackPressed()
                }
                NewLetterVM.LetterStatus.MessageToShort -> {
                    toast(it.message)
                }
                NewLetterVM.LetterStatus.SentFailed -> {
                    toast(it.message)
                }
                NewLetterVM.LetterStatus.OpenPhotoChooser -> {
                    val args = Bundle()
                    args.putString("driven_user_id", it.message)
                    args.putBoolean("isChat", false)
                    navController.navigate(
                        R.id.choosePhotoFragment,
                        args,
                        NavOptions.Builder()
                            .setPopUpTo(
                                R.id.newLetterFragment,
                                false
                            ).build()
                    )
                }
            }
            (viewModel as NewLetterVM).letterEvent.postValue(
                null
            )
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        choosePhotoVM.clearData()
    }
}
