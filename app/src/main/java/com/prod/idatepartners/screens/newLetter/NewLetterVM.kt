package com.prod.idatepartners.screens.newLetter

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.prod.idatepartners.R
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.screens.choosePhoto.AddPhotoAdapter
import com.prod.idatepartners.usecases.ILetterUseCase
import com.prod.idatepartners.utils.RxUtils
import io.reactivex.functions.Consumer
import javax.inject.Inject

class NewLetterVM @Inject constructor(var letterUseCase: ILetterUseCase, val context: Context) :
    BaseViewModel() {


    lateinit var userId: String
    lateinit var drivenUserId: String
    lateinit var letterId: String
    lateinit var subject: String


    var layoutDirection = LinearLayoutManager.HORIZONTAL
    var subjectText: ObservableField<String> = ObservableField()
    var letterText: ObservableField<String> = ObservableField()
    var letterEvent: MutableLiveData<LetterEvent> = MutableLiveData()
    var listLiveData: MutableLiveData<List<Photo>> = MutableLiveData()

    var adapter = AddPhotoAdapter(listLiveData,
        View.OnClickListener {
            letterEvent.postValue(
                LetterEvent(
                    LetterStatus.OpenPhotoChooser,
                    ""
                )
            )
        })

    override fun passBundle(arguments: Bundle?) {
        userId = arguments?.getString("user_id") ?: ""
        drivenUserId = arguments?.getString("driven_user_id") ?: ""
        letterId = arguments?.getString("letter_id") ?: ""
        subject = arguments?.getString("subject") ?: ""

        subjectText.set(subject)
    }

    init {
        listLiveData.value = arrayListOf()
    }

    fun sendLetter() {
        if (!hasErrors()) {
            val images = listLiveData.value?.map { it.id ?: "" } ?: ArrayList()

            safeDisposable(
                letterUseCase.sendLetter(
                    drivenUserId,
                    userId,
                    letterId,
                    subjectText.get() ?: "",
                    letterText.get() ?: "", images
                )
                    .compose(RxUtils.withLoading(Consumer { isProgress.value = it }))
                    .subscribe(
                        {
                            letterEvent.postValue(LetterEvent(LetterStatus.MessageSent, null))
                        }, {
                            letterEvent.postValue(
                                LetterEvent(
                                    LetterStatus.SentFailed,
                                    "Message sent failed"
                                ) //TODO add list of errors handler
                            )
                        }
                    )
            )
        }
    }

    fun onChoosePhotoClick() {
        letterEvent.postValue(LetterEvent(LetterStatus.OpenPhotoChooser, drivenUserId))
    }

    fun hasErrors(): Boolean {
        var errorMessage = ""
        if (letterText.get()?.length ?: 0 < 200) errorMessage += context.getString(R.string.min_200_characters)
        if ((subjectText.get() ?: "").isEmpty())
            errorMessage += context.getString(R.string.should_consist_subject)
        if (errorMessage.isNotEmpty()) {
            letterEvent.postValue(
                LetterEvent(
                    LetterStatus.SentFailed,
                    errorMessage.substring(0, errorMessage.length - 2)
                )
            )
        }

        return errorMessage.isNotEmpty()
    }

    data class LetterEvent(var letterStatus: LetterStatus, var message: String?)

    enum class LetterStatus {
        MessageSent,
        SentFailed,
        MessageToShort,
        OpenPhotoChooser
    }
}