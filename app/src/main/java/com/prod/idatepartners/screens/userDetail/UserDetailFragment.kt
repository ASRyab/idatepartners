package com.prod.idatepartners.screens.userDetail

import android.os.Bundle
import android.view.View
import com.prod.idatepartners.BR
import com.prod.idatepartners.R
import com.prod.idatepartners.base.BaseFragment

class UserDetailFragment : BaseFragment<UserDetailVM>() {
    override fun layoutId(): Int = R.layout.fragment_user_detail
    override fun viewModelClass(): Class<UserDetailVM> = UserDetailVM::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.clickListener, View.OnClickListener { activity?.onBackPressed() })

    }

}