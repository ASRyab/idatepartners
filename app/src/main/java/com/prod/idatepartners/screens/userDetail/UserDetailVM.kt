package com.prod.idatepartners.screens.userDetail

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.prod.idatepartners.base.BaseViewModel
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.usecases.IUsersUseCase
import com.prod.idatepartners.utils.RxUtils
import io.reactivex.functions.Consumer
import javax.inject.Inject

class UserDetailVM @Inject constructor(private val usersUseCase: IUsersUseCase) : BaseViewModel() {

    var user: MutableLiveData<UserDb> = MutableLiveData()
    var adapter = PhotoAdapter(MutableLiveData())
    var layoutDirection = LinearLayoutManager.HORIZONTAL

    override fun passBundle(arguments: Bundle?) {
        val userId: String = arguments?.getString("user_id") ?: ""
        val drivenUserId: String = arguments?.getString("driven_user_id") ?: ""

        if (userId.isNotEmpty() && drivenUserId.isNotEmpty()) {
            updateUserInfo(userId, drivenUserId)
        }
    }

    private fun updateUserInfo(userId: String, drivenUserId: String) {
        safeDisposable(
            usersUseCase.getUser(userId, drivenUserId)
                .subscribe(Consumer {
                    user.postValue(it)
                    adapter.list.postValue(it.photos)
                }, RxUtils.getEmptyErrorConsumer(TAG, "updateUserInfo"))
        )
    }


    val education = hashMapOf(
        0 to "Not Given",
        1 to "High school",
        2 to "Some college",
        8 to "Still studying",
        9 to "Bachelor's degree",
        10 to "Master's degree/ Doctorate",
        11 to "No degree",
        12 to "Rather not say"
    )


    val build: HashMap<Int, String> = hashMapOf(
        0 to "Not Given",
        2 to "Slim",
        3 to "Average",
        4 to "A few extra pounds",
        5 to "Athletic",
        6 to "Big & Beautiful",
        7 to "Rather not say"
    )

    val height: HashMap<Int, String> = hashMapOf(
        0 to "Not Given",
        1 to "4' ,0\" (1.22m)",
        2 to "4' 1\" (1.24m)",
        3 to "4' 2\" (1.27m)",
        4 to "4' 3\" (1.30m)",
        5 to "4' 4\" (1.32m)",
        6 to "4' 5\" (1.35m)",
        7 to "4' 6\" (1.37m)",
        8 to "4' 7\" (1.40m)",
        9 to "4' 8\" (1.42m)",
        10 to "4' 9\" (1.45m)",
        11 to "4' 10\" (1.47m)",
        12 to "4' 11\" (1.50m)",
        13 to "5' 0\" (1.52m)",
        14 to "5' 1\" (1.55m)",
        15 to "5' 2\" (1.57m)",
        16 to "5' 3\" (1.60m)",
        17 to "5' 4\" (1.63m)",
        18 to "5' 5\" (1.65m)",
        19 to "5' 6\" (1.68m)",
        20 to "5' 7\" (1.70m)",
        21 to "5' 8\" (1.73m)",
        22 to "5' 9\" (1.75m)",
        23 to "5' 10\" (1.78m)",
        24 to "5' 11\" (1.80m)",
        25 to "6' 0\" (1.83m)",
        26 to "6' 1\" (1.85m)",
        27 to "6' 2\" (1.88m)",
        28 to "6' 3\" (1.91m)",
        29 to "6' 4\" (1.93m)",
        30 to "6' 5\" (1.96m)",
        31 to "6' 6\" (1.98m)",
        32 to "6' 7\" (2.01m)",
        33 to "6' 8\" (2.03m)",
        34 to "6' 9\" (2.06m)",
        35 to "6' 10\" (2.08m)",
        36 to "6' 11\" (2.11m)",
        37 to "7' 0\" (2.13m)",
        38 to "Rather not say"
    )

    val weight: HashMap<Int, String> = hashMapOf(
        0 to "Not Given",
        1 to "6st 6lb - 41kg",
        2 to "6st 8lb - 42kg",
        3 to "6st 10lb - 43kg",
        4 to "6st 12lb - 44kg",
        5 to "7st 1lb - 45kg",
        6 to "7st 3lb - 46kg",
        7 to "7st 5lb - 47kg",
        8 to "7st 7lb - 48kg",
        9 to "7st 9lb - 49kg",
        10 to "7st 12lb - 50kg",
        11 to "8st 0lb - 51kg",
        12 to "8st 2lb - 52kg",
        13 to "8st 4lb - 53kg",
        14 to "8st 6lb - 54kg",
        15 to "8st 9lb - 55kg",
        16 to "8st 11lb - 56kg",
        17 to "8st 13lb - 57kg",
        18 to "9st 1lb - 58kg",
        19 to "9st 3lb - 59kg",
        20 to "9st 6lb - 60kg",
        21 to "9st 8lb - 61kg",
        22 to "9st 10lb - 62kg",
        23 to "9st 12lb - 63kg",
        24 to "10st 0lb - 64kg",
        25 to "10st 3lb - 65kg",
        26 to "10st 5lb - 66kg",
        27 to "10st 7lb - 67kg",
        28 to "10st 9lb - 68kg",
        29 to "10st 11lb - 69kg",
        30 to "11st 0lb - 70kg",
        31 to "11st 2lb - 71kg",
        32 to "11st 4lb - 72kg",
        33 to "11st 6lb - 73kg",
        34 to "11st 8lb - 74kg",
        35 to "11st 11lb - 75kg",
        36 to "11st 13lb - 76kg",
        37 to "12st 1lb - 77kg",
        38 to "12st 3lb - 78kg",
        39 to "12st 5lb - 79kg",
        40 to "12st 8lb - 80kg",
        41 to "12st 10lb - 81kg",
        42 to "12st 12lb - 82kg",
        43 to "13st 0lb - 83kg",
        44 to "13st 2lb - 84kg",
        45 to "13st 5lb - 85kg",
        46 to "13st 7lb - 86kg",
        47 to "13st 9lb - 87kg",
        48 to "13st 11lb - 88kg",
        49 to "13st 13lb - 89kg",
        50 to "14st 2lb - 90kg",
        51 to "14st 4lb - 91kg",
        52 to "14st 6lb - 92kg",
        53 to "14st 8lb - 93kg",
        54 to "14st 10lb - 94kg",
        55 to "14st 13lb - 95kg",
        56 to "15st 1lb - 96kg",
        57 to "15st 3lb - 97kg",
        58 to "15st 5lb - 98kg",
        59 to "15st 7lb - 99kg",
        60 to "15st 10lb - 100kg",
        61 to "15st 12lb - 101kg",
        62 to "16st 0lb - 102kg",
        63 to "16st 2lb - 103kg",
        64 to "16st 4lb - 104kg",
        65 to "16st 7lb - 105kg",
        66 to "16st 9lb - 106kg",
        67 to "16st 11lb - 107kg)",
        68 to "16st 13lb - 108kg",
        69 to "17st 1lb - 109kg",
        70 to "17st 4lb - 110kg",
        71 to "17st 6lb - 111kg",
        72 to "17st 8lb - 112kg",
        73 to "17st 10lb - 113kg)",
        74 to "17st 12lb - 114kg)",
        75 to "18st 1lb - 115kg",
        76 to "18st 3lb - 116kg",
        77 to "18st 5lb - 117kg",
        78 to "18st 7lb - 118kg",
        79 to "18st 9lb - 119kg",
        80 to "18st 12lb - 120kg",
        81 to "19st 0lb - 121kg",
        82 to "19st 2lb - 122kg",
        83 to "19st 4lb - 123kg",
        84 to "19st 6lb - 124kg",
        85 to "19st 9lb - 125kg",
        86 to "19st 11lb - 126kg",
        87 to "19st 13lb - 127kg",
        88 to "20st 1lb - 128kg",
        89 to "20st 3lb - 129kg",
        90 to "20st 6lb - 130kg",
        91 to "20st 8lb - 131kg",
        92 to "20st 10lb - 132kg",
        93 to "20st 12lb - 133kg",
        94 to "21st 0lb - 134kg",
        95 to "21st 3lb - 135kg",
        96 to "21st 5lb - 136kg",
        97 to "21st 7lb - 137kg",
        98 to "21st 9lb - 138kg",
        99 to "21st 11lb - 139kg",
        100 to "Rather not say"
    )
}