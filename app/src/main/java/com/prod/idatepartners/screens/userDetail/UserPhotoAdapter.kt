package com.prod.idatepartners.screens.userDetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.prod.idatepartners.BR
import com.prod.idatepartners.R
import com.prod.idatepartners.api.models.profiles.Photo

class PhotoAdapter(var list: MutableLiveData<List<Photo>>) :
    RecyclerView.Adapter<UrlPhotoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UrlPhotoViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val binding: ViewDataBinding = DataBindingUtil.inflate(
            inflater, R.layout.item_photo,
            parent,
            false
        )
        return UrlPhotoViewHolder(binding)
    }

    override fun getItemCount(): Int = list.value?.size ?: 0

    override fun onBindViewHolder(holder: UrlPhotoViewHolder, position: Int) {
        list.value?.let { holder.bind(it[position]) }
    }

}

class UrlPhotoViewHolder(var binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(photo: Photo) {
        binding.setVariable(BR.photo, photo)
        binding.executePendingBindings()

    }
}

