package com.prod.idatepartners.usecases

import com.prod.idatepartners.api.RequestManager
import com.prod.idatepartners.base.BaseUseCase
import io.reactivex.Single

class AutoLoginUseCase(
    val requestManager: RequestManager
) : BaseUseCase(), IAutoLoginUseCase {

    override fun tryAutologin(): Single<Any> =
        requestManager.currentSessionObservable().filter { session -> session.refreshToken != null }
            .map { it.refreshToken }
            .flatMapSingle { token -> requestManager.refreshToken(token) }
}

interface IAutoLoginUseCase {
    fun tryAutologin(): Single<Any>
}