package com.prod.idatepartners.usecases

import androidx.paging.DataSource
import com.prod.idatepartners.base.BaseUseCase
import com.prod.idatepartners.db.MessageDb
import com.prod.idatepartners.repo.IChatRepository

class ChatUseCase(private val chatRepository: IChatRepository)  : BaseUseCase() {

    fun getChatHistoryPaged(drivenUserId: String, userId: String): DataSource.Factory<Int, MessageDb> =
        chatRepository.getChatHistoryPaged(drivenUserId, userId)

    fun loadMore(drivenUserId: String, userId: String, offset: Int) {
        chatRepository.loadNext(drivenUserId, userId, offset)
    }
}