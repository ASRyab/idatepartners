package com.prod.idatepartners.usecases

import com.prod.idatepartners.api.models.GiftModelApi
import com.prod.idatepartners.base.BaseUseCase
import com.prod.idatepartners.repo.GiftRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

class GiftUseCase(private val giftRepository: GiftRepository) : BaseUseCase(), IGiftUseCase  {

    override fun getAllGifts(): Single<ArrayList<GiftModelApi>> = giftRepository.getAllGifts()
        .map {
            val values = it.values
            val result = arrayListOf<GiftModelApi>()
            values.forEach { t: List<GiftModelApi> -> result.addAll(t) }
            return@map result
        }
        .observeOn(AndroidSchedulers.mainThread())

}

interface IGiftUseCase {
    fun getAllGifts(): Single<ArrayList<GiftModelApi>>
}
