package com.prod.idatepartners.usecases

import androidx.paging.DataSource
import com.prod.idatepartners.base.BaseUseCase
import com.prod.idatepartners.db.LetterAndUserDb
import com.prod.idatepartners.db.LetterDb
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.repo.IDrivenUserRepository
import com.prod.idatepartners.repo.ILettersRepository
import com.prod.idatepartners.repo.IUserRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LetterUseCase(
    private val lettersRepository: ILettersRepository,
    private val userRepository: IUserRepository,
    private val drivenUserRepository: IDrivenUserRepository
)  : BaseUseCase(), ILetterUseCase {

    override fun getLetterHistoryPaged(
        drivenUserId: String, userId: String
    ): DataSource.Factory<Int, LetterAndUserDb> =
        lettersRepository.getLetterHistoryPaged(drivenUserId, userId)

    override fun loadMore(drivenUserId: String, userId: String, offset: Int) {
        lettersRepository.loadNext(drivenUserId, userId, offset)
    }

    override fun sendLetter(
        drivenId: String,
        userId: String,
        replyToId: String,
        subject: String,
        text: String,
        images: List<String>?
    ): Single<Any> =
        lettersRepository.sendLetter(drivenId, userId, replyToId, subject, text, images)
            .observeOn(AndroidSchedulers.mainThread())


    override fun getLetterById(
        drivenId: String,
        userId: String,
        letterId: String
    ): Observable<LetterAndUserDb> {
        return lettersRepository.getLetterApiById(drivenId, userId, letterId)
    }

    override fun getLettersListener(): Observable<*> {
        return lettersRepository.getLettersListener()
            .flatMapSingle { message ->
                updateUserCounter(message)
                    .flatMap { needUpdate ->
                        if (needUpdate) {
                            updateDrivenUserCounter(message)
                        } else {
                            Single.just(needUpdate)
                        }
                    }
            }
    }

    private fun updateDrivenUserCounter(message: LetterDb): Single<DrivenUserDb> {
        return drivenUserRepository
            .getDrivenUserById(message.recipientId)
            .firstOrError()
            .doOnSuccess { it.countUnread++ }
            .observeOn(Schedulers.io())
            .doOnSuccess { drivenUserRepository.update(it) }
    }

    private fun updateUserCounter(message: LetterDb): Single<Boolean> = userRepository
        .getUserById(message.senderId, message.recipientId)
        .firstOrError()
        .observeOn(Schedulers.io())
        .map {
            val oldMail = it.mailCount
            val oldLetter = it.letterCount
            it.letterCount++
            userRepository.saveUser(it)
            (oldMail == 0 && oldLetter == 0)
        }
}

interface ILetterUseCase {
    fun sendLetter(
        drivenId: String,
        userId: String,
        replyToId: String,
        subject: String,
        text: String,
        images: List<String>?
    ): Single<Any>

    fun getLetterHistoryPaged(
        drivenUserId: String, userId: String
    ): DataSource.Factory<Int, LetterAndUserDb>

    fun loadMore(drivenUserId: String, userId: String, offset: Int)
    fun getLetterById(
        drivenId: String,
        userId: String,
        letterId: String
    ): Observable<LetterAndUserDb>

    fun getLettersListener(): Observable<*>
}