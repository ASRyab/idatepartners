package com.prod.idatepartners.usecases

import com.prod.idatepartners.api.models.AuthData
import com.prod.idatepartners.base.BaseUseCase
import com.prod.idatepartners.repo.LoginRepository
import io.reactivex.Single

class LoginUseCase(private val loginRepository: LoginRepository) : BaseUseCase(), ILoginUseCase {

    override fun loginRequest(name: String, pass: String): Single<AuthData> =
        loginRepository.loginRequest(name, pass)
}

interface ILoginUseCase {
    fun loginRequest(name: String, pass: String): Single<AuthData>
}