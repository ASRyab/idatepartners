package com.prod.idatepartners.usecases

import com.prod.idatepartners.api.RequestManager
import com.prod.idatepartners.base.BaseUseCase
import com.prod.idatepartners.repo.IChatRepository
import com.prod.idatepartners.repo.IDrivenUserRepository
import com.prod.idatepartners.repo.ILettersRepository
import com.prod.idatepartners.repo.IUserRepository
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class LogoutUseCase(
    var requestManager: RequestManager,
    private val chatRepository: IChatRepository,
    private val drivenUserRepository: IDrivenUserRepository,
    private val userRepository: IUserRepository,
    private val lettersRepository: ILettersRepository
)  : BaseUseCase(),ILogoutUseCase {

    override fun logout(): Observable<Unit> {
      return  Observable.fromCallable {
            requestManager.clearSession()
            chatRepository.clear()
            drivenUserRepository.clear()
            lettersRepository.clear()
            userRepository.clear()
        }.subscribeOn(Schedulers.io())

    }
}

interface ILogoutUseCase {
    fun logout(): Observable<Unit>
}
