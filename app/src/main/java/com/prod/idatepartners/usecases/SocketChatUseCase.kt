package com.prod.idatepartners.usecases

import com.prod.idatepartners.base.BaseUseCase
import com.prod.idatepartners.repo.IChatRepository
import com.prod.idatepartners.repo.IDrivenUserRepository
import com.prod.idatepartners.repo.IUserRepository
import com.prod.idatepartners.screens.choosePhoto.ContentView
import com.prod.idatepartners.utils.Debug
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SocketChatUseCase(
    private val chatRepository: IChatRepository,
    private val userRepository: IUserRepository,
    private val drivenUserRepository: IDrivenUserRepository
) : BaseUseCase(), ISocketChatUseCase {
    override fun getChatReadAllListener(): Observable<*> {
        return chatRepository.getChatReadAllListener()
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun sendMessage(message: String, drivenId: String, userId: String): Single<*> {
        return chatRepository.sendMessage(message, drivenId, userId)
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun sendImage(message: ContentView, drivenId: String, userId: String): Single<*> {
        return chatRepository.sendImage(drivenId, userId, message)
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getChatListener(): Observable<*> {
        return chatRepository.getChatListener()
            .flatMapSingle { message ->
                userRepository
                    .getUserById(message.senderId, message.recipientId)
                    .firstOrError()
                    .observeOn(Schedulers.io())
                    .map {
                        val oldMail = it.mailCount
                        val oldLetter = it.letterCount
                        it.mailCount++
                        Debug.logD(TAG, "getChatListener.saveUser=${it.userId} ${it.mailCount}")
                        userRepository.saveUser(it)
                        (oldMail == 0 && oldLetter == 0)
                    }
                    .flatMap { needUpdate ->
                        if (needUpdate) {
                            drivenUserRepository
                                .getDrivenUserById(message.recipientId)
                                .firstOrError()
                                .doOnSuccess { it.countUnread++ }
                                .observeOn(Schedulers.io())
                                .doOnSuccess { drivenUserRepository.update(it) }
                        } else {
                            Single.just(needUpdate)
                        }
                    }
            }
    }


    override fun setReadAll(drivenUserId: String, userId: String): Single<Any> =
        chatRepository.setReadAll(drivenUserId, userId)
            .flatMap {
                userRepository
                    .getUserById(userId, drivenUserId)
                    .firstOrError()
                    .observeOn(Schedulers.io())
                    .map {
                        val oldMail = it.mailCount
                        val oldLetter = it.letterCount
                        Debug.logD(TAG, "setReadAll.saveUser=${it.userId} ${it.mailCount}")
                        if (oldMail > 0) {
                            it.mailCount = 0
                            userRepository.saveUser(it)
                        }
                        Debug.logD(TAG, "needUpdate=${(oldMail > 0 && oldLetter == 0)}")
                        (oldMail > 0 && oldLetter == 0)
                    }
                    .flatMap { needUpdate ->
                        if (needUpdate) {
                            drivenUserRepository
                                .getDrivenUserById(drivenUserId)
                                .firstOrError()
                                .doOnSuccess {
                                    if (it.countUnread > 0) {
                                        it.countUnread--
                                    }
                                }
                                .observeOn(Schedulers.io())
                                .doOnSuccess { drivenUserRepository.update(it) }
                        } else {
                            Single.just(needUpdate)
                        }
                    }
            }

    override fun sendGift(fromId: String, toId: String, giftId: Int, categoryId: Int): Single<*> {
        return chatRepository.sendGift(fromId, toId, giftId, categoryId)
            .observeOn(AndroidSchedulers.mainThread())
    }
}

interface ISocketChatUseCase {
    fun sendMessage(message: String, drivenId: String, userId: String): Single<*>
    fun sendImage(message: ContentView, drivenId: String, userId: String): Single<*>
    fun getChatListener(): Observable<*>
    fun getChatReadAllListener(): Observable<*>
    fun setReadAll(drivenUserId: String, userId: String): Single<Any>
    fun sendGift(fromId: String, toId: String, giftId: Int, categoryId: Int): Single<*>
}
