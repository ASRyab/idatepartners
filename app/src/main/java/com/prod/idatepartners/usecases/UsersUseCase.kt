package com.prod.idatepartners.usecases

import com.prod.idatepartners.base.BaseUseCase
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.repo.IDrivenUserRepository
import com.prod.idatepartners.repo.IUserRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class UsersUseCase(
    private val drivenUserRepository: IDrivenUserRepository,
    private val userRepository: IUserRepository
) : BaseUseCase() ,IUsersUseCase{

    override fun getDrivenUsers(): Observable<MutableList<DrivenUserDb>> =
        drivenUserRepository.getDrivenUsers()
            .observeOn(
                AndroidSchedulers.mainThread()
            )

    override fun getDrivenById(userId: String): Observable<DrivenUserDb> =
        drivenUserRepository.getDrivenUserById(userId)
            .observeOn(
                AndroidSchedulers.mainThread()
            )

    override fun getUsers(drivenUserId: String): Observable<MutableList<UserDb>> =
        userRepository.getUsersByDriven(drivenUserId).observeOn(AndroidSchedulers.mainThread())

    override fun getUser(userId: String, drivenUserId: String): Observable<UserDb> =
        userRepository.getUserById(userId, drivenUserId).observeOn(AndroidSchedulers.mainThread())
}

interface IUsersUseCase {
    fun getDrivenUsers(): Observable<MutableList<DrivenUserDb>>
    fun getDrivenById(userId: String): Observable<DrivenUserDb>
    fun getUsers(drivenUserId: String): Observable<MutableList<UserDb>>
    fun getUser(userId: String, drivenUserId: String): Observable<UserDb>
}