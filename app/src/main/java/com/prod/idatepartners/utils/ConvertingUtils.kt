package com.prod.idatepartners.utils

import java.text.SimpleDateFormat
import java.util.*


class ConvertingUtils {

    companion object {

        const val DATE_FORMAT_LETTER_DETAIL = "dd MMM, hh:mm a"
        const val DATE_FORMAT_ONLY_DATE = "dd MMM yyyy"
        const val DATE_FORMAT_TIME = "HH:mm"

        @JvmStatic
        fun getStringDateByPattern(timeSTamp: Long?, pattern: String): String {
            return if (timeSTamp == null || timeSTamp == 0L) ""
            else {
                val date = Date(timeSTamp * 1000)
                val formatter = SimpleDateFormat(pattern, Locale.UK)
                formatter.timeZone = TimeZone.getDefault()
                formatter.format(date)
            }

        }
    }

}