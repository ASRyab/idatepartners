package com.prod.idatepartners.utils

import android.util.Log

object Debug {

    val TAG = "Debug"

    @JvmField
    var isEnabled = true
    private var isLiveServer = true

    private val isLogEnabled: Boolean
        get() = isEnabled || !isLiveServer

    @JvmStatic
    fun logD(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.d(TAG, msg)
        }
    }

    @JvmStatic
    fun logI(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.i(TAG, msg)
        }
    }

    @JvmStatic
    fun logW(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.w(TAG, msg)
        }
    }

    @JvmStatic
    fun logE(TAG: String, msg: String, e: Throwable? = null) {
        if (isLogEnabled) {
            Log.e(TAG, msg)
        }
    }

    @JvmStatic
    fun logE(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.e(TAG, msg)
        }
    }

    fun initDebug(isReleaseServer: Boolean) {
        //todo fix in future
        isEnabled = true
        Debug.isLiveServer = isReleaseServer
    }

    fun logException(e: Exception?) {
        if (isLogEnabled && e != null) {
            Log.e("Exception", e.toString())
        }
    }
}
