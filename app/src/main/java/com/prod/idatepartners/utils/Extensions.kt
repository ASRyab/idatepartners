package com.prod.idatepartners.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.prod.idatepartners.api.models.LetterPhoto
import com.prod.idatepartners.api.models.SingleLetterApi
import com.prod.idatepartners.api.models.chat.MessageApi
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.api.models.profiles.UserApi
import com.prod.idatepartners.api.rpc.rpc_actions.MessageSocket
import com.prod.idatepartners.db.LetterDb
import com.prod.idatepartners.db.MessageDb
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.screens.main.UserView

fun DrivenUserDb.toUserView(): UserView =
    UserView(
        this.id,
        this.firstName + " " + this.lastName,
        this.age,
        this.countUnread,
        0,
        this.primaryPhoto.photoUrl,
        true,
        true
    )


fun UserDb.toUserView(): UserView =
    UserView(
        userId,
        firstName + " " + lastName,
        age,
        letterCount,
        mailCount,
        primaryPhoto?.photoUrl,
        false,
        isOnline
    )


fun SingleLetterApi.toDb(): LetterDb =
    LetterDb(
        this.id,
        this.senderId,
        this.recipientId,
        this.isRead,
        this.subject,
        this.type,
        this.replyToId,
        this.timestamp,
        this.direction,
        this.senderName,
        this.hasRepliedSubject,
        this.text,
        this.images
    )

fun MessageSocket.toMessageDb(): MessageDb = MessageDb(
    this.messageId,
    this.fromId,
    this.toId,
    null,
    false,
    this.subject,
    this.text,
    this.msgType,
    this.type,
    this.timestamp,
    this.resources?.privateChatImage

)

fun MessageSocket.toLetterDb(): LetterDb = LetterDb(
    this.messageId,
    this.fromId,
    this.toId,
    false,
    this.subject,
    this.type,
    null,
    this.timestamp,
    "in",
    this.firstName,
    false,
    this.text,
    ArrayList()
)

fun MessageApi.toDb(): MessageDb = MessageDb(
    this.id,
    this.senderId,
    this.recipientId,
    this.viewedAt,
    this.isRead,
    this.subject,
    this.text,
    this.type,
    "mail",
    this.timestamp,
    this.resources?.privateChatImage
)

fun UserApi.toDb(drivenId: String, letterCount: Int?, mailCount: Int?): UserDb {
    val u = this
    return UserDb(
        u.primaryPhoto,
        u.build,
        u.height,
        u.weight,
        u.education,
        u.hobbiesAndInterests,
        u.id,
        u.login,
        u.firstName,
        u.lastName,
        u.gender,
        u.age,
        u.country,
        u.city,
        u.registeredAt,
        u.isFriend,
        u.isOnline,
        drivenId,
        u.photos,
        letterCount ?: 0,
        mailCount ?: 0,
        u.lastMessageTime
    )
}

fun LetterPhoto.toPhoto(): Photo = Photo(this.key, this.url, null)

fun Fragment.toast(message: String?) =
    Toast.makeText(view?.context, message, Toast.LENGTH_SHORT).show()

fun Fragment.toast(message: Int) =
    Toast.makeText(view?.context, message, Toast.LENGTH_SHORT).show()

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

