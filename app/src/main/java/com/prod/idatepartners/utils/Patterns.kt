package com.prod.idatepartners.utils

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class Patterns {
    companion object {

        val EMAIL_ADDRESS = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )

        /**
         * This pattern is intended for searching for things that look like they
         * might be phone numbers in arbitrary text, not for validating whether
         * something is in fact a phone number. It will miss many things that
         * are legitimate phone numbers.
         *
         *
         * The pattern matches the following:
         *
         *  * Optionally, a + sign followed immediately by one or more digits. Spaces, dots, or dashes may follow.
         *  * Optionally, sets of digits in parentheses, separated by spaces, dots, or dashes.
         *  * A string starting and ending with a digit, containing digits, spaces, dots, and/or dashes.
         *
         */
        val PHONE = Pattern.compile( // sdd = space, dot, or dash
            ("(\\+[0-9]+[\\- \\.]*)?" // +<digits><sdd>*

                    + "(\\([0-9]+\\)[\\- \\.]*)?" // (<digits>)<sdd>*

                    + "([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])")
        ) // <digit><digit|sdd>+<digit>

        val CHARACTERS_PATTERN = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)

        val DATE_FORMAT_LONG = SimpleDateFormat("EEEE, d MMM", Locale.US)
        val DATE_FORMAT_SHORT = SimpleDateFormat("d MMM", Locale.US)
        val TIME_FORMAT = SimpleDateFormat("h:mm a z", Locale.US)
        val DATE_TIME_FORMAT = SimpleDateFormat("MMM d, h:mm", Locale.US)
        val DATE_TIME_FORMAT_LONG = SimpleDateFormat("EEEE, d MMM, h:mm a z", Locale.US)
        val DATE_FORMAT_LINKED_DIALOG = SimpleDateFormat("dd MMM yyyy 'at' HH:mm", Locale.US)
    }

}