package com.prod.idatepartners.utils

import android.content.Context
import android.text.TextUtils
import com.google.gson.Gson
import com.prod.idatepartners.api.ServerSession
import com.prod.idatepartners.api.ServerSession.Companion.EMPTY_SESSION
import com.prod.idatepartners.utils.defaults.PreferenceHelper

class PreferenceManager constructor(context: Context, private val gson: Gson) : PreferenceHelper(context) {

    var refreshToken: String?
        get() = getStringValue(REFRESH_TOKEN, true)
        set(value) {
            storeValue(REFRESH_TOKEN, value, true)
            refreshTokenLastUpdateTime = System.currentTimeMillis()
        }

    var isInstallTracked: Boolean
        get() = getBooleanValue(IS_TRACK_INSTALL, true, false)
        set(isTrackInstall) = storeValue(IS_TRACK_INSTALL, isTrackInstall, true)

    var keyReferrer: String?
        get() = getStringValue(INSTALL_REFERRER, true)
        set(referrer) = storeValue(INSTALL_REFERRER, referrer, true)

    var refreshTokenLastUpdateTime: Long
        get() = getLongValue(REFRESH_TOKEN_LAST_UPDATE_TIME, false)
        set(time) = storeValue(REFRESH_TOKEN_LAST_UPDATE_TIME, time, false)

    var currentUserId: String?
        get() = getStringValue(CURRENT_USER_ID, false)
        set(id) = storeValue(CURRENT_USER_ID, id, false)

    var serverSession: ServerSession
        get() {
            val stringValue = getStringValue(SESSION, true)
            return if (TextUtils.isEmpty(stringValue)) {
                EMPTY_SESSION
            } else {
                gson.fromJson(stringValue, ServerSession::class.java)
            }
        }
        set(session) {
            storeValue(SESSION, gson.toJson(session), true)
        }


    fun clearAll() {
        removeValues(USER_SESSION_KEYS)
    }

    companion object {
        /**
         * Value who not need USER_ID
         */
        // AuthData
        private const val REFRESH_TOKEN = "refresh_token"
        private const val IS_TRACK_INSTALL = "isTrackInstall"
        private const val INSTALL_REFERRER = "install_referrer"
        private const val REFRESH_TOKEN_LAST_UPDATE_TIME = "refresh_token_last_update_time"

        /**
         * Value who need USER_ID
         */
        private const val CURRENT_USER_ID = "current_user_id"
        private const val SESSION = "session"

        private val USER_SESSION_KEYS = setOf(
            REFRESH_TOKEN, REFRESH_TOKEN_LAST_UPDATE_TIME,
            CURRENT_USER_ID, SESSION
        )

    }

}