package com.prod.idatepartners.utils

import android.text.TextUtils
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.functions.Consumer

object RxUtils {
    private val TAG = RxUtils::class.java.name

    val emptyDataConsumer: Consumer<Any> = Consumer { }

    val emptyErrorConsumer: Consumer<Throwable>
        get() = getEmptyErrorConsumer(null, null)


    fun getEmptyErrorConsumer(className: String?, methodName: String? = null): Consumer<Throwable> {
        return Consumer { throwable ->
            val tag = if (TextUtils.isEmpty(className)) TAG else "ClassName : " + className!!
            val errorMsg =
                (if (TextUtils.isEmpty(methodName)) "" else "MethodName : $methodName\n") +
                        "Error : " +
                        throwable.toString()
//            if (Config.isLogging()) {
            Debug.logE(tag, errorMsg, throwable)
//            }

        }
    }

    fun <T> withLoading(showLoading: Consumer<Boolean>): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream
                .doOnSubscribe { showLoading.accept(true) }
                .doOnError { showLoading.accept(false) }
                .doOnSuccess { showLoading.accept(false) }
        }
    }

    fun <T> withLoadingObservable(showLoading: Consumer<Boolean>): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream
                .doOnSubscribe { showLoading.accept(true) }
                .doOnError { showLoading.accept(false) }
                .doOnNext { showLoading.accept(false) }
                .doOnComplete { showLoading.accept(false) }
        }
    }
}