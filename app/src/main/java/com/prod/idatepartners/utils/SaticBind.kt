package com.prod.idatepartners.utils

import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.prod.idatepartners.R
import com.prod.idatepartners.utils.ui.RedPointButton
import com.prod.idatepartners.utils.ui.RoundedImageView

@BindingAdapter("lm")
fun lm(view: RecyclerView, direction: Int) {
    view.layoutManager = LinearLayoutManager(view.context, direction, false)
}

@BindingAdapter("android:visibilityAnim")
fun visibilityAnim(view: View, visibility: Boolean) {
    view.visibility = if (visibility) View.VISIBLE else View.GONE
}

@BindingAdapter("boldFont")
fun boldFont(view: TextView, isBold: Boolean) {
    view.typeface =
        ResourcesCompat.getFont(
            view.context,
            (if (isBold) R.font.roboto_medium else R.font.roboto_regular)
        )
}

@BindingAdapter("colorFilter")
fun filter(view: RoundedImageView, shouldTint: Boolean) {
    if (shouldTint) {
        val color = view.context.getColor(R.color.imageTint)
        val cf: ColorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY)
        view.setColorFilter(cf)
    } else view.setColorFilter(ColorFilter())
}

@BindingAdapter("roundedImageView")
fun loadImage(view: RoundedImageView, url: String?) {
    Glide.with(view.context).load(url).into(view)
}

@BindingAdapter("imageRoundUrl")
fun loadImage(view: ImageView, url: String?) {
    var requestOptions = RequestOptions()
    requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
    Glide.with(view.context).load(url)
        .apply(requestOptions).into(view)
}

@BindingAdapter("imageUrl")
fun loadImages(view: ImageView, url: String?) {
    var requestOptions = RequestOptions()
    requestOptions = requestOptions.transforms(CenterCrop())
    Glide.with(view.context).load(url)
        .apply(requestOptions).into(view)
}

@BindingAdapter("imageUrlNoCrop")
fun loadImageNoCrop(view: ImageView, url: String?) {
    val circularProgressDrawable = CircularProgressDrawable(view.context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()

    Glide.with(view.context).load(url)
        .into(view)

}

@BindingAdapter("bgRedPoint")
fun bgRedPoint(view: RedPointButton, isSelected: Boolean) {
    view.setBackground(isSelected)
}

@BindingAdapter("redPointEnabled")
fun redPointEnabled(view: RedPointButton, isEnabled: Boolean) {
    view.setRedPointEnabled(isEnabled)
}
