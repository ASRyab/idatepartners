package com.prod.idatepartners.utils

import io.reactivex.subjects.BehaviorSubject

class UiHandler {
    val haveUiSubject = BehaviorSubject.create<Boolean>()
}