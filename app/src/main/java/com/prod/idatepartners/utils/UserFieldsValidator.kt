package com.prod.idatepartners.utils

import android.content.Context
import android.text.TextUtils
import com.prod.idatepartners.R
import io.reactivex.functions.Consumer


class UserFieldsValidator(val context: Context) {

    private fun validateNotEmpty(line: String): Boolean {
        return !TextUtils.isEmpty(line)
    }

    private fun validateEmailPattern(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun validatePasswordMinLength(password: String): Boolean {
        return password.length >= 6
    }

    private fun validatePasswordMaxLength(password: String): Boolean {
        return password.length <= 20
    }

    private fun validateNoSpaces(line: String): Boolean {
        return !line.contains(" ")
    }

    private fun validateInvalidCharacters(userName: String): Boolean {
        return !Patterns.CHARACTERS_PATTERN.matcher(userName).find()
    }


    fun validatePassword(str: String, setError: Consumer<String>): Boolean {
        try {
            if (str.isEmpty()) {
                setError.accept(context.getString(R.string.password_error_empty))
                return false
            }
            if (!validatePasswordMaxLength(str)) {
                setError.accept(context.getString(R.string.validator_error_password_length))
                return false
            }
            if (!validatePasswordMinLength(str)) {
                setError.accept(context.getString(R.string.validator_error_password_length))
                return false
            }
            if (!validateInvalidCharacters(str) || !validateNoSpaces(str)) {
                setError.accept(context.getString(R.string.password_error))
                return false
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return true
    }

    fun validateEmail(str: String, setError: Consumer<String>): Boolean {
        if (!validateEmailPattern(str) || !validateNotEmpty(str)) {
            try {
                setError.accept(context.getString(R.string.email_error))
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return false
        }
        return true
    }
}