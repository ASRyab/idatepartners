package com.prod.idatepartners.utils.defaults

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

abstract class PreferenceHelper protected constructor(context: Context) {
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    protected fun getStringValue(key: String, useDecryption: Boolean): String? {
        val value = prefs.getString(key, null)
        return if (useDecryption && value != null) {
            SecurityHelper.decodeString(value)
        } else {
            value
        }
    }

    protected fun getStringValue(key: String, useDecryption: Boolean, defaultValue: String): String? {
        var value = getStringValue(key, useDecryption)
        if (value == null) {
            value = defaultValue
        }
        return value
    }

    protected fun getLongValue(key: String, useDecryption: Boolean): Long {
        val stringValue = getStringValue(key, useDecryption)
        return if (stringValue == null) {
            0L
        } else {
            java.lang.Long.valueOf(stringValue)
        }
    }

    protected fun getIntValue(key: String, useDecryption: Boolean): Int {
        val stringValue = getStringValue(key, useDecryption)
        return if (stringValue == null) {
            -1
        } else {
            Integer.valueOf(stringValue)
        }
    }

    @JvmOverloads
    protected fun getBooleanValue(key: String, useDecryption: Boolean, defaultValue: Boolean = false): Boolean {
        val stringValue = getStringValue(key, useDecryption)
        return if (stringValue == null) {
            defaultValue
        } else {
            java.lang.Boolean.valueOf(stringValue)
        }
    }

    protected fun <U> storeValue(key: String, value: U?, useEncryption: Boolean) {
        val resultValue: String = if (useEncryption) {
            SecurityHelper.encodeString(value.toString())
        } else {
            value.toString()
        }
        prefs.edit().putString(key, resultValue).apply()
    }

    protected fun <U> removeValue(key: String) {
        prefs.edit().remove(key).apply()
    }

    protected fun removeValues(keys: Iterable<String>) {
        val editor = prefs.edit()
        for (key in keys) {
            editor.remove(key)
        }
        editor.apply()
    }

    protected fun containsValue(key: String): Boolean {
        return prefs.contains(key)
    }
}