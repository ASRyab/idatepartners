package com.prod.idatepartners.utils.defaults;

import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


public class SecurityHelper {
    /* Security */
    private static final char[] SEED_CHARS = "thisisseed".toCharArray();
    private static final int KEY_SIZE = 32;
    private static final byte[] SALT = new byte[KEY_SIZE];

    private static final int INTERACTION_COUNT = 100;
    private static final int KEY_SIZE_INT_BITS = 8;

    static {
        Arrays.fill(SALT, (byte) 1);
    }

    private static final String ALG = "AES";

    private static SecretKey getRawKeyNew() {
        KeySpec keySpec = new PBEKeySpec(SEED_CHARS, SALT,
                INTERACTION_COUNT, KEY_SIZE * KEY_SIZE_INT_BITS);
        try {
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
            return new SecretKeySpec(keyBytes, ALG);
        } catch (Exception e) {
            throw new RuntimeException("Deal with exceptions properly!", e);
        }

    }

    public static String encodeString(String str) {
        if (str == null)
            return null;

        byte[] result = null;
        try {
            Cipher cipher = Cipher.getInstance(ALG);
            cipher.init(Cipher.ENCRYPT_MODE, getRawKeyNew());

            result = cipher.doFinal(str.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Base64.encodeBytes(result);
    }

    public static String decodeString(String encrypted) {
        String result = null;
        try {
            Cipher cipher = Cipher.getInstance(ALG);
            cipher.init(Cipher.DECRYPT_MODE, getRawKeyNew());

            byte[] encryptedBytes = Base64.decode(encrypted);
            byte[] decrypted = cipher.doFinal(encryptedBytes);
            result = new String(decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
