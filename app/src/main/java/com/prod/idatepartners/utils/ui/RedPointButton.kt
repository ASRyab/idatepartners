package com.prod.idatepartners.utils.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.prod.idatepartners.R
import kotlinx.android.synthetic.main.red_point_button.view.*


class RedPointButton : LinearLayout {

    var text: String? = ""

    constructor(context: Context) : this(context, null) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        val typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RedPointButton)
        text = typedArray.getString(R.styleable.RedPointButton_label)
        typedArray.recycle()
        init()
    }

    private fun init() {
        inflate(context, R.layout.red_point_button, this)
        tvName.text = text
    }


    fun setRedPointEnabled(redPointEnabled: Boolean) {
        ivRedPoint.visibility = if (redPointEnabled) View.VISIBLE else View.GONE
    }

    fun setBackground(isSelected: Boolean) {
        setBackgroundResource(if (isSelected) R.drawable.white_rounded_drawable else R.drawable.gray_rounded_drawable)
        tvName.setTextColor(
            context.getColor(if (isSelected) R.color.colorAccent else R.color.text_gray)
        )
    }

}