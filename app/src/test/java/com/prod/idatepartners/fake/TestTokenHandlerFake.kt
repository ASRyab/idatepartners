package com.prod.idatepartners.fake

import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.base.ITokenHandler
import io.reactivex.Single

class TestTokenHandlerFake : ITokenHandler {
    override fun <T : Any> makeRequest(single: Single<BaseModelResponse<T>>): Single<T> {
        return Single.just(single.blockingGet().data)
    }
}