package com.prod.idatepartners.repository

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.chat.ChatPhoto
import com.prod.idatepartners.api.models.chat.MessageEcho
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.api.requests.ChatApi
import com.prod.idatepartners.api.rpc.ISocketManager
import com.prod.idatepartners.api.rpc.RPCResponse
import com.prod.idatepartners.api.rpc.rpc_actions.SendSocketMessage
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.MessageDao
import com.prod.idatepartners.db.MessageDb
import com.prod.idatepartners.db.users.UserDao
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.repo.ChatRepository
import com.prod.idatepartners.screens.choosePhoto.ContentView
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ChatRepositoryTest {
    private lateinit var chatRepository: ChatRepository

    @Mock
    private lateinit var chatApi: ChatApi
    @Mock
    private lateinit var tokenHandler: ITokenHandler
    @Mock
    private lateinit var messageDao: MessageDao
    @Mock
    private lateinit var userDao: UserDao
    @Mock
    private lateinit var socketManager: ISocketManager


    private lateinit var userDb: UserDb
    private val userId = "111"
    private val drivenId = "222"
    private val msgText = "123 text"
    private val lastMessageTime: Long = 123
    private val giftId = 1
    private val giftCategoryId = 2

    private val contentView = ContentView("1", "", "", "", "", "", "", false)


    @Before
    fun init() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }

        MockitoAnnotations.initMocks(this)
        userDb = getUserDb()

        chatRepository = ChatRepository(chatApi, tokenHandler, messageDao, userDao, socketManager)


        val rpcResponse = RPCResponse<MessageEcho>()
        rpcResponse.result = MessageEcho(
            "1",
            "time",
            "todayConversation",
            lastMessageTime,
            "",
            false,
            false,
            "fromUserPhoto",
            "fullMembership",
            0
        )


        whenever(userDao.getUsersById(userId, drivenId)).thenReturn(Observable.just(userDb))


        val simpleMessage = SendSocketMessage(
            drivenId,
            userId,
            msgText,
            "hi",
            ChatRepository.TEXT_MSG_TYPE,
            null
        )
        whenever(socketManager.executeRPCAction(simpleMessage)).thenReturn(
            Single.just(rpcResponse)
        )
        val giftMsg = SendSocketMessage(
            drivenId,
            userId,
            giftId.toString(),
            giftCategoryId.toString(),
            ChatRepository.GIFT_MSG_TYPE,
            null
        )
        whenever(socketManager.executeRPCAction(giftMsg)).thenReturn(
            Single.just(rpcResponse)
        )

        val imageMsg = SendSocketMessage(
            drivenId,
            userId,
            "#privateChatImage#" + contentView.contentId + "#",
            "hi",
            ChatRepository.TEXT_MSG_TYPE,
            contentView
        )
        whenever(socketManager.executeRPCAction(imageMsg)).thenReturn(
            Single.just(rpcResponse)
        )


    }

    private fun getUserDb(): UserDb {
        return UserDb(
            Photo("", "", ""),
            0,
            0,
            0,
            0,
            "",
            "111",
            "",
            "",
            "",
            "",
            0,
            "",
            "",
            "",
            false,
            false,
            "222",
            null,
            0,
            0,
            0
        )
    }

    @Test
    fun sendMessage_success() {
        chatRepository.sendMessage(msgText, drivenId, userId)
            .test()
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    fun sendMessage_verifyMessageDbInsert() {
        val messageDb = MessageDb(
            "1",
            drivenId,
            userId,
            null,
            false,
            "hi",
            msgText,
            ChatRepository.TEXT_MSG_TYPE,
            "mail",
            lastMessageTime,
            null
        )

        chatRepository.sendMessage(msgText, drivenId, userId)
            .test()
        verify(messageDao).insert(messageDb)
    }

    @Test
    fun sendMessage_verifyUserUpdated() {
        chatRepository.sendMessage(msgText, drivenId, userId)
            .test()

        val us2 = getUserDb()
        us2.lastMessageTime = lastMessageTime
        verify(userDao).insert(us2)
    }

    @Test
    fun sendGift__success() {
        chatRepository.sendGift(drivenId, userId, giftId, giftCategoryId)
            .test()
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    fun sendGift_verifyMessageDbInsert() {
        val messageDb = MessageDb(
            "1",
            drivenId,
            userId,
            null,
            false,
            giftCategoryId.toString(),
            giftId.toString(),
            ChatRepository.GIFT_MSG_TYPE,
            "mail",
            lastMessageTime,
            null
        )

        chatRepository.sendGift(drivenId, userId, giftId, giftCategoryId)
            .test()
        verify(messageDao).insert(messageDb)
    }

    @Test
    fun sendGift_verifyUserUpdated() {
        chatRepository.sendGift(drivenId, userId, giftId, giftCategoryId)
            .test()

        val us2 = getUserDb()
        us2.lastMessageTime = lastMessageTime
        verify(userDao).insert(us2)
    }

    @Test
    fun sendImage_success() {
        chatRepository.sendImage(drivenId, userId, contentView)
            .test()
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    fun sendImage_verifyMessageDbInsert() {
        val messageDb = MessageDb(
            "1",
            drivenId,
            userId,
            null,
            false,
            "hi",
            "#privateChatImage#" + contentView.contentId + "#",
            ChatRepository.TEXT_MSG_TYPE,
            "mail",
            lastMessageTime,
            arrayListOf(ChatPhoto(contentView.contentId, contentView.url, false))
        )

        chatRepository.sendImage(drivenId, userId, contentView)
            .test()
        verify(messageDao).insert(messageDb)
    }

    @Test
    fun sendImage_verifyUserUpdated() {
        chatRepository.sendImage(drivenId, userId, contentView)
            .test()

        val us2 = getUserDb()
        us2.lastMessageTime = lastMessageTime
        verify(userDao).insert(us2)
    }

    @Test
    fun setReadAll_success_empty() {
        whenever(messageDao.getAll(drivenId, userId)).thenReturn(
            Single.just(arrayListOf())
        )

        chatRepository.setReadAll(drivenId, userId)
            .test()
            .assertComplete()
            .assertNoErrors()
        verify(messageDao).insertAll(arrayListOf())
        verifyZeroInteractions(socketManager)
    }

    @Test
    fun setReadAll_success() {
        whenever(messageDao.getAll(drivenId, userId)).thenReturn(
            Single.just(getTenUnreadMsgsArray())
        )

        chatRepository.setReadAll(drivenId, userId).test()

        verify(messageDao).insertAll(getTenReadMsgsArray())
    }

    private fun getTenUnreadMsgsArray(): ArrayList<MessageDb> {
        val arrayList: ArrayList<MessageDb> = arrayListOf()
        for (it in 1..10) {
            arrayList.add(
                MessageDb(
                    it.toString(),
                    userId,
                    drivenId,
                    null,
                    false,
                    "hi",
                    msgText,
                    ChatRepository.TEXT_MSG_TYPE,
                    "mail",
                    lastMessageTime,
                    null
                )
            )
        }
        return arrayList
    }

    private fun getTenReadMsgsArray(): ArrayList<MessageDb> {
        val arrayList: ArrayList<MessageDb> = arrayListOf()
        for (it in 1..10) {
            arrayList.add(
                MessageDb(
                    it.toString(),
                    userId,
                    drivenId,
                    null,
                    true,
                    "hi",
                    msgText,
                    ChatRepository.TEXT_MSG_TYPE,
                    "mail",
                    lastMessageTime,
                    null
                )
            )
        }
        return arrayList
    }
}