package com.prod.idatepartners.repository

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.profiles.*
import com.prod.idatepartners.api.requests.UsersApi
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.users.DrivenUserDao
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.fake.TestTokenHandlerFake
import com.prod.idatepartners.repo.DrivenUserRepository
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class DrivenUserRepositoryTest {

    @Mock
    private lateinit var usersApi: UsersApi
    @Mock
    private lateinit var drivenUserDao: DrivenUserDao
    private lateinit var drivenUserRepository: DrivenUserRepository
    private var tokenHandler: ITokenHandler = TestTokenHandlerFake()

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        drivenUserRepository = DrivenUserRepository(usersApi, tokenHandler, drivenUserDao)

        whenever(drivenUserDao.getAllObservable()).thenReturn(
            Observable.just(ArrayList())
        )
        whenever(usersApi.getDrivenCounters()).thenReturn(
            Single.just(getEmptyCounters())
        )
        whenever(usersApi.getDrivenContent()).thenReturn(
            Single.just(getDrivenContentResponse())
        )
    }

    @Test
    fun getDrivenUsers_success() {
        whenever(usersApi.getDrivenUsers()).thenReturn(Single.just(getDrivenUserResponse(hashMapOf())))
        drivenUserRepository.getDrivenUsers().test().assertNoErrors().assertComplete()
    }

    @Test
    fun getDrivenUsers_checkHideNoInternetUsers() {

        val map = HashMap<String, DrivenUser>()

        for (i in 1..5) {
            map[i.toString()] = defaultDriven(i.toString())
        }

        whenever(usersApi.getDrivenUsers()).thenReturn(Single.just(getDrivenUserResponse(map)))
        whenever(drivenUserDao.getAllObservable()).thenReturn(Observable.just(getFullDrivenDbList()))

        drivenUserRepository.getDrivenUsers().test()

        verify(drivenUserDao).insertAll(getTenAndFiveHiddenDrivenDbList())
    }

    @Test
    fun getDrivenUsers_checkAllUsersSame() {
        whenever(usersApi.getDrivenUsers()).thenReturn(
            Single.just(
                getDrivenUserResponse(getFullDrivenApiList())
            )
        )
        whenever(drivenUserDao.getAllObservable()).thenReturn(Observable.just(getFullDrivenDbList()))

        drivenUserRepository.getDrivenUsers().test()

        verify(drivenUserDao).insertAll(getFullDrivenDbList())
    }

    @Test
    fun getDrivenUsers_addNewUsersToDb() {
        whenever(usersApi.getDrivenUsers()).thenReturn(
            Single.just(
                getDrivenUserResponse(
                    getFullDrivenApiList()
                )
            )
        )
        whenever(drivenUserDao.getAllObservable()).thenReturn(Observable.just(arrayListOf()))

        drivenUserRepository.getDrivenUsers().test()

        verify(drivenUserDao).insertAll(getFullDrivenDbList())

    }

    private fun getFullDrivenApiList(): HashMap<String, DrivenUser> {
        val map = HashMap<String, DrivenUser>()

        for (i in 1..10) {
            map[i.toString()] = defaultDriven(i.toString())
        }
        return map
    }

    private fun getFullDrivenDbList(): ArrayList<DrivenUserDb> {
        val list = ArrayList<DrivenUserDb>()
        for (i in 1..10) {
            list.add(defaultDrivenDb(i.toString()))
        }
        return list
    }

    private fun getTenAndFiveHiddenDrivenDbList(): ArrayList<DrivenUserDb> {
        val list = ArrayList<DrivenUserDb>()
        for (i in 1..10) {

            list.add(if (i < 6) defaultDrivenDb(i.toString()) else hiddenDrivenDb(i.toString()))
        }
        return list
    }

    private fun getDrivenUserResponse(map: HashMap<String, DrivenUser>): BaseModelResponse<DrivenUserResponse> {

        val drivenUserResponse = DrivenUserResponse(map)
        return BaseModelResponse(
            BaseModelResponse.Status.SUCCESS,
            null,
            drivenUserResponse
        )
    }

    private fun getEmptyCounters(): BaseModelResponse<DrivenUserCountersResponse> {

        return BaseModelResponse(
            BaseModelResponse.Status.SUCCESS,
            null,
            DrivenUserCountersResponse(hashMapOf())
        )

    }

    private fun getCountersFirstFive(): BaseModelResponse<DrivenUserCountersResponse> {
        val map = HashMap<String, Int>()

        for (i in 1..10) {
            if (i < 6) {
                map[i.toString()] = 1
            }
        }

        return BaseModelResponse(
            BaseModelResponse.Status.SUCCESS,
            null,
            DrivenUserCountersResponse(hashMapOf())
        )


    }

    private fun getDrivenContentResponse(): BaseModelResponse<DrivenContentResponse> {
        return BaseModelResponse(
            BaseModelResponse.Status.SUCCESS,
            null,
            DrivenContentResponse(hashMapOf())
        )

    }


    private fun defaultDrivenDb(id: String) = DrivenUserDb(
        Photo("1", "2", "3"),
        id,
        "log",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123",
        0,
        null,
        false
    )

    private fun defaultDriven(id: String) = DrivenUser(
        Photo("1", "2", "3"),
        id,
        "log",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123"
    )

    private fun hiddenDrivenDb(id: String) = DrivenUserDb(
        Photo("1", "2", "3"),
        id,
        "log",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123",
        0,
        null,
        true
    )

}