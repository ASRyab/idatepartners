package com.prod.idatepartners.repository

import com.nhaarman.mockitokotlin2.anyVararg
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.SingleLetterApi
import com.prod.idatepartners.api.models.chat.PrivatChatImage
import com.prod.idatepartners.api.requests.LetterApi
import com.prod.idatepartners.api.rpc.ISocketManager
import com.prod.idatepartners.api.rpc.rpc_actions.MessageSocket
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.LetterAndUserDb
import com.prod.idatepartners.db.LetterDao
import com.prod.idatepartners.db.LetterDb
import com.prod.idatepartners.fake.TestTokenHandlerFake
import com.prod.idatepartners.repo.LettersRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LetterRepositoryTest {

    @Mock
    private lateinit var letterApi: LetterApi
    @Mock
    private lateinit var dao: LetterDao
    @Mock
    private lateinit var socketManager: ISocketManager

    private var tokenHandler: ITokenHandler = TestTokenHandlerFake()

    private lateinit var lettersRepository: LettersRepository

    private val userId = "111"
    private val drivenId = "222"
    private val letterId = "3"
    private val subject = "hi"
    private val message = "Text of letter"


    @Before
    fun init() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }

        MockitoAnnotations.initMocks(this)
        lettersRepository = LettersRepository(letterApi, tokenHandler, dao, socketManager)

        mapOf(
            Pair("id", drivenId),
            Pair("toId", userId),
            Pair("messageId", letterId)
        )
        whenever(
            letterApi.getLetterById(
                mapOf(
                    Pair("id", drivenId),
                    Pair("toId", userId),
                    Pair("messageId", letterId)
                )
            )
        ).thenReturn(
            Single.just(
                BaseModelResponse(
                    BaseModelResponse.Status.SUCCESS,
                    null,
                    getIncomeLetterApiModel(letterId, drivenId, userId)
                )
            )
        )
        whenever(
            letterApi.sendLetter(anyVararg())
        ).thenReturn(
            Single.just(BaseModelResponse(BaseModelResponse.Status.SUCCESS, null, Any()))
        )

        whenever(dao.getLetterByIdWithUser(drivenId, userId, letterId)).thenReturn(
            Observable.just(
                LetterAndUserDb(
                    null, null, null
                )
            )
        )

    }

    private fun getIncomeLetterApiModel(
        letterId: String,
        senderId: String,
        recipientId: String
    ): SingleLetterApi {

        return SingleLetterApi(
            letterId,
            senderId,
            recipientId,
            "0",
            "1"
            , false
            , "hi"
            , "letter"
            , senderId
            , arrayListOf()
            , 0
            , "shorttext"
            , "in"
            , "Vasya"
            , false
            , "Text of letter"
        )
    }

    @Test
    fun sendLetter_success() {
        val images: List<String>? = arrayListOf(
            "image1",
            "image2",
            "image3"
        )

        val insertedMap: HashMap<String, String?> = hashMapOf(
            "id" to drivenId,
            "recipientId" to userId,
            "replyToId" to userId,
            "subject" to subject,
            "text" to message,
            "titleImageKey" to "image1",
            "images[0]" to "image1",
            "images[1]" to "image2",
            "images[2]" to "image3"
        )

        lettersRepository.sendLetter(drivenId, userId, userId, subject, message, images).test()
            .assertComplete()
            .assertNoErrors()

        verify(letterApi).sendLetter(insertedMap)
    }

    @Test
    fun getLetterApiById_setRead() {

        lettersRepository.getLetterApiById(drivenId, userId, letterId)
            .test()
            .assertComplete()
            .assertNoErrors()

        verify(dao).insert(
            LetterDb(
                letterId,
                drivenId,
                userId,
                true,
                "hi",
                "letter",
                drivenId,
                0,
                "in",
                "Vasya",
                false,
                message,
                arrayListOf()
            )

        )
    }

    @Test
    fun getLettersListener_convertDb() {

        whenever(socketManager.lettersListener).thenReturn(
            Observable.just(
                MessageSocket(
                    "date",
                    subject,
                    "letter",
                    "activityId",
                    message,
                    letterId,
                    "Vasya",
                    "VasyasLogin",
                    drivenId,
                    "letter",
                    false,
                    "login",
                    0,
                    userId,
                    "time",
                    PrivatChatImage(arrayListOf())
                )
            )
        )

        lettersRepository.getLettersListener().test()
            .assertNoErrors()

        verify(
            dao
        ).insert(
            LetterDb(
                letterId,
                userId,
                drivenId,
                false,
                subject,
                "letter",
                null,
                0,
                "in",
                "Vasya",
                false,
                message,
                arrayListOf()
            )
        )
    }


}