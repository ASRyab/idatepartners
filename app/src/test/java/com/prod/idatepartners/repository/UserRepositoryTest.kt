package com.prod.idatepartners.repository

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.BaseModelResponse
import com.prod.idatepartners.api.models.profiles.*
import com.prod.idatepartners.api.requests.UsersApi
import com.prod.idatepartners.base.ITokenHandler
import com.prod.idatepartners.db.users.UserDao
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.fake.TestTokenHandlerFake
import com.prod.idatepartners.repo.IUserRepository
import com.prod.idatepartners.repo.UserRepository
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class UserRepositoryTest {

    @Mock
    lateinit var usersApi: UsersApi

    @Mock
    lateinit var userDao: UserDao

    private val tokenHandler: ITokenHandler = TestTokenHandlerFake()

    private lateinit var userRepository: IUserRepository
    private val drivenId = "123"

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        whenever(usersApi.getUsersByDriven(drivenId)).thenReturn(getUsersForDrivenList(drivenId))
        whenever(userDao.getUsersByDriven(drivenId)).thenReturn(
            Observable.just(
                getUsersForDrivenDBList(drivenId)
            )
        )
        userRepository = UserRepository(usersApi, tokenHandler, userDao)

    }

    @Test
    fun getUsersByDriven_noErrors() {
        userRepository.getUsersByDriven(drivenId).test()
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun getUsersByDriven_checkMapNoCounters() {
        userRepository.getUsersByDriven(drivenId).test()
        verify(userDao).insertAll(getUsersForDrivenDBList(drivenId))
    }

    @Test
    fun getUsersByDriven_checkMapLetterCounters() {
        val mapLetter = HashMap<String, Int>()
        val mapChat = HashMap<String, Int>()

        mapLetter["1"] = 1
        mapChat["2"] = 5
        whenever(usersApi.getUsersByDriven(drivenId)).thenReturn(
            getUsersForDrivenListWithCounters(drivenId, mapLetter, mapChat)
        )
        val list = ArrayList<UserDb>()
        list.add(defaultUserDb("1", drivenId, 1, 0))
        list.add(defaultUserDb("2", drivenId, 0, 5))

        for (i in 3..10) {
            list.add(defaultUserDb(i.toString(), drivenId, 0, 0))
        }

        userRepository.getUsersByDriven(drivenId).test()
        verify(userDao).insertAll(list)
    }


    private fun getUsersForDrivenList(drivenId: String): Single<BaseModelResponse<ProfileContactsResponse>>? {

        return Single.just(
            BaseModelResponse(
                BaseModelResponse.Status.SUCCESS, null, ProfileContactsResponse(
                    ProfileContacts(getDefaultUserApiList(), Counters.EMPTY_COUNTERS, drivenId)
                )
            )
        )
    }

    private fun getDefaultUserApiList(): HashMap<String, UserApi> {
        val map = HashMap<String, UserApi>()
        for (i in 1..10) {
            map[i.toString()] = defaultUserApi(i.toString())
        }
        return map
    }

    private fun getUsersForDrivenListWithCounters(
        drivenId: String,
        mapLetter: HashMap<String, Int>,
        mapChat: HashMap<String, Int>
    ): Single<BaseModelResponse<ProfileContactsResponse>>? {
        val counters = Counters(mapChat, mapLetter)

        return Single.just(
            BaseModelResponse(
                BaseModelResponse.Status.SUCCESS,
                null,
                ProfileContactsResponse(
                    ProfileContacts(
                        getDefaultUserApiList(),
                        counters,
                        drivenId
                    )
                )
            )
        )
    }

    private fun getUsersForDrivenDBList(drivenId: String): MutableList<UserDb> {
        val list = ArrayList<UserDb>()
        for (i in 1..10) {
            list.add(defaultUserDb(i.toString(), drivenId, 0, 0))
        }
        return list

    }

    private fun defaultUserDb(id: String, drivenId: String, letterCounter: Int, mailCounter: Int) =
        UserDb(
            Photo("1", "2", "3"),
            0,
            0,
            0,
            9,
            "",
            id,
            "log",
            "vasya",
            "petrov",
            "male",
            18,
            "Ukraine",
            "Zhytomyr",
            "123",
            true,
            true,
            drivenId,
            arrayListOf(),
            letterCounter,
            mailCounter,
            0
        )

    private fun defaultUserApi(id: String) = UserApi(
        Photo("1", "2", "3"),
        0,
        0,
        0,
        9,
        "",
        id,
        "log",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123",
        true,
        true, arrayListOf(),
        0
    )

}
