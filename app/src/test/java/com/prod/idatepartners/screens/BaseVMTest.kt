package com.prod.idatepartners.screens

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import com.nhaarman.mockitokotlin2.whenever
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.mockito.Mock
import org.mockito.MockitoAnnotations

abstract class BaseVMTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var context: Context

    @Mock
    lateinit var mOwner: LifecycleOwner

    lateinit var mLifecycle: LifecycleRegistry

    @Before
    open fun setUp() {
        MockitoAnnotations.initMocks(this)
        initContext()
        initLifeCycle()
    }

    @After
    fun tearDown() {
        mLifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    }

    open fun initLifeCycle() {
        mLifecycle = LifecycleRegistry(mOwner)
        whenever(mOwner.lifecycle).thenReturn(mLifecycle)
        mLifecycle.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        mLifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    open fun initContext() {}
}