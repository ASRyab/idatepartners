package com.prod.idatepartners.screens

import android.os.Bundle
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.only
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.screens.choosePhoto.ChoosePhotoAdapter
import com.prod.idatepartners.screens.choosePhoto.ChoosePhotoVM
import com.prod.idatepartners.screens.choosePhoto.ContentView
import com.prod.idatepartners.usecases.IUsersUseCase
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.mock

class ChoosePhotoVMTest : BaseVMTest() {

    @Mock
    lateinit var usersUseCase: IUsersUseCase
    @Mock
    private lateinit var listLiveDataObserver: Observer<List<ContentView>>
    @Mock
    private lateinit var selectedObserver: Observer<List<ContentView>>
    @Mock
    lateinit var currentUserObserver: Observer<DrivenUserDb?>
    @Mock
    lateinit var isChatObserver: Observer<Boolean>
    @Mock
    lateinit var choosePhotoEventObserver: Observer<ChoosePhotoVM.ChoosePhotoStatus>

    lateinit var vm: ChoosePhotoVM

    private val drivenId = "123"
    private val mockAdapter: ChoosePhotoAdapter = mock(ChoosePhotoAdapter::class.java)

    private var drivenUserDb: DrivenUserDb = DrivenUserDb(
        Photo("", "", ""),
        drivenId, "", "", "", "", 30, "", "", "", 0, null, false
    )


    @Before
    override fun setUp() {
        super.setUp()

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }

        vm = ChoosePhotoVM(usersUseCase)

        vm.listLiveData.observe(mOwner, listLiveDataObserver)
        vm.selected.observe(mOwner, selectedObserver)
        vm.currentUser.observe(mOwner, currentUserObserver)
        vm.isChat.observe(mOwner, isChatObserver)
        vm.choosePhotoEvent.observe(mOwner, choosePhotoEventObserver)

        vm.adapter = mockAdapter

        whenever(usersUseCase.getDrivenById(drivenId)).thenReturn(Observable.just(drivenUserDb))
    }

    @Test
    fun passBundle_fromChat() {
        val bundle = mock(Bundle::class.java)

        whenever(bundle.getString("driven_user_id")).thenReturn(drivenId)
        whenever(bundle.getBoolean("isChat")).thenReturn(true)

        vm.passBundle(bundle)
        verify(isChatObserver).onChanged(true)
        verify(mockAdapter, only()).notifyDataSetChanged()
        verify(currentUserObserver, only()).onChanged(drivenUserDb)
        verify(selectedObserver, only()).onChanged(arrayListOf())
        verify(listLiveDataObserver, only()).onChanged(null)

    }

    @Test
    fun passBundle_fromLetter() {
        val bundle = mock(Bundle::class.java)

        whenever(bundle.getString("driven_user_id")).thenReturn(drivenId)
        whenever(bundle.getBoolean("isChat")).thenReturn(false)

        vm.passBundle(bundle)
        verify(isChatObserver).onChanged(false)
        verify(mockAdapter, only()).notifyDataSetChanged()
        verify(currentUserObserver, only()).onChanged(drivenUserDb)
        verify(selectedObserver, only()).onChanged(arrayListOf())
        verify(listLiveDataObserver, only()).onChanged(null)
    }

    @Test
    fun passBundle_set_selected_and_open_screen_again() {
        val bundle = mock(Bundle::class.java)

        whenever(bundle.getString("driven_user_id")).thenReturn(drivenId)
        whenever(bundle.getBoolean("isChat")).thenReturn(false)

        vm.oldUserId = drivenId
        val contentList = arrayListOf(
            getContentView("1", drivenId, false),
            getContentView("2", drivenId, false),
            getContentView("3", drivenId, true)
        )
        vm.listLiveData.value = contentList
        vm.onOkClick()
        vm.passBundle(bundle)
        assertEquals(
            arrayListOf(getContentView("3", drivenId, true)),
            vm.selected.value
        )
        assertEquals(
            vm.listLiveData.value, contentList
        )
    }

    @Test
    fun selectItem_select_item() {
        vm.isChat.value = false

        val contentList = arrayListOf(
            getContentView("1", drivenId, false),
            getContentView("2", drivenId, false),
            getContentView("3", drivenId, false)
        )
        vm.listLiveData.value = contentList

        vm.selectItem(0)
        vm.selectItem(1)

        assertEquals(
            arrayListOf(
                getContentView("1", drivenId, true),
                getContentView("2", drivenId, true),
                getContentView("3", drivenId, false)

            ), vm.listLiveData.value

        )
    }

    @Test
    fun selectItem_select_item_if_chat() {
        vm.isChat.value = true
        val contentList = arrayListOf(
            getContentView("1", drivenId, false),
            getContentView("2", drivenId, true)
        )
        vm.listLiveData.value = contentList

        vm.selectItem(0)

        assertEquals(
            arrayListOf(
                getContentView("1", drivenId, true),
                getContentView("2", drivenId, false)
            ), vm.listLiveData.value

        )
    }

    @Test
    fun onCloseClick_chat_clear() {
        vm.isChat.value = true
        vm.onCloseClick()
        verify(selectedObserver).onChanged(ArrayList())
        verify(listLiveDataObserver).onChanged(ArrayList())
        verify(mockAdapter).notifyDataSetChanged()
        verify(choosePhotoEventObserver).onChanged(ChoosePhotoVM.ChoosePhotoStatus.Close)

    }

    @Test
    fun onOkClick() {
        val contentList = arrayListOf(
            getContentView("1", drivenId, false),
            getContentView("2", drivenId, true),
            getContentView("3", drivenId, true)
        )
        vm.listLiveData.value = contentList

        vm.onOkClick()
        verify(selectedObserver).onChanged(
            arrayListOf(getContentView("2", drivenId, true), getContentView("3", drivenId, true))
        )
        verify(choosePhotoEventObserver).onChanged(ChoosePhotoVM.ChoosePhotoStatus.Close)
    }

    private fun getContentView(id: String, profileId: String, isSelected: Boolean): ContentView {
        return ContentView(
            id,
            "img",
            profileId,
            "approved",
            "11-05-2019",
            "someUrl",
            "previewUrl",
            isSelected
        )
    }
}