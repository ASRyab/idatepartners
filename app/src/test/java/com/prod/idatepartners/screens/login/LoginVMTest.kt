package com.prod.idatepartners.screens.login

import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.R
import com.prod.idatepartners.api.models.AuthData
import com.prod.idatepartners.screens.BaseVMTest
import com.prod.idatepartners.usecases.IAutoLoginUseCase
import com.prod.idatepartners.usecases.ILoginUseCase
import com.prod.idatepartners.utils.UserFieldsValidator
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class LoginVMTest: BaseVMTest() {

    @Mock
    private lateinit var loginUseCase: ILoginUseCase
    @Mock
    private lateinit var autoLoginUseCase: IAutoLoginUseCase
    @Mock
    private lateinit var authData: AuthData

    @Mock
    private lateinit var mEventObserver: Observer<LoginVM.LoginEvent>

    private lateinit var loginVM: LoginVM
    private val LOGIN = "johnmitchell333555@gmail.com"
    private val PASS = "Megapups123321"

    @Before
    override fun setUp() {
        super.setUp()
        whenever(autoLoginUseCase.tryAutologin()).thenReturn(Single.error(Exception()))
        loginVM = LoginVM(loginUseCase, UserFieldsValidator(context), autoLoginUseCase)
        loginVM.loginEvent.observe(mOwner, mEventObserver)

    }

     override fun initContext() {
        whenever(context.getString(R.string.validator_error_password_length)).thenReturn("validator_error_password_length")
        whenever(context.getString(R.string.password_error_empty)).thenReturn("password_error_empty")
        whenever(context.getString(R.string.password_error)).thenReturn("password_error")
        whenever(context.getString(R.string.email_error)).thenReturn("email_error")
    }

    @Test
    fun cleanStart() {
        val loginData = LoginData("", "")
        assertEquals(loginData.name, loginVM.data.get()?.name)
        assertEquals(loginData.pass, loginVM.data.get()?.pass)
        assertTrue(loginVM.startAnimation.get())
    }

    @Test
    fun cleanStart_animation() {
        val loginData = LoginData("", "")
        assertEquals(loginData.name, loginVM.data.get()?.name)
        assertEquals(loginData.pass, loginVM.data.get()?.pass)
    }

    @Test
    fun onLoginClicked_non_enter() {
        loginVM.onLoginClicked()
        verify(mEventObserver).onChanged(
            LoginVM.LoginEvent(
                LoginVM.LoginStatus.LoginInvalidPass,
                "password_error_empty"
            )
        )
        verify(mEventObserver).onChanged(LoginVM.LoginEvent(LoginVM.LoginStatus.LoginInvalidEmail, "email_error"))
    }

    @Test
    fun onLoginClicked_ok() {
        loginVM.data.set(LoginData(LOGIN, PASS))
        whenever(loginUseCase.loginRequest(LOGIN, PASS)).thenReturn(
            Single.just(
                authData
            )
        )
        loginVM.onLoginClicked()
        verify(mEventObserver).onChanged(LoginVM.LoginEvent(LoginVM.LoginStatus.LoginOk, null))
        verifyZeroInteractions(mEventObserver)
    }

    @Test
    fun onLoginClicked_no_name() {
        loginVM.data.set(LoginData("", PASS))
        loginVM.onLoginClicked()
        verify(mEventObserver).onChanged(LoginVM.LoginEvent(LoginVM.LoginStatus.LoginInvalidEmail, "email_error"))
        verifyZeroInteractions(mEventObserver)
    }
    @Test
    fun onLoginClicked_no_pass() {
        loginVM.data.set(LoginData(LOGIN, ""))
        loginVM.onLoginClicked()
        verify(mEventObserver).onChanged(LoginVM.LoginEvent(LoginVM.LoginStatus.LoginInvalidPass, "password_error_empty"))
        verifyZeroInteractions(mEventObserver)
    }
    @Test
    fun onLoginClicked_min_pass() {
        loginVM.data.set(LoginData(LOGIN, "12345"))
        loginVM.onLoginClicked()
        verify(mEventObserver).onChanged(LoginVM.LoginEvent(LoginVM.LoginStatus.LoginInvalidPass, "validator_error_password_length"))
        verifyZeroInteractions(mEventObserver)
    }
    @Test
    fun onLoginClicked_max_pass() {
        loginVM.data.set(LoginData(LOGIN, "123456789012345678901"))
        loginVM.onLoginClicked()
        verify(mEventObserver).onChanged(LoginVM.LoginEvent(LoginVM.LoginStatus.LoginInvalidPass, "validator_error_password_length"))
        verifyZeroInteractions(mEventObserver)
    }

    @Test
    fun onLoginClicked_bad_name() {
        loginVM.data.set(LoginData("adad@@f.f", PASS))
        loginVM.onLoginClicked()
        verify(mEventObserver).onChanged(LoginVM.LoginEvent(LoginVM.LoginStatus.LoginInvalidEmail, "email_error"))
        verifyZeroInteractions(mEventObserver)
    }
}