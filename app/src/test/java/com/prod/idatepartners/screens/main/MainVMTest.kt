package com.prod.idatepartners.screens.main

import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.only
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.screens.BaseVMTest
import com.prod.idatepartners.usecases.IGiftUseCase
import com.prod.idatepartners.usecases.ILogoutUseCase
import com.prod.idatepartners.usecases.ISocketChatUseCase
import com.prod.idatepartners.usecases.IUsersUseCase
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class MainVMTest : BaseVMTest() {
    @Mock
    lateinit var usersUseCase: IUsersUseCase
    @Mock
    lateinit var socketChatUseCase: ISocketChatUseCase
    @Mock
    lateinit var logoutUseCase: ILogoutUseCase
    @Mock
    lateinit var giftUseCase: IGiftUseCase

    lateinit var vm: MainVM
    private lateinit var listDrivenUser: MutableList<DrivenUserDb>
    private lateinit var listUser: MutableList<UserDb>

    private val drivenId = "123"

    var drivenUserDb: DrivenUserDb = DrivenUserDb(
        Photo("", "", ""),
        drivenId, "", "", "", "", 30, "", "", "", 0, null, false
    )

    private val userId = "321"

    var userDb: UserDb = UserDb(
        Photo("", "", ""), 0, 0, 0, 0, "", userId, "", "", "", "", 0, "", "", "", false, false,
        drivenId, null, 0, 0, 0
    )

    @Mock
    private lateinit var mainCounterObserver: Observer<Int>
    @Mock
    private lateinit var selectedDrivenIdDataObserver: Observer<String>
    @Mock
    private lateinit var selectedUserIdDataObserver: Observer<String>
    @Mock
    private lateinit var hasChatRedPointObserver: Observer<Boolean>
    @Mock
    private lateinit var hasLetterRedPointObserver: Observer<Boolean>
    @Mock
    private lateinit var tapUserIdObserver: Observer<String>
    @Mock
    private lateinit var isChatObserver: Observer<Boolean>
    @Mock
    private lateinit var selectedUserPositionObserver: Observer<Int>

    @Before
    override fun setUp() {
        super.setUp()
        vm = MainVM(usersUseCase, socketChatUseCase, logoutUseCase, giftUseCase)
        vm.mainCounter.observe(mOwner, mainCounterObserver)
        vm.selectedDrivenIdData.observe(mOwner, selectedDrivenIdDataObserver)
        vm.selectedUserIdData.observe(mOwner, selectedUserIdDataObserver)
        vm.hasChatRedPoint.observe(mOwner, hasChatRedPointObserver)
        vm.hasLetterRedPoint.observe(mOwner, hasLetterRedPointObserver)
        vm.tapUserIdEvent.observe(mOwner, tapUserIdObserver)
        vm.isChat.observe(mOwner, isChatObserver)
        vm.selectedUserPosition.observe(mOwner, selectedUserPositionObserver)
        listDrivenUser = mutableListOf(drivenUserDb, drivenUserDb)
        listUser = mutableListOf(userDb, userDb)
        whenever(usersUseCase.getDrivenUsers()).thenReturn(Observable.just(listDrivenUser))
        whenever(usersUseCase.getUsers(drivenId)).thenReturn(Observable.just(listUser))
        whenever(usersUseCase.getUser(userId, drivenId)).thenReturn(Observable.just(userDb))
        whenever(socketChatUseCase.setReadAll(drivenId, userId)).thenReturn(Single.just(Any()))
    }


    @Test
    fun updateDrivenUsers_base() {
        drivenUserDb.countUnread = 1
        userDb.letterCount = 0
        vm.updateDrivenUsers()
        verify(mainCounterObserver).onChanged(2)
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver).onChanged(userId)
        verify(socketChatUseCase, only()).setReadAll(drivenId, userId)
        verify(hasChatRedPointObserver, only()).onChanged(false)
        verify(hasLetterRedPointObserver, only()).onChanged(false)
    }

    @Test
    fun updateDrivenUsers_non_main_counter() {
        drivenUserDb.countUnread = 0
        userDb.letterCount = 0
        vm.updateDrivenUsers()
        verify(mainCounterObserver).onChanged(0)
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver).onChanged(userId)
        verify(socketChatUseCase, only()).setReadAll(drivenId, userId)
        verify(hasChatRedPointObserver, only()).onChanged(false)
        verify(hasLetterRedPointObserver, only()).onChanged(false)
    }

    @Test
    fun updateDrivenUsers_letter_red() {
        drivenUserDb.countUnread = 1
        userDb.letterCount = 1
        vm.updateDrivenUsers()
        verify(mainCounterObserver).onChanged(2)
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver).onChanged(userId)
        verify(socketChatUseCase, only()).setReadAll(drivenId, userId)
        verify(hasChatRedPointObserver, only()).onChanged(false)
        verify(hasLetterRedPointObserver, only()).onChanged(true)
    }

    @Test
    fun updateDrivenUsers_mail__red() {
        drivenUserDb.countUnread = 1
        userDb.mailCount = 1
        vm.isChat.value = false
        vm.updateDrivenUsers()
        verify(mainCounterObserver).onChanged(2)
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver).onChanged(userId)
        verify(hasChatRedPointObserver, only()).onChanged(true)
        verify(hasLetterRedPointObserver, only()).onChanged(false)
    }

    @Test
    fun updateDrivenUsers_mail_letter_red_on_letter() {
        drivenUserDb.countUnread = 1
        userDb.mailCount = 1
        userDb.letterCount = 1
        vm.isChat.value = false
        vm.updateDrivenUsers()
        verify(mainCounterObserver).onChanged(2)
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver).onChanged(userId)
        verify(hasChatRedPointObserver, only()).onChanged(true)
        verify(hasLetterRedPointObserver, only()).onChanged(false)
    }

    @Test
    fun updateDrivenUsers_mail_letter_red_on_chat() {
        drivenUserDb.countUnread = 1
        userDb.mailCount = 1
        userDb.letterCount = 1
        vm.updateDrivenUsers()
        verify(mainCounterObserver).onChanged(2)
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver).onChanged(userId)
        verify(socketChatUseCase, only()).setReadAll(drivenId, userId)
        verify(hasChatRedPointObserver, only()).onChanged(false)
        verify(hasLetterRedPointObserver, only()).onChanged(true)
    }

    @Test
    fun onDrivenSelected_base() {
        val testId = "testId"
        val drivenUserDb = DrivenUserDb(
            Photo("", "", ""),
            testId, "", "", "", "", 30, "", "", "", 0, null, false
        )

        listDrivenUser.add(2, drivenUserDb)
        val mock = Mockito.mock(UsersAdapter::class.java)
        vm.adapterDrivenUser = mock
        vm.updateDrivenUsers()

        whenever(usersUseCase.getUsers(testId)).thenReturn(Observable.just(listUser))
        vm.onDrivenSelected(testId, 2)
        //main
        verify(selectedDrivenIdDataObserver).onChanged(testId)
        verify(selectedUserIdDataObserver, times(2)).onChanged(userId)// 1 updateDrivenUsers, 2 test
        verify(socketChatUseCase).setReadAll(testId, userId)
        verify(hasChatRedPointObserver).onChanged(false)
        verify(hasLetterRedPointObserver).onChanged(false)
        //other
        verify(isChatObserver, times(2)).onChanged(true)// 1 init, 2 test
        assert(vm.selectedDrivenPosition == 2)
        verify(selectedUserPositionObserver, times(3)).onChanged(0)// 1 init, 2 updateDrivenUsers, 3 test
    }

    @Test
    fun onUserSelected_base() {
        val testId = "testId"
        val userDb = UserDb(
            Photo("", "", ""), 0, 0, 0, 0, "", testId, "", "", "", "", 0, "", "", "", false, false,
            drivenId, null, 0, 0, 0
        )

        listUser.add(2, userDb)
        val mock = Mockito.mock(UsersAdapter::class.java)
        vm.adapterUser = mock
        vm.updateDrivenUsers()

        whenever(socketChatUseCase.setReadAll(drivenId, testId)).thenReturn(Single.just(Any()))
        whenever(usersUseCase.getUser(testId, drivenId)).thenReturn(Observable.just(userDb))
        vm.onUserSelected(testId, 2)
        //main
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver, times(1)).onChanged(userId)
        verify(selectedUserIdDataObserver, times(1)).onChanged(testId)
        verify(socketChatUseCase).setReadAll(drivenId, testId)
        verify(hasChatRedPointObserver, times(2)).onChanged(false)
        verify(hasLetterRedPointObserver, times(2)).onChanged(false)
        //other
        verify(isChatObserver, times(2)).onChanged(true)// 1 init, 2 test
        verify(selectedUserPositionObserver, times(2)).onChanged(0)// 1 init, 2 updateDrivenUsers
        verify(selectedUserPositionObserver, times(1)).onChanged(2)
    }

    @Test
    fun onUserSelected_base_double_click() {
        val testId = "testId"
        val userDb = UserDb(
            Photo("", "", ""), 0, 0, 0, 0, "", testId, "", "", "", "", 0, "", "", "", false, false,
            drivenId, null, 0, 0, 0
        )

        listUser.add(2, userDb)
        val mock = Mockito.mock(UsersAdapter::class.java)
        vm.adapterUser = mock
        vm.updateDrivenUsers()

        whenever(socketChatUseCase.setReadAll(drivenId, testId)).thenReturn(Single.just(Any()))
        whenever(usersUseCase.getUser(testId, drivenId)).thenReturn(Observable.just(userDb))
        vm.onUserSelected(testId, 2)
        vm.onUserSelected(testId, 2)
        //main
        verify(selectedDrivenIdDataObserver).onChanged(drivenId)
        verify(selectedUserIdDataObserver, times(1)).onChanged(userId)
        verify(selectedUserIdDataObserver, times(1)).onChanged(testId)
        verify(socketChatUseCase).setReadAll(drivenId, testId)
        verify(hasChatRedPointObserver, times(3)).onChanged(false)
        verify(hasLetterRedPointObserver, times(3)).onChanged(false)
        //other
        verify(tapUserIdObserver, times(1)).onChanged(testId)
        verify(isChatObserver, times(2)).onChanged(true)// 1 init, 2 test
        verify(selectedUserPositionObserver, times(2)).onChanged(0)// 1 init, 2 updateDrivenUsers
        verify(selectedUserPositionObserver, times(2)).onChanged(2)
    }

}