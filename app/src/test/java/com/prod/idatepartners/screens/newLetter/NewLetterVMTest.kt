package com.prod.idatepartners.screens.newLetter

import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.R
import com.prod.idatepartners.screens.BaseVMTest
import com.prod.idatepartners.usecases.ILetterUseCase
import io.reactivex.Single
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import java.io.IOException

class NewLetterVMTest : BaseVMTest() {

    @Mock
    private lateinit var letterUseCase: ILetterUseCase


    private lateinit var newLetterVM: NewLetterVM

    @Mock
    private lateinit var mEventObserver: Observer<NewLetterVM.LetterEvent>

    private val TEXT_OK =
        "20020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020"
    private val TEXT_MORE =
        "20020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020"
    private val TEXT_LESS =
        "20020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020020"
    private val SUBJECT_OK = "subjectText"
    private val EMPTY_TEXT = ""

    @Before
    override fun setUp() {
        super.setUp()
        newLetterVM = NewLetterVM(letterUseCase, context)
        newLetterVM.letterEvent.observe(mOwner, mEventObserver)
    }

    override fun initContext() {
        whenever(context.getString(R.string.min_200_characters)).thenReturn("error message")
        whenever(context.getString(R.string.should_consist_subject)).thenReturn("error message")
    }

    @Test
    fun hasErrors_empty_fields() {
        newLetterVM.letterText.set("")
        newLetterVM.subjectText.set("")
        assertTrue(newLetterVM.hasErrors())
    }


    @Test
    fun hasErrors_ok() {
        newLetterVM.letterText.set(TEXT_OK)
        newLetterVM.subjectText.set(SUBJECT_OK)
        assertFalse(newLetterVM.hasErrors())
    }

    @Test
    fun hasErrors_ok_more() {
        newLetterVM.letterText.set(TEXT_MORE)
        newLetterVM.subjectText.set(SUBJECT_OK)
        assertFalse(newLetterVM.hasErrors())
    }

    @Test
    fun hasErrors_no_less() {
        newLetterVM.letterText.set(TEXT_LESS)
        newLetterVM.subjectText.set(SUBJECT_OK)
        assertTrue(newLetterVM.hasErrors())
    }

    @Test
    fun hasErrors_no_subject() {
        newLetterVM.letterText.set(TEXT_OK)
        newLetterVM.subjectText.set(EMPTY_TEXT)
        assertTrue(newLetterVM.hasErrors())
    }


    @Test
    fun sendLetter() {

        newLetterVM.drivenUserId = EMPTY_TEXT
        newLetterVM.userId = EMPTY_TEXT
        newLetterVM.letterId = EMPTY_TEXT
        newLetterVM.letterText.set(TEXT_OK)
        newLetterVM.subjectText.set(SUBJECT_OK)
        whenever(
            letterUseCase.sendLetter(
                EMPTY_TEXT,
                EMPTY_TEXT,
                EMPTY_TEXT,
                SUBJECT_OK,
                TEXT_OK, ArrayList()
            )
        ).thenReturn(
            Single.just(1)
        )

        newLetterVM.sendLetter()
        verify(mEventObserver).onChanged(NewLetterVM.LetterEvent(NewLetterVM.LetterStatus.MessageSent, null))
    }

    @Test
    fun sendLetter_Error() {

        newLetterVM.drivenUserId = EMPTY_TEXT
        newLetterVM.userId = EMPTY_TEXT
        newLetterVM.letterId = EMPTY_TEXT
        newLetterVM.letterText.set(TEXT_OK)
        newLetterVM.subjectText.set(SUBJECT_OK)
        whenever(
            letterUseCase.sendLetter(
                EMPTY_TEXT,
                EMPTY_TEXT,
                EMPTY_TEXT,
                SUBJECT_OK,
                TEXT_OK, ArrayList()
            )
        ).thenReturn(
            Single.error(IOException())
        )
        newLetterVM.sendLetter()
        verify(mEventObserver).onChanged(NewLetterVM.LetterEvent(NewLetterVM.LetterStatus.SentFailed, "Message sent failed"))
    }
}