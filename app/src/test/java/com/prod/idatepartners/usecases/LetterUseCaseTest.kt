package com.prod.idatepartners.usecases

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.db.LetterDb
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.repo.IDrivenUserRepository
import com.prod.idatepartners.repo.ILettersRepository
import com.prod.idatepartners.repo.IUserRepository
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class LetterUseCaseTest {

    @Mock
    lateinit var lettersRepository: ILettersRepository

    @Mock
    lateinit var userRepository: IUserRepository

    @Mock
    lateinit var drivenUserRepository: IDrivenUserRepository

    var letterUseCase: LetterUseCase

    private lateinit var letter: LetterDb
    private lateinit var drivenUser: DrivenUserDb
    private lateinit var user: UserDb

    @Before
    fun init() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }

        letter = defaultLetter()
        drivenUser = defaultDriven()
        user = defaultUser()

        whenever(lettersRepository.getLettersListener())
            .thenReturn(Observable.just(letter))

        whenever(drivenUserRepository.getDrivenUserById(letter.recipientId))
            .thenReturn(Observable.just(drivenUser))

        whenever(userRepository.getUserById(letter.senderId, letter.recipientId))
            .thenReturn(Observable.just(user))


    }

    init {
        MockitoAnnotations.initMocks(this)
        letterUseCase = LetterUseCase(
            lettersRepository,
            userRepository,
            drivenUserRepository
        )
    }


    private fun defaultLetter() = LetterDb(
        "111",
        "222",
        "333",
        false,
        "hi",
        "letter",
        "222",
        0,
        "to",
        "vasya",
        false,
        "texttext",
        ArrayList()
    )

    private fun defaultDriven() = DrivenUserDb(
        Photo("1", "2", "3"),
        "222",
        "log",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123",
        0,
        ArrayList(),
        false
    )

    private fun defaultUser() = UserDb(
        Photo("1", "2", "3"),
        1,
        2,
        3,
        4,
        "1",
        "333",
        "333",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123",
        true,
        false, "222",
        ArrayList(),
        0,
        0
    )

    @Test
    fun getLettersListener_check_driven_counter_update() {
        letterUseCase.getLettersListener()
            .test()
            .assertValue { it is DrivenUserDb }
            .assertValue { (it as DrivenUserDb).countUnread == 1 }
        val u = defaultUser()
        u.letterCount = 1
        verify(userRepository).saveUser(u)

        val drivenUser = defaultDriven()
        drivenUser.countUnread = 1
        verify(drivenUserRepository).update(drivenUser)

    }

    @Test
    fun getLettersListener_check_driven_not_update_when_counter_exists() {
        user.letterCount = 1
        letterUseCase.getLettersListener()
            .test()
            .assertValue { it is Boolean }

        val u = defaultUser()
        u.letterCount = 2
        verify(userRepository).saveUser(u)
    }

}