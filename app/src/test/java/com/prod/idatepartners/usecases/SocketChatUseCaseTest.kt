package com.prod.idatepartners.usecases

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.prod.idatepartners.api.models.profiles.Photo
import com.prod.idatepartners.db.MessageDb
import com.prod.idatepartners.db.users.DrivenUserDb
import com.prod.idatepartners.db.users.UserDb
import com.prod.idatepartners.repo.IChatRepository
import com.prod.idatepartners.repo.IDrivenUserRepository
import com.prod.idatepartners.repo.IUserRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SocketChatUseCaseTest {

    @Mock
    lateinit var chatRepository: IChatRepository

    @Mock
    lateinit var userRepository: IUserRepository

    @Mock
    lateinit var drivenUserRepository: IDrivenUserRepository

    private var socketChatUseCase: SocketChatUseCase

    private lateinit var message: MessageDb
    private lateinit var drivenUser: DrivenUserDb
    private lateinit var user: UserDb

    @Before
    fun init() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }

        message = defaultMessage()
        drivenUser = defaultDriven()
        user = defaultUser()

        whenever(chatRepository.getChatListener())
            .thenReturn(Observable.just(message))

        whenever(drivenUserRepository.getDrivenUserById(message.recipientId))
            .thenReturn(Observable.just(drivenUser))

        whenever(userRepository.getUserById(message.senderId, message.recipientId))
            .thenReturn(Observable.just(user))

        whenever(chatRepository.setReadAll(message.recipientId, message.senderId))
            .thenReturn(Single.just(true))


    }

    init {
        MockitoAnnotations.initMocks(this)
        socketChatUseCase = SocketChatUseCase(chatRepository, userRepository, drivenUserRepository)
    }


    private fun defaultMessage() = MessageDb(
        "111",
        "333",
        "222",
        "data",
        false,
        "hi",
        "some short text",
        "mail",
        "to",
        0,
        ArrayList()
    )

    private fun defaultDriven() = DrivenUserDb(
        Photo("1", "2", "3"),
        "222",
        "log",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123",
        0,
        ArrayList(),
        false
    )

    private fun defaultUser() = UserDb(
        Photo("1", "2", "3"),
        1,
        2,
        3,
        4,
        "1",
        "333",
        "333",
        "vasya",
        "petrov",
        "male",
        18,
        "Ukraine",
        "Zhytomyr",
        "123",
        true,
        false, "222",
        ArrayList(),
        0,
        0
    )

    @Test
    fun getChatListener_check_driven_counter_update() {
        socketChatUseCase.getChatListener()
            .test()
            .assertValue { it is DrivenUserDb }
            .assertValue { (it as DrivenUserDb).countUnread == 1 }

        val u = defaultUser()
        u.mailCount = 1
        verify(userRepository).saveUser(u)

        val drivenUser = defaultDriven()
        drivenUser.countUnread = 1
        verify(drivenUserRepository).update(drivenUser)

    }

    @Test
    fun getChatListener_check_driven_not_update_when_counter_exists() {
        user.mailCount = 1
        socketChatUseCase.getChatListener()
            .test()
            .assertValue { it is Boolean }

        val u = defaultUser()
        u.mailCount = 2
        verify(userRepository).saveUser(u)
    }

    @Test
    fun setReadAll_check_all_messages_read() {
        user.mailCount = 10
        drivenUser.countUnread = 10
        socketChatUseCase.setReadAll(drivenUser.id, user.userId)
            .test()
            .assertNoErrors()

        val u = defaultUser()
        u.mailCount = 0
        verify(userRepository).saveUser(u)

        val drivenUser = defaultDriven()
        drivenUser.countUnread = 9
        verify(drivenUserRepository).update(drivenUser)
    }

    @Test
    fun setReadAll_check_not_upd_if_counter_was_not_exist() {
        user.mailCount = 1
        drivenUser.countUnread = 0
        socketChatUseCase.setReadAll(drivenUser.id, user.userId)
            .test()

        val u = defaultUser()
        u.mailCount = 0
        verify(userRepository).saveUser(u)

        val drivenUser = defaultDriven()
        drivenUser.countUnread = 0
        verify(drivenUserRepository).update(drivenUser)
    }

}